package pt.unl.fct.di.www.myapplication;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class NotificationAdapter extends ArrayAdapter<String> {

    private final String[] state;

    NotificationAdapter(Context context, String[] notificationsArray, String[]state){
        super(context,R.layout.notification_row, notificationsArray);
        this.state = state;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.notification_row,parent,false);



        String singleItem = getItem(position);
        String st = state[position];
        TextView text = (TextView) customView.findViewById(R.id.notText);
        TextView notState = (TextView) customView.findViewById(R.id.notState);
        ImageView image = (ImageView) customView.findViewById(R.id.fixedImage);
        text.setText(singleItem);
        notState.setText(st);
        if(st.equals(" NOVO ")) {
            notState.setTextColor(customView.getResources().getColor(R.color.Black));
            notState.setBackground(customView.getResources().getDrawable(R.drawable.new_not));
        }
        image.setImageResource(R.mipmap.ic_green_check);

        return customView;
    }


}
