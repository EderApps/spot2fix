package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ProfilesActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GetUserTask mGetUserTask = null;
    TextView nameText;
    TextView username;
    private String user="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);
        SharedPreferences sp = getSharedPreferences("AUTHENTICATION", 0);
        user = sp.getString("email", "");

        nameText = (TextView) findViewById(R.id.nameText);
        username = (TextView) findViewById(R.id.usernameText);

        // username.setText(userID);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getUser();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void getUser(){

        mGetUserTask = new GetUserTask(user);
        mGetUserTask.execute((Void) null);
    }



    public class GetUserTask extends AsyncTask<Void, Void, String> {
        private String userID = "";

        GetUserTask(String userID) {
            this.userID = userID;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getUser/"+userID);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetUserTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    JSONObject propertyMap = response.getJSONObject("propertyMap");
                    String name = propertyMap.getString("user_name");
                    String userAddress = propertyMap.getString("user_address");
                    nameText.setText(name);
                    username.setText(user);

                    if (userAddress != null && !userAddress.equals("")) {
                        List<Address> addressList = null;
                        Geocoder geocoder = new Geocoder(ProfilesActivity.this, Locale.getDefault());


                        try {
                            addressList = geocoder.getFromLocationName(userAddress, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        MarkerOptions opt = new MarkerOptions().title(userAddress).
                                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                                .position(latLng)
                                .snippet("Morada do utilizador");
                        mMap.addMarker(opt);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                            @Override
                            public View getInfoWindow(Marker marker) {
                                return null;
                            }

                            @Override
                            public View getInfoContents(Marker marker) {

                                LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                View v = getLayoutInflater().inflate(R.layout.info_window, null);

                                TextView tvLocal = (TextView) v.findViewById(R.id.tv_local);
                                TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);


                                tvLocal.setLayoutParams(lparams);
                                tvSnippet.setLayoutParams(lparams);
                                tvLocal.setText(marker.getTitle());
                                tvSnippet.setText(marker.getSnippet());

                                return v;
                            }
                        });
                    }

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetUserTask = null;
        }


    }

    @Override
    public void onBackPressed() {
        String last = getIntent().getStringExtra("last");
        if(last.equals("AlterarDados") || last.equals("AlterarSenha"))
        startActivity(new Intent(ProfilesActivity.this,UserAreaActivity.class));
        else
            super.onBackPressed();
    }

}

