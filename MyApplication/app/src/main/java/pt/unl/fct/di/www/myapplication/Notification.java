package pt.unl.fct.di.www.myapplication;

/**
 * Created by Admin on 01/07/2017.
 */

public class Notification {
    String occId = "";
    String notID = "";

    public Notification(String occId,String notID){
        this.occId = occId;
        this.notID = notID;
    }

    public String getOccurrenceId(){
        return occId;
    }
    public String getNotificationId(){
        return notID;
    }

}
