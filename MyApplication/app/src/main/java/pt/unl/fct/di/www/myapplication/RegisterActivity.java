package pt.unl.fct.di.www.myapplication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends FragmentActivity {

    private View RegisterFormView, ProgressView;
    private EditText etNome, password, confPassword;
    private String etMorada ="";
    EditText morada;
    private AutoCompleteTextView etEmail;
    private UserRegisterTask mAuthTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        etNome = (EditText) findViewById(R.id.etNome);
        morada =(EditText) findViewById(R.id.etMorada);
        etEmail = (AutoCompleteTextView) findViewById(R.id.etEmail);


        password = (EditText) findViewById(R.id.password);
        confPassword = (EditText) findViewById(R.id.confirmPassword);


        BottomNavigationView topNavigationView = (BottomNavigationView) findViewById(R.id.topSubmeterNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(topNavigationView);
        Menu menuSubmeter = topNavigationView.getMenu();
        MenuItem menuSubmeterItem = menuSubmeter.getItem(0);
        menuSubmeterItem.setChecked(true);

        topNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_check:
                        attemptRegister();
                        break;
                }
                return false;
            }
        });




        RegisterFormView = findViewById(R.id.register_user_form);
        ProgressView = findViewById(R.id.login_progress);

        /*PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                etMorada= place.getAddress().toString();
            }

            @Override
            public void onError(Status status) {
                // TODO: Solucionar o erro.
            }
        });*/


    }



    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }


        etNome.setError(null);
        etEmail.setError(null);
        morada.setError(null);
        password.setError(null);
        confPassword.setError(null);


        String nome = etNome.getText().toString();
        String email = etEmail.getText().toString().replaceAll("\\s+$", "");
        String mor = morada.getText().toString();
        String pass = password.getText().toString();
        String confPass = confPassword.getText().toString();



        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(nome)) {
            etNome.setError(getString(R.string.error_field_required));
            focusView = etNome;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            etEmail.setError(getString(R.string.error_field_required));
            focusView = etEmail;
            cancel = true;

        }
        else if (!isEmailValid(email)) {
            etEmail.setError(getString(R.string.error_character_required));
            focusView = etEmail;
            cancel = true;
        }

        if(TextUtils.isEmpty(mor)){
            morada.setError(getString(R.string.error_field_required));
            focusView = morada;
            cancel = true;
        }


        if(!pass.matches(".*\\d.*")){
            password.setError(getString(R.string.error_integer_required));
            focusView = password;
            cancel = true;
        }



        if (TextUtils.isEmpty(pass) && !isPasswordValid(pass)) {
            password.setError(getString(R.string.error_field_required));
            focusView = password;
            cancel = true;
        }
        if (!isPasswordValid(pass)){
            password.setError(getString(R.string.error_password_length));
            focusView = password;
            cancel = true;
        }

        if (TextUtils.isEmpty(confPass) && !isPasswordValid(confPass)) {
            confPassword.setError(getString(R.string.error_field_required));
            focusView = confPassword;
            cancel = true;
        }
        else if(!TextUtils.equals(pass,confPass)) {
            confPassword.setError(getString(R.string.error_password_match));
            focusView = confPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else{

            showProgress(true);
            mAuthTask = new UserRegisterTask(email,pass,nome,mor,confPass);
            mAuthTask.execute((Void) null);
        }


    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4 ;
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            RegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            ProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {

                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), RegisterActivity.ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }


    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(RegisterActivity.ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }



    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(RegisterActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        etEmail.setAdapter(adapter);
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */

    public class UserRegisterTask extends AsyncTask<Void, Void, String> {
        private final String uNome;
        private final String uEmail;
        private final String uMorada;
        private final String uPassword;
        private final String uConfPass;

        UserRegisterTask(String email, String password, String nome, String morada, String confPass) {
            uEmail = email;
            uPassword = password;
            uNome = nome;
            uMorada = morada;
            uConfPass = confPass;

        }

        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if ((networkInfo == null) || !networkInfo.isConnected() ||
                    ((networkInfo.getType() != ConnectivityManager.TYPE_WIFI) &&
                            (networkInfo.getType() != ConnectivityManager.TYPE_MOBILE))) {
                cancel(true);
            }
        }


        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/register/");
                JSONObject json = new JSONObject();
                json.accumulate("name", uNome);
                json.accumulate("email", uEmail);
                json.accumulate("address", uMorada);
                json.accumulate("password", uPassword);
                json.accumulate("confirm", uConfPass);

                return RequestsREST.doPOST(url, json);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mAuthTask = null;
            String s = result;
            showProgress(false);

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    Log.i("RegisterActivity", response.toString());

                    SharedPreferences settings = getSharedPreferences("REGISTRATION", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("name", uNome);
                    editor.putString("email", uEmail);
                    editor.putString("address", uMorada);
                    editor.putString("password", uPassword);


                    editor.commit();
                    finish();

                    Toast.makeText(RegisterActivity.this, "Registo efetuado com sucesso.", Toast.LENGTH_SHORT).show();

                    Intent menuIntent = new Intent(RegisterActivity.this, FrontPage.class);
                    RegisterActivity.this.startActivity(menuIntent);
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                } catch (JSONException e) {
                    Log.e("Registration", e.toString());
                }
            } else
                Toast.makeText(RegisterActivity.this, "O registo falhou.", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

