package pt.unl.fct.di.www.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SubmeterOcMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMapLongClickListener {

    private String tok = "";
    private String user ="";
    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    ZoomControls zoom;
    Button markBt;
    double selLatitude;
    double selLongitude ;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    protected static final String TAG = "SubmeterOcMapActivity";
    private double myLongitude;
    private double myLatitude;
    private boolean check = false;
    private GetNotifications mNotifications = null;
    private int count =0;
    TextView badge;
    Polygon polygon;
    Polygon lisbonPolygon;
    private boolean inAlmada = false;
    private boolean inLisboa = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submeter_oc_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        tok = settings.getString("tokenID","");
        user = settings.getString("email","");


        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(15 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        BottomNavigationView topNavigationView = (BottomNavigationView) findViewById(R.id.topNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(topNavigationView);
        Menu topMenu = topNavigationView.getMenu();
        MenuItem topMenuItem = topMenu.getItem(1);
        topMenuItem.setChecked(true);
        badge = (TextView) findViewById(R.id.badge_notification);
        listNotifications();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.ic_submOc:
                        Intent submeterOcIntent = new Intent(SubmeterOcMapActivity.this, SubmeterOcMapActivity.class);
                        startActivity(submeterOcIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_listarOc:
                        Intent listarOcIntent = new Intent(SubmeterOcMapActivity.this, ListOcActivity.class);
                        startActivity(listarOcIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_menu:
                        Intent menuIntent = new Intent(SubmeterOcMapActivity.this, UserAreaActivity.class);
                        startActivity(menuIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_duvidas:
                        Intent duvidasIntent = new Intent(SubmeterOcMapActivity.this, DuvidasActivity.class);
                        startActivity(duvidasIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_notifications:
                        Intent intent = new Intent(SubmeterOcMapActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition( 0, 0);
                        break;

                }
                return false;
            }
        });

        topNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_submit:
                        if(check && (inLisboa || inAlmada)) {
                            Intent sub = new Intent(SubmeterOcMapActivity.this, SubmeterOcActivity.class);

                            if(inLisboa)
                                sub.putExtra("absLocal","Lisboa");
                            else if(inAlmada)
                                sub.putExtra("absLocal","Almada");

                            startActivity(sub);
                            overridePendingTransition( 0, 0);
                            break;
                        }
                        else
                            Toast.makeText(SubmeterOcMapActivity.this, "Não selecionou a localização da ocorrência.", Toast.LENGTH_SHORT).show();
                        break;


                    case R.id.ic_cancel:
                        mMap.clear();
                        createPolygon();
                        check =false;
                }
                return false;
            }
        });


        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_COUNTRY)
                .setCountry("PT")
                .build();

        autocompleteFragment.setFilter(autocompleteFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                LatLng pl = place.getLatLng();
                inAlmada =  PolyUtil.containsLocation(pl, polygon.getPoints(), true);
                inLisboa =  PolyUtil.containsLocation(pl, lisbonPolygon.getPoints(), true);

                if(inAlmada || inLisboa) {
                    selLongitude = pl.longitude;
                    selLatitude= pl.latitude;

                    if (place != null && !place.toString().equals("")) {
                        List<android.location.Address> addressList = null;
                        Geocoder geocoder = new Geocoder(SubmeterOcMapActivity.this, Locale.getDefault());

                        try {
                            addressList = geocoder.getFromLocationName(place.getAddress().toString(), 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        mMap.clear();
                        createPolygon();
                        mMap.addMarker(new MarkerOptions().position(latLng).title("Localização escolhida"));
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));


                        if (address.getLatitude() != 0 ||  address.getLongitude() != 0) {
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(address.getLatitude() ,
                                        address.getLongitude(), 1);
                                String pos = addresses.get(0).getAddressLine(0);
                                SharedPreferences settings = getSharedPreferences("LOCATION", 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("address", pos);
                                editor.commit();
                                check = true;
                                Toast.makeText(SubmeterOcMapActivity.this,pos, Toast.LENGTH_LONG).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            Toast.makeText(SubmeterOcMapActivity.this, "latitude and longitude are null",
                                    Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                    else
                    Toast.makeText(SubmeterOcMapActivity.this, "A localização escolhida está fora da área.",
                            Toast.LENGTH_LONG).show();


            }


            @Override
            public void onError(Status status) {
                // TODO: Solucionar o erro.
                Log.i(TAG, "Ocorreu um erro: " + status);
            }
        });
        //set to balanced power accuracy on real device

        zoom = (ZoomControls) findViewById(R.id.zcZoom);
        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());

            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());

            }
        });


        markBt = (Button) findViewById(R.id.btMark);
        markBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng myLocation = new LatLng(myLatitude, myLongitude);
                mMap.clear();
                createPolygon();
                inAlmada =  PolyUtil.containsLocation(myLocation, polygon.getPoints(), true);
                inLisboa =  PolyUtil.containsLocation(myLocation, lisbonPolygon.getPoints(), true);

                if(inAlmada || inLisboa) {
                    mMap.addMarker(new MarkerOptions().position(myLocation).title("A minha localização"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16));
                    try {
                        Geocoder geocoder;
                        geocoder = new Geocoder(SubmeterOcMapActivity.this, Locale.getDefault());

                        if (myLatitude != 0 || myLongitude != 0) {
                            List<Address> addresses = geocoder.getFromLocation(myLatitude ,
                                    myLongitude, 1);
                            String address = addresses.get(0).getAddressLine(0);
                            SharedPreferences settings = getSharedPreferences("LOCATION", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("address", address);
                            editor.commit();
                            check = true;
                            Toast.makeText(SubmeterOcMapActivity.this,address, Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(SubmeterOcMapActivity.this, "latitude and longitude are null",
                                    Toast.LENGTH_LONG).show();

                        }


                        } catch (IOException e) {
                        e.printStackTrace();
                        }
                }

                else
                    Toast.makeText(SubmeterOcMapActivity.this, "A localização escolhida está fora da área.",
                            Toast.LENGTH_SHORT).show();



            }
        });



    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(this);
        createPolygon();

         LatLng loc = new LatLng(38.6765, -9.1651);
         mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
         mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc,10));



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.clear();
        createPolygon();
        inAlmada =  PolyUtil.containsLocation(latLng, polygon.getPoints(), true);
        inLisboa =  PolyUtil.containsLocation(latLng, lisbonPolygon.getPoints(), true);

        if(inAlmada || inLisboa) {
            mMap.addMarker(new MarkerOptions().position(latLng).title("A minha localização"));

            try {
                Geocoder geocoder;
                geocoder = new Geocoder(SubmeterOcMapActivity.this, Locale.getDefault());

                if (latLng.latitude != 0 || latLng.longitude != 0) {
                    List<Address> addresses = geocoder.getFromLocation(latLng.latitude,
                            latLng.longitude, 1);
                    String address = addresses.get(0).getAddressLine(0);
                    SharedPreferences settings = getSharedPreferences("LOCATION", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("address", address);
                    editor.commit();
                    check = true;
                    Toast.makeText(SubmeterOcMapActivity.this, address, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SubmeterOcMapActivity.this, "latitude and longitude are null",
                            Toast.LENGTH_LONG).show();

                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            Toast.makeText(this, "A localização escolhida está fora da área.", Toast.LENGTH_SHORT).show();

    }


        @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection Failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        myLatitude = location.getLatitude();
        myLongitude = location.getLongitude();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            requestLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }


    private void onBackDialog()
    {
        AlertDialog.Builder alertDialog =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("Deseja sair da página?")
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        SubmeterOcMapActivity.super.onBackPressed();
                        //dialog.dismiss();
                    }

                });



        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog.create().show();

    }

    @Override
    public void onBackPressed() {
        if(check == true)
            onBackDialog();
        else
            super.onBackPressed();
    }





    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }

    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + user);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {
                        //if (i == notificationsArray.length)
                        //  resize();

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }

    private void createPolygon(){

        polygon = mMap.addPolygon(new PolygonOptions()
                .add(     new LatLng(38.551295, -9.185944)
                        , new LatLng(38.551564, -9.181824)
                        , new LatLng(38.565167, -9.182339)
                        , new LatLng(38.574026, -9.157963)
                        , new LatLng(38.60070654, -9.17658806)
                        , new LatLng(38.60288653, -9.17564392)
                        , new LatLng(38.60442926, -9.17055845)
                        , new LatLng(38.6118668, -9.17826176)
                        , new LatLng(38.65448275, -9.12927389)
                        , new LatLng(38.68852463, -9.14712667)
                        , new LatLng(38.67485621, -9.24119711)
                        , new LatLng(38.66306176, -9.26076651)).strokeColor(Color.RED));

        lisbonPolygon = mMap.addPolygon(new PolygonOptions()
                .add(     new LatLng(38.69497609, -9.22963048)
                        , new LatLng(38.69212898, -9.22289278)
                        , new LatLng(38.6927989, -9.21937372)
                        , new LatLng(38.69132507, -9.21645547)
                        , new LatLng(38.69989906, -9.16349794)
                        , new LatLng(38.7064772, -9.13356543)
                        , new LatLng(38.70934392, -9.1267624)
                        , new LatLng(38.7264341, -9.10474777)
                        , new LatLng(38.75139255, -9.09208681)
                        , new LatLng(38.79490145, -9.08998489)
                        , new LatLng(38.79680796, -9.09607887)
                        , new LatLng(38.79660728, -9.09925461)
                        , new LatLng(38.78051739, -9.10007)
                        , new LatLng(38.78864641, -9.11603451)
                        , new LatLng(38.79159003, -9.14513111)
                        , new LatLng(38.80215929, -9.14598942)
                        , new LatLng(38.75036833, -9.21023369)
                        , new LatLng(38.72184719, -9.22070503)).strokeColor(Color.BLUE));


        lisbonPolygon.setClickable(true);
        lisbonPolygon.setVisible(true);
        polygon.setClickable(true);
        polygon.setVisible(true);

        mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {
            }
        });
    }



}