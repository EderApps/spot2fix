package pt.unl.fct.di.www.myapplication;
import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;


public class SubmeterOcActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    private UserSubmitTask mSubmTask = null;
    private static final String SERVER_ADDRESS = "https://fit-sanctum-159416.appspot.com/gcs/fit-sanctum-159416.appspot.com/";
    protected static final int SELECTED_PICTURE = 100;
    protected static final int TAKE_PICTURE = 200;
    protected static final int REQUEST_EXTERNAL_STORAGE_RESULT = 1;
    Uri uri;
    private GetNotifications mNotifications = null;
    private int count =0;
    private boolean exit = false;
    TextView badge;
    private imageToGCS img = null;
    private ProgressDialog progressDialog;

    private String[] SPINNERLIST = {"Inundação", "Excesso de Neve", "Lixo a transbordar de caixote", "Animais Mortos", "Buraco no Chão",
            "Sinal Danificado", "Passeio Danificado", "Material do Parque danificado", "Veículo Abandonado", "Bicicleta Abandonada", "Estacionamento Ilegal",
            "Semáforo", "Luzes da Rua", "Luzes do Parque", "Graffiti Ilegal", "Risco de queda"};

    private EditText etDescricao;
    private TextView tv;
    private ImageButton btPasta;
    private ImageButton btImagem;
    private String tipo = null;
    private String urgencia = null;
    private String id = "";
    private String creator = "";
    private String local = "";
    private String filename = "";
    private String imagePath ="";
    private String absoluteLocal="";
    Bitmap bitmap;
    private File photoFile = null;
    private RadioGroup grupo;
    private RadioButton rPurgente;
    private RadioButton rUrgente;
    private RadioButton rMurgente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submeter_oc);

        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        absoluteLocal = getIntent().getStringExtra("absLocal");

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_submOc:
                        Intent submeterOcIntent = new Intent(SubmeterOcActivity.this, SubmeterOcMapActivity.class);
                        startActivity(submeterOcIntent);
                        overridePendingTransition(0, 0);

                        break;
                    case R.id.ic_listarOc:
                        Intent listarOcIntent = new Intent(SubmeterOcActivity.this, ListOcActivity.class);
                        startActivity(listarOcIntent);
                        overridePendingTransition(0, 0);

                        break;
                    case R.id.ic_menu:
                        Intent menuIntent = new Intent(SubmeterOcActivity.this, UserAreaActivity.class);
                        startActivity(menuIntent);
                        overridePendingTransition(0, 0);

                        break;
                    case R.id.ic_duvidas:
                        Intent duvidasIntent = new Intent(SubmeterOcActivity.this, DuvidasActivity.class);
                        startActivity(duvidasIntent);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.ic_notifications:
                        Intent intent = new Intent(SubmeterOcActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        break;

                }
                return false;
            }
        });


        BottomNavigationView topNavigationView = (BottomNavigationView) findViewById(R.id.topSubmeterNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(topNavigationView);
        Menu menuSubmeter = topNavigationView.getMenu();
        MenuItem menuSubmeterItem = menuSubmeter.getItem(0);
        menuSubmeterItem.setChecked(true);

        topNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_check:
                        if(!filename.equals("")){
                            uploadImage();
                        }

                        attemptSubmit();


                        break;
                }
                return false;
            }
        });


        etDescricao = (EditText) findViewById(R.id.etDescricao);
        btPasta = (ImageButton) findViewById(R.id.btPasta);
        btImagem = (ImageButton) findViewById(R.id.btImagem);
        grupo = (RadioGroup) findViewById(R.id.grupo);
        tv = (TextView) findViewById(R.id.idText);
        rPurgente = (RadioButton) findViewById(R.id.rPurgente);
        rUrgente = (RadioButton) findViewById(R.id.rUrgente);
        rMurgente = (RadioButton) findViewById(R.id.rMurgente);
        id = UUID.randomUUID().toString();
        SharedPreferences log = getSharedPreferences("AUTHENTICATION", 0);
        SharedPreferences pos = getSharedPreferences("LOCATION", 0);
        creator = log.getString("email","");
        local = pos.getString("address","");
        rPurgente.setOnClickListener(this);
        rUrgente.setOnClickListener(this);
        rMurgente.setOnClickListener(this);

        badge = (TextView) findViewById(R.id.badge_notification);
        listNotifications();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        Spinner spinner = (Spinner) findViewById(R.id.spinnerTipo);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(this);

        btPasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,SELECTED_PICTURE);

            }
        });

        btImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });


    }


    private void uploadImage(){
        progressDialog = new ProgressDialog(SubmeterOcActivity.this);
        progressDialog.setTitle("Upload");
        progressDialog.setMessage("Uploading Image...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        img = new imageToGCS();
        img.execute((Void) null);
    }



    @Override
    public void onClick(View v) {
        int checkedRadioButtonId = grupo.getCheckedRadioButtonId();

        switch (checkedRadioButtonId) {
            case R.id.rPurgente:
                if (rPurgente.isChecked()) {
                    urgencia = rPurgente.getText().toString();
                }
                break;

            case R.id.rUrgente:
                if (rUrgente.isChecked()) {
                    urgencia = rUrgente.getText().toString();
                }
                break;

            case R.id.rMurgente:
                if (rMurgente.isChecked()) {
                    urgencia = rMurgente.getText().toString();
                }
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
        tipo = selected;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void attemptSubmit() {
        if (mSubmTask != null) {
            return;
        }


        etDescricao.setError(null);


        String ocID = id;
        String textDescricao = etDescricao.getText().toString();
        String tipoOC = tipo;
        String urg = urgencia;
        String creat = creator;
        String textLocal = local;
        String file = filename;
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(textDescricao)) {
            etDescricao.setError("Campo obrigatório.");
            focusView = etDescricao;
            cancel = true;
        }

        if (urgencia.equals(null)) {
            cancel = true;
        }

        if (tipo.equals(null)) {
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            mSubmTask = new UserSubmitTask(ocID, textLocal, textDescricao, tipoOC, urg, creat, file);
            mSubmTask.execute((Void) null);
        }


    }


    public class UserSubmitTask extends AsyncTask<Void, Void, String> {


        private final String descricao;
        private final String idOcc;
        private final String urgencia;
        private final String tipo;
        private final String localPos;
        private final String creator;
        private final String filename;

        UserSubmitTask(String id,String local, String descricao,String tipo,String urgencia,String creator,String filename) {

            idOcc = id;
            this.descricao = descricao;
            this.tipo=tipo;
            this.urgencia=urgencia;
            localPos = local;
            this.creator = creator;
            this.filename=filename;
        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                SharedPreferences sp = getSharedPreferences("AUTHENTICATION",0);

                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/registerOccurrence/"+ creator+"/"+absoluteLocal);

                json.accumulate("occID",idOcc);
                json.accumulate("description", descricao);
                json.accumulate("typeOc", tipo);
                json.accumulate("urgency", urgencia);
                json.accumulate("creator",creator);
                json.accumulate("location", localPos);
                json.accumulate("fileName", filename);


                return RequestsREST.doPOST(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mSubmTask = null;
            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    Log.i("SubmeterOcActivity", response.toString());

                    SharedPreferences settings = getSharedPreferences("SUBMITIONS", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("description",descricao);
                    editor.putString("occID",idOcc);
                    editor.putString("typeOc",tipo);
                    editor.putString("location",localPos);
                    editor.putString("urgency",urgencia);
                    editor.putString("creator",creator);
                    editor.putString("filename",filename);

                    editor.commit();


                    finish();
                    Toast.makeText(SubmeterOcActivity.this, "Submissão efetuada com sucesso.", Toast.LENGTH_SHORT).show();
                    Intent menuIntent = new Intent(SubmeterOcActivity.this, ListOcActivity.class);
                    SubmeterOcActivity.this.startActivity(menuIntent);
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);


                } catch (JSONException e) {
                    Log.e("Submition", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mSubmTask = null;
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == TAKE_PICTURE){

            filename = photoFile.getAbsolutePath().substring(photoFile.getAbsolutePath().lastIndexOf("/") + 1);
            bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
            tv.setText(filename);

        }

        else if (resultCode == RESULT_OK && requestCode == SELECTED_PICTURE && data != null) {
            uri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            cursor.close();

            filename = imagePath.substring(imagePath.lastIndexOf("/")+1);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            tv.setText(filename);
        }
    }


    public void takePhoto() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp();
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this,
                            "External storage permission required to save images",
                            Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_EXTERNAL_STORAGE_RESULT);
            }
        } else {
            callCameraApp();
        }
    }


    public void callCameraApp() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));

        startActivityForResult(callCameraApplicationIntent, TAKE_PICTURE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == REQUEST_EXTERNAL_STORAGE_RESULT) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callCameraApp();
            } else {
                Toast.makeText(this,
                        "External write permission has not been granted, cannot saved images",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private  File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);

        return image;

    }

    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] imgBytes =  byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes,Base64.DEFAULT);
    }


    public String resizeBase64Image(String base64image){
        byte [] encodeByte=Base64.decode(base64image.getBytes(),Base64.DEFAULT);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);


        if(image.getHeight() <= 400 && image.getWidth() <= 400){
            return base64image;
        }
        int altura = image.getHeight();
        int largura = image.getWidth();

        if(altura > largura) {
            largura = (400*largura)/altura;
            altura = 400;
        }
        else if(altura < largura) {
            altura = (400*altura)/largura;
            largura = 400;
        }
        else{
            altura = 400;
            largura = 400;
        }

        image = Bitmap.createScaledBitmap(image,largura , altura, false);

        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG,100, baos);

        byte [] b=baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    public class imageToGCS extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL(SERVER_ADDRESS + filename);
                json.accumulate("image",resizeBase64Image(imageToString(bitmap)));
                return RequestsREST.doPOSTImage(url, json);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            img = null;

            if(result != null) {
                progressDialog.dismiss();
            }else{
                Toast.makeText(SubmeterOcActivity.this, "erro", Toast.LENGTH_SHORT).show();

            }
        }

    }


    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + creator);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }

    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }

}