package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class CommentsActivity extends AppCompatActivity {

    ListAdapter listAdapter;
    private String[] commentsList;
    private GetCommentsTask mGetComments = null;
    private ListView commentListView;
    private String occID = "";
    private String creator = "";
    EditText commentText;
    Button btComment;
    private String cursor = "";
    TextView tv;
    private boolean flag_loading;
    private UserCommentTask mCommentTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        SharedPreferences sp = getSharedPreferences("AUTHENTICATION", 0);
        commentText = (EditText) findViewById(R.id.textComment);;
        btComment = (Button) findViewById(R.id.btComment);
        creator = sp.getString("email", "");
        commentsList = new String[0];
        cursor = "first";
        flag_loading = false;
        tv = new TextView(CommentsActivity.this);

        Intent i = getIntent();
        occID = i.getStringExtra("occurrenceID");

        commentListView = (ListView) findViewById(R.id.lComments);
        CommentsActivity.this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        BottomNavigationView topNavigationView = (BottomNavigationView) findViewById(R.id.topCommentNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(topNavigationView);
        Menu menuSubmeter = topNavigationView.getMenu();
        MenuItem menuSubmeterItem = menuSubmeter.getItem(1);
        menuSubmeterItem.setChecked(false);

        topNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.back:
                        onBackPressed();
                        break;
                }
                return false;
            }
        });

        getComments(occID,cursor);

        commentListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount - 1 && commentListView.getCount() >= 4 && flag_loading == false) {
                    flag_loading = true;
                    getComments(occID,cursor);
                }
            }
        });

        btComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addComment();
            }
        });

    }


    private void getComments(String o,String c) {
        mGetComments = new GetCommentsTask(o,c);
        mGetComments.execute((Void) null);
    }

    private void addComment() {
        if (mCommentTask != null) {
            return;
        }

        commentText.setError(null);
        String mComment = commentText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(mComment)) {
            commentText.setError("Não escreveu nenhum comentário");
            focusView = commentText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            mCommentTask = new UserCommentTask(occID, creator, mComment);
            mCommentTask.execute((Void) null);
        }
    }


    public class GetCommentsTask extends AsyncTask<Void, Void, String> {
        private String oc = "";
        private String c = "";


        GetCommentsTask(String oc, String c) {
            this.oc = oc;
            this.c = c;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllCommentsPage/" + oc +"/"+c);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetComments = null;
            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    int i = commentsList.length;
                    for (int j = 0; j < arr.length(); j++) {
                        if (i == commentsList.length)
                            resize();

                        JSONObject jObj = arr.getJSONObject(j);

                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        JSONObject time = propertyMap.getJSONObject("comment_creation_time");
                        String date = time.getString("value");
                        String commentText = propertyMap.getString("comment_text");
                        String commentCreator = propertyMap.getString("comment_creator");

                        commentsList[i] = commentCreator + "," + " " + date + "\n" + " " +commentText;

                        i++;
                    }


                    if(response.length()!=0) {
                        flag_loading = false;
                        listAdapter = new CommentsAdapter(CommentsActivity.this, commentsList);
                        Parcelable state = commentListView.onSaveInstanceState();
                        commentListView.setAdapter(listAdapter);
                        commentListView.onRestoreInstanceState(state);
                        tv.setText(cursor);

                    }


                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetComments = null;
        }


    }



    public class UserCommentTask extends AsyncTask<Void, Void, String> {

        private String oc;
        private String user;
        private String commentMessage;

        UserCommentTask(String oc, String user, String commentMessage) {

            this.oc = oc;
            this.user = user;
            this.commentMessage = commentMessage;
        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/addComment/" + oc);

                json.accumulate("creator", user);
                json.accumulate("commentmessage", commentMessage);

                return RequestsREST.doPOST(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mCommentTask = null;
            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    Log.i("SubmeterOcActivity", response.toString());

                    Toast.makeText(CommentsActivity.this, "Comentário adicionado.", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);


                } catch (JSONException e) {
                    Log.e("Submition", e.toString());
                }
            }

        }
        @Override
        protected void onCancelled() {
            mCommentTask = null;
        }
    }


    private void resize() {
        String[] newArray = new String[commentsList.length + 1];
        for (int i = 0; i < commentsList.length; i++)
            newArray[i] = commentsList[i];

        commentsList = newArray;
    }

}


