package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;


public class AlterarDadosActivity extends AppCompatActivity {

    private String user;
    EditText name;
    EditText address;
    Button submit;
    private UserUpdateTask mUpdateProfileTask = null;
    private GetUserTask mGetUserTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_dados);
        SharedPreferences sp = getSharedPreferences("AUTHENTICATION",0);
        user = sp.getString("email","");
        submit = (Button) findViewById(R.id.btAlterar);
        name =(EditText) findViewById(R.id.editName);
        address =(EditText) findViewById(R.id.editAddress);
        getUser();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

    }



       private void updateProfile(){

        if (mUpdateProfileTask != null) {
            return;
        }

        name.setError(null);
        address.setError(null);

        String nome = name.getText().toString().replaceAll("\\s+$", "");
        String userAddress = address.getText().toString().replaceAll("\\s+$", "");

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(nome)) {
            name.setError(getString(R.string.error_field_required));
            focusView = name;
            cancel = true;
        }

        if(TextUtils.isEmpty(userAddress)) {
            address.setError(getString(R.string.error_field_required));
            focusView = name;
            cancel = true;
        }

        if(nome.matches("^\\s*$")){
               name.setError("O nome não pode conter espaços.");
               focusView = name;
               cancel = true;
           }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {
            mUpdateProfileTask = new UserUpdateTask(user, userAddress, nome);
            mUpdateProfileTask.execute();
        }
    }



    public class UserUpdateTask extends AsyncTask<Void, Void, String> {


        private String userID;
        private String userAddress;
        private String userName;


        UserUpdateTask(String id, String address, String username) {

            userID = id;
            userAddress = address;
            userName = username;
        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {

                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/updateProfile/" + userID + "/" + userName + "/" + userAddress);

                return RequestsREST.doPUT(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mUpdateProfileTask = null;
            if (result != null) {
                    Toast.makeText(AlterarDadosActivity.this, "Dados alterados com êxito!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AlterarDadosActivity.this,ProfilesActivity.class);
                i.putExtra("last","AlterarDados");
                startActivity(i);
            }
        }

    }



    private void getUser(){

        mGetUserTask = new GetUserTask(user);
        mGetUserTask.execute((Void) null);
    }



    public class GetUserTask extends AsyncTask<Void, Void, String> {
        private String userID = "";

        GetUserTask(String userID) {
            this.userID = userID;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getUser/"+userID);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetUserTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    JSONObject propertyMap = response.getJSONObject("propertyMap");
                    String userName = propertyMap.getString("user_name");
                    String userAddress = propertyMap.getString("user_address");
                    name.setText(userName ,TextView.BufferType.EDITABLE);
                    address.setText(userAddress,TextView.BufferType.EDITABLE);
                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetUserTask = null;
        }

    }

}
