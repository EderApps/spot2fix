package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class DefinicoesActivity extends AppCompatActivity  {

    private ListView l;
    ListAdapter listAdapter;
    String[] options = {"Alterar Dados","Alterar Senha"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definicoes);
        l = (ListView) findViewById(R.id.lv);

        listAdapter = new SimpleAdapter(DefinicoesActivity.this,options);
        l.setAdapter(listAdapter);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                        Intent intent = new Intent(DefinicoesActivity.this, AlterarDadosActivity.class);
                        startActivity(intent);
                }
                else{
                    Intent intent = new Intent(DefinicoesActivity.this, AlterarSenhaActivity.class);
                    startActivity(intent);
                }
            }
        });
    }







}
