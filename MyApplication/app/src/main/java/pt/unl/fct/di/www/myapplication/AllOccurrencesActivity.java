package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class AllOccurrencesActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private Occurrence[] occurrences;
    private UserListOcurrencesTask mList = null;
    private String cursor;
    Button button;
    private boolean flag_loading;
    TextView tv;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_occurrences_activity);

        button = (Button) findViewById(R.id.allOcButton);
        cursor = "first";
        flag_loading = false;
        tv = new TextView(AllOccurrencesActivity.this);
        occurrences = new Occurrence[0];
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        listOcorrences(cursor);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_loading = true;
                listOcorrences(cursor);
            }
        });

    }


    public void listOcorrences(String c) {
        progressDialog = new ProgressDialog(AllOccurrencesActivity.this);
        progressDialog.setTitle("Carregar");
        progressDialog.setMessage("A carregar...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        mList = new UserListOcurrencesTask(c);
        mList.execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    public class UserListOcurrencesTask extends AsyncTask<Void, Void, String> {

        private String c;
        UserListOcurrencesTask(String c) {
            this.c = c;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllOccurrences/"+c);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mList = null;

            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    if(arr.length() ==0 )
                        progressDialog.dismiss();

                    int i = occurrences.length;

                    if(arr.length() == 0 && i ==0 )
                        Toast.makeText(AllOccurrencesActivity.this, "Sem ocorrências registadas.", Toast.LENGTH_SHORT).show();

                    for (int j = 0; j < arr.length(); j++) {

                        if (i == occurrences.length)
                            resize();


                        JSONObject jObj = arr.getJSONObject(j);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                        String ocId = propertyMap.getString("occurrence_id");
                        String ocType = propertyMap.getString("occurrence_type");
                        String creator = propertyMap.getString("occurrence_creator");
                        String masterType = propertyMap.getString("occurrence_type_master");
                        String date = key.getString("value");
                        String location = propertyMap.getString("occurrence_location");
                        String description = propertyMap.getString("occurrence_description");
                        int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                        String status = propertyMap.getString("occurrence_status");
                        String rating = propertyMap.getString("occurrence_rating");
                        if (!propertyMap.isNull("occurrence_filename")) {
                            String filename = propertyMap.getString("occurrence_filename");
                            occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                    description, likes, filename, status, rating);
                        } else
                            occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                    description, likes, "", status, rating);


                        if (arr.length() != 0) {
                            flag_loading = false;

                            if (occurrences[i].getLocation() != null && !occurrences[i].getLocation().equals("")) {
                                List<Address> addressList = null;
                                Geocoder geocoder = new Geocoder(AllOccurrencesActivity.this, Locale.getDefault());


                                try {
                                    addressList = geocoder.getFromLocationName(occurrences[i].getLocation(), 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Address address = addressList.get(0);
                                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                                if (occurrences[i].getStatus().equals("Resolvido")) {
                                    MarkerOptions opt = new MarkerOptions().title(occurrences[i].getType()).

                                            icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_fixed_logo))
                                            .position(latLng)
                                            .snippet(occurrences[i].getDescription() + "\n" + occurrences[i].getLocation() +
                                                    "\n" + occurrences[i].getStatus());
                                    mMap.addMarker(opt);
                                } else {

                                    MarkerOptions opt = new MarkerOptions().title(occurrences[i].getType()).

                                            icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_red_logo))
                                            .position(latLng)
                                            .snippet(occurrences[i].getDescription() + "\n" + occurrences[i].getLocation() +
                                                    "\n" + occurrences[i].getStatus());
                                    mMap.addMarker(opt);

                                }
                                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                    @Override
                                    public View getInfoWindow(Marker marker) {
                                        return null;
                                    }

                                    @Override
                                    public View getInfoContents(Marker marker) {

                                        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                                                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                        View v = getLayoutInflater().inflate(R.layout.info_window, null);

                                        TextView tvLocal = (TextView) v.findViewById(R.id.tv_local);
                                        TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);
                                        //ImageView iv = (ImageView) v.findViewById(R.id.infoImage);
                                        //loadImageFromURL("https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+file,iv);


                                        tvLocal.setLayoutParams(lparams);
                                        tvSnippet.setLayoutParams(lparams);
                                        //  iv.setLayoutParams(lparams);
                                        tvLocal.setText(marker.getTitle());
                                        tvSnippet.setText(marker.getSnippet());

                                        return v;
                                    }
                                });
                            }
                            progressDialog.dismiss();
                            LatLng loc = new LatLng(38.6765, -9.1651);
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc,10));
                            tv.setText(cursor);
                        }
                    }


                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mList = null;
        }


    }


    private void resize() {
        Occurrence[] newArray = new Occurrence[occurrences.length + 1];
        for (int i = 0; i < occurrences.length; i++)
            newArray[i] = occurrences[i];

        occurrences = newArray;
    }


    private void loadImageFromURL(String url, ImageView iv) {
        Picasso.with(this).load(url).into(iv, new com.squareup.picasso.Callback() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });
    }
}
