package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class AlterarSenhaActivity extends AppCompatActivity {
    EditText etPassword;
    EditText newPassword;
    EditText newPassConfirm;
    private String user = "";
    private UserUpdatePasswordTask mUpdatePasswordTask = null;
    Button chPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_senha);
        SharedPreferences sp = getSharedPreferences("AUTHENTICATION",0);
        user = sp.getString("email","");
        chPassword = (Button) findViewById(R.id.altPassword);
        etPassword = (EditText) findViewById(R.id.editPass);
        newPassword = (EditText) findViewById(R.id.editNovaPass);
        newPassConfirm = (EditText) findViewById(R.id.editConfirm);


        chPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });
    }


    private void updatePassword(){

        if (mUpdatePasswordTask != null) {
            return;
        }

        etPassword.setError(null);
        newPassword.setError(null);
        newPassConfirm.setError(null);

        String pass= etPassword.getText().toString();
        String newPass = newPassword.getText().toString();
        String confirm = newPassConfirm.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(pass)) {
            etPassword.setError(getString(R.string.error_field_required));
            focusView = etPassword;
            cancel = true;
        }

        if(TextUtils.isEmpty(newPass)) {
            newPassword.setError(getString(R.string.error_field_required));
            focusView = newPassword;
            cancel = true;
        }

        if(!newPass.matches(".*\\d.*")){
            newPassword.setError(getString(R.string.error_integer_required));
            focusView = newPassword;
            cancel = true;
        }

        if(TextUtils.isEmpty(confirm)) {
            newPassConfirm.setError(getString(R.string.error_field_required));
            focusView = newPassConfirm;
            cancel = true;
        }

        if(TextUtils.isEmpty(confirm)) {
            newPassConfirm.setError(getString(R.string.error_field_required));
            focusView = newPassConfirm;
            cancel = true;
        }

        if (TextUtils.isEmpty(confirm) && !isPasswordValid(confirm)) {
            newPassConfirm.setError(getString(R.string.error_field_required));
            focusView = newPassConfirm;
            cancel = true;
        }

        if(!TextUtils.equals(newPass,confirm)) {
            newPassConfirm.setError(getString(R.string.error_password_match));
            focusView = newPassConfirm;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {
            mUpdatePasswordTask = new UserUpdatePasswordTask(user, pass, newPass);
            mUpdatePasswordTask.execute();
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4 ;
    }


    public class UserUpdatePasswordTask extends AsyncTask<Void, Void, String> {


        private String userID;
        private String password;
        private String nPassword;


        UserUpdatePasswordTask(String userID, String password, String nPassword) {

            this.userID = userID;
            this.password = password;
            this.nPassword = nPassword;
        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {

                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/updatePassword/" + userID + "/" + password + "/" + nPassword);

                return RequestsREST.doPUT(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mUpdatePasswordTask = null;
            if (result != null) {
                Toast.makeText(AlterarSenhaActivity.this, "Senha alterada com êxito!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AlterarSenhaActivity.this,ProfilesActivity.class);
                i.putExtra("last","AlterarSenha");
                startActivity(i);
            }
            else {
                etPassword.setError("Password incorreta.");
                etPassword.requestFocus();
            }
        }

    }

}

