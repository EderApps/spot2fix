package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class ResetPasswordActivity extends AppCompatActivity {

    private AutoCompleteTextView emailView;
    private Button resetButton;
    private RecoverTask mResetTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        emailView = (AutoCompleteTextView) findViewById(R.id.userEmail);
        resetButton = (Button) findViewById(R.id.resetButton);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

    }

    private void reset() {
        if (mResetTask != null) {
            return;
        }

        emailView.setError(null);

        // Store values at the time of the login attempt.
        String email = emailView.getText().toString().replaceAll("\\s+$", "");
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        }

        else {
            mResetTask = new RecoverTask(email);
            mResetTask.execute((Void) null);
        }


    }


    public class RecoverTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;

        RecoverTask(String email) {
            mEmail = email;
        }

        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/recover/"+mEmail);
                //json.accumulate("username", mEmail);
                return RequestsREST.doPOST(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mResetTask = null;

            if (result != null) {
                    Toast.makeText(ResetPasswordActivity.this, "Ser-lhe-á enviado um email para concluir o pedido.", Toast.LENGTH_SHORT).show();

                    Intent menuIntent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                    ResetPasswordActivity.this.startActivity(menuIntent);
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        }

        @Override
        protected void onCancelled() {
            mResetTask = null;
        }

    }


}
