package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class NotificationActivity extends AppCompatActivity {

    private ListView l;
    private ListAdapter listAdapter;
    private UserListNotificationsTask mNotifications = null;
    private UserGetOneOccTask mGetOne = null;
    private OpenNotification mOpenNotification = null;
    private String [] notificationsArray;
    private String[] stateArray;
    private Notification [] occIdArray = new Notification[1000];
    String user = "";
    private Occurrence occurrence;
    private int count =0;
    TextView badge;
    private String cursor = "";
    TextView tv;
    private boolean flag_loading = false;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        badge = (TextView) findViewById(R.id.badge_notification);
        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        user = settings.getString("email", "");
        cursor = "first";
        l = (ListView) findViewById(R.id.notificationsList);
        notificationsArray = new String[0];
        stateArray = new String[0];
        tv = new TextView(NotificationActivity.this);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_submOc:
                        Intent submeterOcIntent = new Intent(NotificationActivity.this, SubmeterOcMapActivity.class);
                        startActivity(submeterOcIntent);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.ic_listarOc:
                        Intent listarOcIntent = new Intent(NotificationActivity.this, ListOcActivity.class);
                        startActivity(listarOcIntent);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.ic_menu:
                        Intent menuIntent = new Intent(NotificationActivity.this, UserAreaActivity.class);
                        startActivity(menuIntent);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.ic_duvidas:
                        Intent duvidasIntent = new Intent(NotificationActivity.this, DuvidasActivity.class);
                        startActivity(duvidasIntent);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.ic_notifications:
                        Intent intent = new Intent(NotificationActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        break;

                }
                return false;
            }
        });


        listNotifications(user, cursor);


        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String oID = occIdArray[position].getOccurrenceId();
                String notID = occIdArray[position].getNotificationId();
                getOcc(oID);
                openNotification(notID);

            }
        });


        l.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 4 && flag_loading == false) {
                    flag_loading = true;
                    listNotifications(user, cursor);
                }
            }
        });
    }

    public void listNotifications(String us, String cr){
        addDialog();
        mNotifications = new UserListNotificationsTask(us, cr);
        mNotifications.execute();
    }
    public void getOcc(String oc) {
        mGetOne = new UserGetOneOccTask(oc);
        mGetOne.execute();
    }

    public class UserListNotificationsTask extends AsyncTask<Void, Void, String> {

        private String c;
        private String us;


        UserListNotificationsTask(String us, String c) {
            this.us = us;
            this.c = c;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsPage/" + us+ "/" + c);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    int i = notificationsArray.length;
                    if(arr.length() ==0)
                        progressDialog.dismiss();

                    if(i==0 && arr.length() ==0){
                        Toast.makeText(NotificationActivity.this, "Vazio.", Toast.LENGTH_SHORT).show();
                        l.setAdapter(null);
                    }
                    for (int j = 0; j < arr.length(); j++) {

                        if (i == notificationsArray.length)
                            resize();

                        if (i == stateArray.length)
                            resizeState();

                        JSONObject jObj = arr.getJSONObject(j);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        JSONObject key = jObj.getJSONObject("key");
                        String notID = key.getString("name");
                        JSONObject date = propertyMap.getJSONObject("not_date");
                        String dateValue = date.getString("value");
                        String message = propertyMap.getString("not_message");
                        String opened = propertyMap.getString("not_opened");
                        String occID = propertyMap.getString("not_occID");

                        occIdArray[i] = new Notification(occID, notID);
                        if (opened.equals("false")){
                            count++;
                            stateArray[i] = " NOVO ";
                        }
                        else
                            stateArray[i] = "";


                        notificationsArray[i] = dateValue + "\n\n" + message + "\n\n" +
                                "Foi resolvido!";


                        i++;
                    }



                    badge.setText(" " +count);
                    if(arr.length()!=0) {
                        flag_loading = false;
                        listAdapter = new NotificationAdapter(NotificationActivity.this, notificationsArray,stateArray);
                        Parcelable state = l.onSaveInstanceState();
                        progressDialog.dismiss();
                        l.setAdapter(listAdapter);
                        l.onRestoreInstanceState(state);
                        tv.setText(cursor);
                    }


                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }
    public class UserGetOneOccTask extends AsyncTask<Void, Void, String> {
            private String occID;

            UserGetOneOccTask(String occID) {
                this.occID=occID;
            }


            @Override
            protected void onPreExecute() {

                ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo == null || !networkInfo.isConnected() ||
                        (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                                && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                    cancel(true);

                }
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getOneOccurrence/" + occID);
                    return RequestsREST.doGET(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        @Override
        protected void onPostExecute(final String result) {
            mGetOne = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);

                        JSONObject propertyMap = response.getJSONObject("propertyMap");
                        JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                        String ocId = propertyMap.getString("occurrence_id");
                        String ocType = propertyMap.getString("occurrence_type");
                        String creator = propertyMap.getString("occurrence_creator");
                        String masterType = propertyMap.getString("occurrence_type_master");
                        String date = key.getString("value");
                        String location = propertyMap.getString("occurrence_location");
                        String description = propertyMap.getString("occurrence_description");
                        String rating = propertyMap.getString("occurrence_rating");
                        int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                        String status = propertyMap.getString("occurrence_status");
                        if(!propertyMap.isNull("occurrence_filename")){
                            String filename = propertyMap.getString("occurrence_filename");
                            occurrence = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                    description, likes,filename,status, rating);
                        }

                        else
                            occurrence= new Occurrence(ocId, creator, date, ocType, masterType, location,
                                    description, likes,"",status, rating);


                     Intent intent = new Intent(NotificationActivity.this, SingleOccurrence.class);

                        SharedPreferences loc = getSharedPreferences("LOCAL", 0);
                        SharedPreferences.Editor editor = loc.edit();
                        editor.putString("loc", occurrence.getLocation());
                        editor.commit();


                        SharedPreferences f = getSharedPreferences("FILE", 0);
                        SharedPreferences.Editor e = f.edit();
                        e.putString("file", occurrence.getFilename());
                        e.commit();

                        SharedPreferences occ = getSharedPreferences("OCC", 0);
                        SharedPreferences.Editor edit = occ.edit();
                        edit.putString("occID", occurrence.getId());
                        edit.commit();

                        SharedPreferences userCreator = getSharedPreferences("CREATOR", 0);
                        SharedPreferences.Editor editCreator = userCreator.edit();
                        editCreator.putString("creator", occurrence.getCreator());
                        editCreator.commit();


                        intent.putExtra("tipo", occurrence.getType());
                        intent.putExtra("descricao", occurrence.getDescription());
                        intent.putExtra("data", occurrence.getDate());
                        intent.putExtra("likes", Integer.toString(occurrence.getLikes()));
                        intent.putExtra("status", occurrence.getStatus());
                        intent.putExtra("rating",occurrence.getRating());

                        intent.putExtra("last","Notification");


                        startActivity(intent);

                    } catch (JSONException e) {
                    e.printStackTrace();
                }

                }
            }

            @Override
            protected void onCancelled() {
                mGetOne = null;
            }


        }

    public class OpenNotification extends AsyncTask<Void, Void, String> {


        private String notID;


        OpenNotification(String notID) {

           this.notID = notID;

        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {

                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/openNotification/" + notID);

                return RequestsREST.doPUT(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mOpenNotification = null;
            if (result != null) {

            }

        }

    }

    private void openNotification(String notID){
        mOpenNotification = new OpenNotification(notID);
        mOpenNotification.execute((Void)null);
    }


    private void resize() {
        String[] newArray = new String[notificationsArray.length + 1];
        for (int i = 0; i < notificationsArray.length; i++)
            newArray[i] = notificationsArray[i];

        notificationsArray = newArray;
    }

    private void resizeState() {
        String[] newArray = new String[stateArray.length + 1];
        for (int i = 0; i < stateArray.length; i++)
            newArray[i] = stateArray[i];

        stateArray = newArray;
    }

    private void addDialog(){
        progressDialog = new ProgressDialog(NotificationActivity.this);
        progressDialog.setTitle("Carregar");
        progressDialog.setMessage("A carregar...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    }

