package pt.unl.fct.di.www.myapplication;

import android.content.Context;

import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class CostumAdapter extends ArrayAdapter<String> {

    String [] filenames;
    String [] status;

    CostumAdapter(Context context, String[] occurrList,String[] filenames, String[] status){
        super(context,R.layout.custom_row, occurrList);
        this.filenames=filenames;
        this.status = status;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());

        String singleItem = getItem(position);
        String filename = filenames[position];
        String stat = status[position];
        View customView = inflater.inflate(R.layout.custom_row,parent,false);
        TextView text = (TextView) customView.findViewById(R.id.customText);
        ImageView image = (ImageView) customView.findViewById(R.id.customImage);
        TextView t = (TextView) customView.findViewById(R.id.tv);
        text.setText(singleItem);
        t.setText(stat);
        t.setTextColor(customView.getResources().getColor(R.color.whiteColor));
        if(stat.equals(" ABERTO "))
            t.setBackground(customView.getResources().getDrawable(R.drawable.custom_aberto));
        else
            t.setBackground(customView.getResources().getDrawable(R.drawable.custom_resolvido));

        if(!filename.equals(""))
        loadImageFromURL("https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename,image);

        else
        image.setImageResource(R.mipmap.ic_null_image);

        return customView;
    }
    


    private void loadImageFromURL(String url,ImageView iv) {
        Picasso.with(getContext()).load(url).into(iv, new com.squareup.picasso.Callback() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });


    }
}
