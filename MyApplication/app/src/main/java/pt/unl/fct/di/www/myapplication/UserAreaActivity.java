package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class UserAreaActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private UserLogoutTask mAuthTokTask = null;
    private GetNotifications mNotifications = null;
    private String tok = "";
    private TextView welcome;
    private String user = "";
    private TextView badge;
    private String[] notificationsArray;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        tok = settings.getString("tokenID","");
        user = settings.getString("email","");
        notificationsArray = new String[0];

        setContentView(R.layout.activity_main_drawer);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.setDrawerListener(toggle);
        toggle.syncState();

            /*View v = getLayoutInflater().inflate(R.layout.app_bar_main_drawer,null);
            tv = (TextView) v.findViewById(R.id.emailTextView);
            Toast.makeText(this, email , Toast.LENGTH_SHORT).show();
            tv.setText(email);*/




        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        badge = (TextView) findViewById(R.id.badge_notification);


        listNotifications();


        welcome = (TextView) findViewById(R.id.welcomeText);
        welcome.setText("Bem vindo," + " " + user + "!");

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_submOc:
                        Intent submeterOcIntent = new Intent(UserAreaActivity.this, SubmeterOcMapActivity.class);
                        startActivity(submeterOcIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_listarOc:
                        Intent listarOcIntent = new Intent(UserAreaActivity.this, ListOcActivity.class);
                        startActivity(listarOcIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_menu:
                        Intent menuIntent = new Intent(UserAreaActivity.this, UserAreaActivity.class);
                        startActivity(menuIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_duvidas:
                        Intent duvidasIntent = new Intent(UserAreaActivity.this, DuvidasActivity.class);
                        startActivity(duvidasIntent);
                        overridePendingTransition( 0, 0);
                        break;
                    case R.id.ic_notifications:
                        Intent intent = new Intent(UserAreaActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition( 0, 0);
                        break;

                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            onBackDialog();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_drawer, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

         if (id == R.id.nav_all_occurrences) {
            item.setChecked(false);
            Intent i = new Intent(UserAreaActivity.this, AllOccurrencesActivity.class);
            UserAreaActivity.this.startActivity(i);
            overridePendingTransition(0, 0);

        }  else if (id == R.id.nav_profile) {
            item.setChecked(false);
            Intent i = new Intent (UserAreaActivity.this,ProfilesActivity.class);
            i.putExtra("last"," ");
            UserAreaActivity.this.startActivity(i);
            overridePendingTransition( 0, 0);

        }else if (id == R.id.nav_settings) {
            item.setChecked(false);
            startActivity(new Intent(UserAreaActivity.this,DefinicoesActivity.class));
            overridePendingTransition( 0, 0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public class UserLogoutTask extends AsyncTask<Void, Void, String> {


        private String token;

        UserLogoutTask(String token) {
            this.token = token;

        }


        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }

        }

        //@Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {

                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/logout");
                json.accumulate("tokenID", token);
                return RequestsREST.doDELETE(url, json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mAuthTokTask = null;
            SharedPreferences to = getSharedPreferences("AUTHENTICATION", 0);
            if (result != null) {
                JSONObject token = null;
                try {
                    token = new JSONObject(result);
                    Log.i("LoginActivity", token.toString());

                    SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.remove("tokenID");
                    editor.remove("username");

                    editor.commit();


                    finish();
                    Toast.makeText(UserAreaActivity.this, "Logout efetuado com sucesso.", Toast.LENGTH_SHORT).show();
                    Intent menuIntent = new Intent(UserAreaActivity.this, FrontPage.class);
                    UserAreaActivity.this.startActivity(menuIntent);


                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTokTask = null;
        }

    }

    public void logout(){
        mAuthTokTask = new UserLogoutTask(tok);
        mAuthTokTask.execute((Void) null);
    }


    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + user);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {
                        //if (i == notificationsArray.length)
                          //  resize();

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }


    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }

    /*private void resize() {
        String[] newArray = new String[notificationsArray.length + 1];
        for (int i = 0; i < notificationsArray.length; i++)
            newArray[i] = notificationsArray[i];

        notificationsArray = newArray;
    }*/


    private void onBackDialog()
    {
        AlertDialog.Builder alertDialog =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("Deseja sair da aplicação?")
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        finishAffinity();

                    }

                });



        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.create().show();

    }

    }

