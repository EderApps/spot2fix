package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class SingleOccurrence extends FragmentActivity implements OnMapReadyCallback {

    private UserLikeOcurrenceTask mLikeTask = null;
    private GetUserLikedTask mGetUserTask = null;
    private GetCommentsTask mGetComments = null;
    private GetFollowingTask mCheckFollow = null;
    private UserFollowTask mFollow = null;
    private UserUnfollowTask mUnfollow = null;
    private UserRatingTask mRating = null;
    private GoogleMap mMap;
    RatingBar ratingBar;
    LinearLayout linearLayout;
    LinearLayout layout;
    String tok = "";
    String local = "";
    String file ="";
    String occID = "";
    String user = "";
    String status ="";
    String creator="";
    ImageView imageView;
    ImageButton like;
    Button followButton;
    TextView commentsLink;
    private GetNotifications mNotifications = null;
    private int count =0;
    TextView badge;
    private UserUnlikeOcurrenceTask mUnlikeTask = null;
    TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layout = (LinearLayout) findViewById(R.id.layout);
        Intent i = getIntent();

        String data = i.getStringExtra("data");
        String descri = i.getStringExtra("descricao");
        String tipo = i.getStringExtra("tipo");
        String likes = i.getStringExtra("likes");
        status = i.getStringExtra("status");
        String ocRating = i.getStringExtra("rating");



        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        SharedPreferences loc = getSharedPreferences("LOCAL", 0);
        SharedPreferences oc = getSharedPreferences("OCC", 0);
        SharedPreferences f = getSharedPreferences("FILE", 0);
        SharedPreferences c = getSharedPreferences("CREATOR", 0);

        tok = settings.getString("tokenID", "");
        local = loc.getString("loc", "");
        file = f.getString("file","");
        occID = oc.getString("occID","");
        user = settings.getString("email","");
        creator = c.getString("creator","");

        if (!tok.equals("")) {
            setContentView(R.layout.activity_single_occurrence);
            SingleOccurrence.this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

            commentsLink = (TextView) findViewById(R.id.linkComments);
            tv = new TextView(SingleOccurrence.this);
            ratingBar = (RatingBar) findViewById(R.id.ratingBar);
            if(!ocRating.equals("-1.0"))
                ratingBar.setRating(Float.valueOf(ocRating));
                else
                    ratingBar.setRating(0.0f);


            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    if (status.equals("Resolvido") && creator.equals(user))
                        ratingOc(occID, Float.toString(rating));

                    else if(status.equals("Resolvido")&& !creator.equals(user))
                        Toast.makeText(SingleOccurrence.this, "É necessário ser o criador da ocorrência para classificar.", Toast.LENGTH_SHORT).show();

                    else if(creator.equals(user) && !status.equals("Resolvido"))
                        Toast.makeText(SingleOccurrence.this, "A ocorrência necessita de estar resolvida para classificar.", Toast.LENGTH_SHORT).show();

                    else
                        Toast.makeText(SingleOccurrence.this, "É necessário ser o criador da ocorrência e a mesma estar resolvida.",
                                Toast.LENGTH_SHORT).show();
                }
                    });


            badge = (TextView) findViewById(R.id.badge_notification);
            like = (ImageButton) findViewById(R.id.likeBt);
            followButton = (Button) findViewById(R.id.btFollow);
            followButton.setText("Seguir");
            followButton.setTextColor(getResources().getColor(R.color.blue2));
            getnumberOfComments();
            checkFollowing();
            userLikedOcc();
            listNotifications();

                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(tv.getText().toString().equals("true"))
                            unlike();
                            else
                                like();
                    }
                });

            followButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(followButton.getText().toString().equals("Não Seguir"))
                        unfollow();
                    else
                        follow();
                }
            });
            commentsLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(SingleOccurrence.this,CommentsActivity.class);
                    i.putExtra("occurrenceID",occID);
                    startActivity(i);
                }
            });



            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(1);
            menuItem.setChecked(true);


            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {

                        case R.id.ic_submOc:
                            Intent submeterOcIntent = new Intent(SingleOccurrence.this, SubmeterOcMapActivity.class);
                            startActivity(submeterOcIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_listarOc:
                            Intent listarOcIntent = new Intent(SingleOccurrence.this, ListOcActivity.class);
                            startActivity(listarOcIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_menu:
                            Intent menuIntent = new Intent(SingleOccurrence.this, UserAreaActivity.class);
                            startActivity(menuIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_duvidas:
                            Intent duvidasIntent = new Intent(SingleOccurrence.this, DuvidasActivity.class);
                            startActivity(duvidasIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_notifications:
                            Intent intent = new Intent(SingleOccurrence.this, NotificationActivity.class);
                            startActivity(intent);
                            overridePendingTransition( 0, 0);
                            break;

                    }
                    return false;
                }
            });
        } else {
            setContentView(R.layout.visitor_single_occurrence);
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(1);
            menuItem.setChecked(true);

            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.ic_listarOc:
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_main:
                            Intent menuIntent = new Intent(SingleOccurrence.this, FrontPage.class);
                            startActivity(menuIntent);
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_duvidas:
                            Intent duv = new Intent(SingleOccurrence.this, DuvidasActivity.class);
                            startActivity(duv);
                            overridePendingTransition(0, 0);
                            break;

                    }
                    return false;


                }
            });
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        linearLayout = (LinearLayout) findViewById(R.id.ll10);

        TextView mainTextView = new TextView(SingleOccurrence.this);

        TextView dateTextView = new TextView(SingleOccurrence.this);
        TextView descriTextView = new TextView(SingleOccurrence.this);
        TextView likesTextView = new TextView(SingleOccurrence.this);
        TextView imageTextView = new TextView(SingleOccurrence.this);
        TextView statusTextView = new TextView(SingleOccurrence.this);
        imageView = new ImageView(SingleOccurrence.this);
        if (!file.equals(""))
        loadImageFromURL("https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+file,imageView);

        else
            imageView.setImageResource(R.drawable.post_img);

        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        mainTextView.setLayoutParams(lparams);
        descriTextView.setLayoutParams(lparams);
        imageView.setLayoutParams(lparams);
        dateTextView.setLayoutParams(lparams);
        likesTextView.setLayoutParams(lparams);
        imageTextView.setLayoutParams(lparams);



        mainTextView.setText( tipo + " " + "em" + " " + local + "\n");
        descriTextView.setText( "Descrição : " + descri + "\n");
        dateTextView.setText( "Data da Ocorrência : " + data + "\n");
        likesTextView.setText(  "Número de Gostos : " + likes + "\n");
        statusTextView.setText( "Estado : "+status + "\n");
        imageTextView.setText( "Multimédia : " + "\n");

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage();
            }
        });



        if(status.equals("Resolvido"))
            statusTextView.setTextColor(getResources().getColor(R.color.refresh));

        else
            statusTextView.setTextColor(getResources().getColor(R.color.refresh3));

        mainTextView.setTextColor(getResources().getColor(R.color.blue2));
        descriTextView.setTextColor(getResources().getColor(R.color.Black));
        dateTextView.setTextColor(getResources().getColor(R.color.Black));
        likesTextView.setTextColor(getResources().getColor(R.color.Black));
        imageTextView.setTextColor(getResources().getColor(R.color.Black));

        linearLayout.addView(mainTextView);
        linearLayout.addView(descriTextView);
        linearLayout.addView(dateTextView);
        linearLayout.addView(likesTextView);
        linearLayout.addView(statusTextView);
        linearLayout.addView(imageTextView);
        linearLayout.addView(imageView);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (local != null && !local.toString().equals("")) {
            List<Address> addressList = null;
            Geocoder geocoder = new Geocoder(SingleOccurrence.this, Locale.getDefault());


            try {
                addressList = geocoder.getFromLocationName(local, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            MarkerOptions opt = new MarkerOptions().title(local).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_logo))
                    .position(latLng)
                    .snippet("Local da ocorrência");
            mMap.addMarker(opt);

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    View v = getLayoutInflater().inflate(R.layout.info_window,null);

                    TextView tvLocal = (TextView) v.findViewById(R.id.tv_local);
                    TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);
                    ImageView iv = (ImageView) v.findViewById(R.id.infoImage);
                    loadImageFromURL("https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+file,iv);


                    tvLocal.setLayoutParams(lparams);
                    tvSnippet.setLayoutParams(lparams);
                    iv.setLayoutParams(lparams);
                    tvLocal.setText(marker.getTitle());
                    tvSnippet.setText(marker.getSnippet());

                    return v;
                }
            });
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
    }


    private void loadImageFromURL(String url,ImageView iv){
        Picasso.with(this).load(url).into(iv,new com.squareup.picasso.Callback(){

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });



    }

    private void unlike(){
        mUnlikeTask = new UserUnlikeOcurrenceTask(occID, user);
        mUnlikeTask.execute((Void) null);

    }
    private void like(){
        mLikeTask = new UserLikeOcurrenceTask(occID, user);
        mLikeTask.execute((Void) null);
    }

    private void userLikedOcc(){
        mGetUserTask = new GetUserLikedTask(user);
        mGetUserTask.execute((Void) null);
    }

    private void ratingOc(String oc,String rating){
        mRating = new UserRatingTask(oc,rating);
        mRating.execute((Void) null);
    }

    private void getnumberOfComments(){
        mGetComments = new GetCommentsTask(occID);
        mGetComments.execute((Void) null);
    }
    private void checkFollowing(){
        mCheckFollow = new GetFollowingTask(occID,user);
        mCheckFollow.execute((Void) null);
    }

    private void follow(){
        mFollow = new UserFollowTask(occID,user);
        mFollow.execute((Void) null);
    }

    private void unfollow(){
        mUnfollow = new UserUnfollowTask(occID,user);
        mUnfollow.execute((Void) null);
    }
    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }

    public class UserLikeOcurrenceTask extends AsyncTask<Void, Void, String> {

        private String ocID = "";
        private String userID = "";

        UserLikeOcurrenceTask(String ocID,String userID) {
            this.ocID = ocID;
            this.userID = userID;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/like/"+ocID+"/"+userID);
                return RequestsREST.doPUT(url,json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mLikeTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    Log.i("SingleOccurrence", response.toString());

                    like.setImageResource(android.R.color.transparent);
                    like.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_blue_like));
                    tv.setText("true");
                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mLikeTask = null;
        }


    }


    public class GetUserLikedTask extends AsyncTask<Void, Void, String> {
        private String userID = "";

        GetUserLikedTask(String userID) {
            this.userID = userID;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getUser/"+userID);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetUserTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    JSONObject propertyMap = response.getJSONObject("propertyMap");
                    JSONArray liked = new JSONArray(propertyMap.getString("user_liked"));
                    for (int i = 0; i<liked.length();i++) {
                        if (liked.getString(i).equals(occID)) {
                            like.setImageResource(android.R.color.transparent);
                            like.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_blue_like));
                            tv.setText("true");
                        }
                    }

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetUserTask = null;
        }


    }


    public class GetCommentsTask extends AsyncTask<Void, Void, String> {
        private String oc = "";

        GetCommentsTask(String oc) {
            this.oc = oc;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllComments/"+oc);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetComments = null;

            if (result != null) {
                JSONArray response = null;
                try {
                    response = new JSONArray(result);
                    commentsLink.setText("Comentários" + "(" + response.length() + ")" );
                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetComments = null;
        }


    }

    public class GetFollowingTask extends AsyncTask<Void, Void, String> {
        private String oc = "";
        private String id = "";

        GetFollowingTask(String oc,String id) {
            this.oc = oc;
            this.id = id;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/ocurrences/checkfollower/"+oc+"/"+id);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mCheckFollow = null;

            if (result != null) {
                try {
                        followButton.setText("Não Seguir");
                        followButton.setTextColor(getResources().getColor(R.color.Black));
                    }
                catch (Exception e) {
                }
            }

        }

        @Override
        protected void onCancelled() {
            mCheckFollow = null;
        }


    }

    public class UserFollowTask extends AsyncTask<Void, Void, String> {

        private String ocID = "";
        private String userID = "";

        UserFollowTask(String ocID,String userID) {
            this.ocID = ocID;
            this.userID = userID;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/ocurrences/follow/"+ocID+"/"+userID);
                json.accumulate("occID", ocID);
                json.accumulate("username", userID);
                return RequestsREST.doPUT(url,json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mFollow = null;

            if (result != null) {
                    followButton.setTextColor(getResources().getColor(R.color.Black));
                    followButton.setText("Não Seguir");
            }
            }

        @Override
        protected void onCancelled() {
            mFollow = null;
        }


    }

    public class UserUnfollowTask extends AsyncTask<Void, Void, String> {

        private String ocID = "";
        private String userID = "";

        UserUnfollowTask(String ocID,String userID) {
            this.ocID = ocID;
            this.userID = userID;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/ocurrences/unfollow/"+ocID+"/"+userID);
                return RequestsREST.doPUT(url,json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mUnfollow = null;

            if (result != null) {
                followButton.setTextColor(getResources().getColor(R.color.blue2));
                followButton.setText("Seguir");
            }
        }

        @Override
        protected void onCancelled() {
            mUnfollow = null;
        }


    }

    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + user);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {
                        //if (i == notificationsArray.length)
                        //  resize();

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }

    public class UserRatingTask extends AsyncTask<Void, Void, String> {

        private String ocID = "";
        private String rating = "";

        UserRatingTask(String ocID,String rating) {
            this.ocID = ocID;
            this.rating = rating;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/updateRating/"+ocID+"/"+rating);
                return RequestsREST.doPUT(url,json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mRating = null;

            if (result != null) {
                Toast.makeText(SingleOccurrence.this, "Rating atualizado!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mFollow = null;
        }


    }

    public class UserUnlikeOcurrenceTask extends AsyncTask<Void, Void, String> {

        private String ocID = "";
        private String userID = "";

        UserUnlikeOcurrenceTask(String ocID,String userID) {
            this.ocID = ocID;
            this.userID = userID;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject json = new JSONObject();
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/unlike/"+ocID+"/"+userID);
                return RequestsREST.doPUT(url,json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mUnlikeTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    Log.i("SingleOccurrence", response.toString());

                    like.setImageResource(android.R.color.transparent);
                    like.setBackgroundDrawable(getResources().getDrawable(R.mipmap.ic_mini_heart));
                    tv.setText("false");
                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mUnlikeTask = null;
        }


    }


    public void showImage() {
        Intent i = new Intent(SingleOccurrence.this,NewActivity.class);
        i.putExtra("imageExtra",file);
        startActivity(i);
        overridePendingTransition(0,0);
    }

    @Override
    public void onBackPressed() {
        String last = getIntent().getStringExtra("last");
        if(last.equals("Notification")) {
            startActivity(new Intent(SingleOccurrence.this, NotificationActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
             else
                 super.onBackPressed();
    }

}