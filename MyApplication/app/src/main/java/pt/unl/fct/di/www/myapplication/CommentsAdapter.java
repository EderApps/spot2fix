package pt.unl.fct.di.www.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentsAdapter extends ArrayAdapter<String> {

    CommentsAdapter(Context context, String[] options){
        super(context,R.layout.comment_row, options);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        String [] content= getItem(position).split("\n");
        String comment = content[1];
        String [] firstLine = content[0].split(",");
        String name = firstLine[0];
        String date = firstLine[1] + " " + firstLine[2];


        View customView = inflater.inflate(R.layout.comment_row ,parent,false);
        TextView text = (TextView) customView.findViewById(R.id.commentText);
        TextView nameText = (TextView) customView.findViewById(R.id.commentNameID);
        TextView dateText = (TextView) customView.findViewById(R.id.dateText);
        text.setText(comment);
        nameText.setText(name);
        dateText.setText(date);

        return customView;
    }
}
