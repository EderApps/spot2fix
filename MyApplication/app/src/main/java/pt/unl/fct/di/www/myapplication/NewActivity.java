package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class NewActivity extends AppCompatActivity {



    ImageView iv;
    ImageButton ib;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        iv = (ImageView) findViewById(R.id.iv);
        ib = (ImageButton) findViewById(R.id.ib);
        Intent i = getIntent();
        String file = i.getStringExtra("imageExtra");
        if(!file.equals("")){

            loadImageFromURL("https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+file,iv);
        }
        else
            Toast.makeText(this, "Nada a apresentar", Toast.LENGTH_SHORT).show();


        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void loadImageFromURL(String url,ImageView iv) {
        Picasso.with(this).load(url).into(iv, new com.squareup.picasso.Callback() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,0);
    }
}
