package pt.unl.fct.di.www.myapplication;

import android.content.Context;

import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class SimpleAdapter extends ArrayAdapter<String> {

    SimpleAdapter(Context context, String[] options){
        super(context,R.layout.custom_row, options);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.simple_row,parent,false);



        String singleItem = getItem(position);
        TextView text = (TextView) customView.findViewById(R.id.option);
        text.setText(singleItem);

        return customView;
    }


}
