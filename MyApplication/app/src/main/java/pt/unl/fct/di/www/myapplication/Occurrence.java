package pt.unl.fct.di.www.myapplication;

/**
 * Created by Admin on 07/06/2017.
 */

public class Occurrence {

    String id = "";
    String creator = "";
    String date = "";
    String location = "";
    String description = "";
    String type = "";
    String masterType="";
    int likes = 0;
    String filename ="";
    String status = "";
    String rating = "";

    public Occurrence(String id,String creator, String date, String type,String masterType, String location,
                      String description, int likes, String filename, String status, String rating){
        this.date=date;
        this.id = id;
        this.creator = creator;
        this.location=location;
        this.description=description;
        this.likes=likes;
        this.type=type;
        this.masterType=masterType;
        this.filename = filename;
        this.status = status;
        this.rating=rating;

    }

    public int getLikes(){
        return likes;
    }

    public String getDate(){
        return date;
    }

    public String getDescription(){
        return description;
    }

    public String getLocation(){
        return location;
    }

    public String getId(){
        return id;
    }

    public String getType(){
        return type;
    }

    public String getFilename(){return filename;}

    public String getStatus(){return status;}

    public String getCreator(){return creator;}

    public String getMasterType(){return masterType;}

    public String getRating(){return rating;}



}
