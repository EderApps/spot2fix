package pt.unl.fct.di.www.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class OccurrenceAdapter extends ArrayAdapter<String> {

    OccurrenceAdapter(Context context, String [] occurr){
        super(context,R.layout.singleoccurrence, occurr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.singleoccurrence,parent,false);

        String singleItem = getItem(position);
        TextView text = (TextView) customView.findViewById(R.id.textInfo);
        ImageView image = (ImageView) customView.findViewById(R.id.imageInfo);

        text.setText(singleItem);
        image.setImageResource(R.drawable.logo);
        return customView;
    }

}
