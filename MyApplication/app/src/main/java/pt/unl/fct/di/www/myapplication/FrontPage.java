package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FrontPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences shared = getSharedPreferences("AUTHENTICATION", 0);
        if(!shared.getString("tokenID","").equals(""))
            startActivity(new Intent(this,UserAreaActivity.class));


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);

        final Button logInButton = (Button) findViewById(R.id.bLogIn);
        final Button registerButton = (Button) findViewById(R.id.registerButton);
        final TextView resetPassword = (TextView) findViewById(R.id.resetPassword);

        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FrontPage.this,ResetPasswordActivity.class));
            }
        });


        BottomNavigationView startNavigationView = (BottomNavigationView) findViewById(R.id.menuStart);
        BottomNavigationViewHelper.disableShiftMode(startNavigationView);
        Menu menu = startNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);


        startNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.ic_listarOc:
                        Intent listarOcIntent = new Intent(FrontPage.this, ListOcActivity.class);
                        startActivity(listarOcIntent);
                        overridePendingTransition( 0, 0);
                        break;

                    case R.id.ic_main:
                        startActivity(getIntent());
                        overridePendingTransition( 0, 0);
                        break;

                   case R.id.ic_duvidas:
                        Intent duvidasIntent = new Intent(FrontPage.this, DuvidasActivity.class);
                        startActivity(duvidasIntent);
                        overridePendingTransition( 0, 0);
                        break;
                }
                return false;
            }
        });


        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(FrontPage.this, LoginActivity.class);
                FrontPage.this.startActivity(loginIntent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(FrontPage.this, RegisterActivity.class);
                FrontPage.this.startActivity(registerIntent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });


    }

    private void onBackDialog()
    {
        AlertDialog.Builder alertDialog =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("Deseja sair da aplicação?")
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        finishAffinity();

                    }

                });



        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.create().show();

    }

    @Override
    public void onBackPressed() {
        onBackDialog();
    }
}
