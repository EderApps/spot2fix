package pt.unl.fct.di.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class DuvidasActivity extends AppCompatActivity {
    private String tok = "";
    private String user ="";
    private GetNotifications mNotifications = null;
    private int count =0;
    TextView badge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        tok = settings.getString("tokenID", "");


        if (!tok.equals("")) {
            setContentView(R.layout.activity_duvidas);
            user = settings.getString("email","");
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(4);
            menuItem.setChecked(true);
            badge = (TextView) findViewById(R.id.badge_notification);
            listNotifications();

            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {

                        case R.id.ic_submOc:
                            Intent submeterOcIntent = new Intent(DuvidasActivity.this, SubmeterOcMapActivity.class);
                            startActivity(submeterOcIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_listarOc:
                            Intent listarOcIntent = new Intent(DuvidasActivity.this, ListOcActivity.class);
                            startActivity(listarOcIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_menu:
                            Intent menuIntent = new Intent(DuvidasActivity.this, UserAreaActivity.class);
                            startActivity(menuIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_duvidas:
                            Intent duvidasIntent = new Intent(DuvidasActivity.this, DuvidasActivity.class);
                            startActivity(duvidasIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_notifications:
                            Intent intent = new Intent(DuvidasActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            overridePendingTransition( 0, 0);
                            break;

                    }
                    return false;
                }
            });

        } else {
            setContentView(R.layout.visitor_duvidas);
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(4);
            menuItem.setChecked(true);


            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.ic_listarOc:
                            Intent listarOcIntent = new Intent(DuvidasActivity.this, ListOcActivity.class);
                            startActivity(listarOcIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_main:
                            Intent menuIntent = new Intent(DuvidasActivity.this, FrontPage.class);
                            startActivity(menuIntent);
                            overridePendingTransition( 0, 0);
                            break;
                        case R.id.ic_duvidas:
                            finish();
                            overridePendingTransition( 0, 0);
                            startActivity(getIntent());
                            overridePendingTransition( 0, 0);
                            break;

                    }
                    return false;
                }
            });
        }
    }


    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + user);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {
                        //if (i == notificationsArray.length)
                        //  resize();

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }


    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }

}