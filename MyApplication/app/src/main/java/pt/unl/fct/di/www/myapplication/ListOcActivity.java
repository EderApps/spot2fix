package pt.unl.fct.di.www.myapplication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;

import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListOcActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private UserListOcurrencesTask mList = null;
    private UserListOcurrencesByCategoryTask mListByCategory = null;
    private UserListOcurrencesByStatusTask mListByStatus = null;
    private GetNotifications mNotifications = null;
    private UserListMyOcurrencesTask mMyList = null;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()~'%";
    private String tok = "";
    private String user = "";
    private Occurrence[] occurrences = new Occurrence[1000];
    private String[] occurrList;
    private ListView l;
    ListAdapter listAdapter;
    private String cursor = "";
    TextView tv;
    private TextView likeText;
    TextView badge;
    private int count =0;
    private boolean flag_loading;
    private String[] SPINNERLIST = {"Todos", "Minhas Ocorrências" ,"Abertos", "Resolvidos", "Sazonal", "Lixo", "Saúde Pública", "Rua/Parque Danificado", "Veículos/Estacionamento", "Luzes", "Graffiti Ilegal", "Árvores"};
    private String[] filenameArray;
    private String[] statusArray;
    private ProgressDialog progressDialog;
    private GetUserLikedTask mGetUserTask = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("AUTHENTICATION", 0);
        tok = settings.getString("tokenID", "");
        user = settings.getString("email", "");
        occurrList = new String[0];
        filenameArray = new String[0];
        statusArray = new String[0];
        cursor = "first";
        flag_loading = false;
        tv = new TextView(ListOcActivity.this);
        likeText = new TextView(ListOcActivity.this);

        if (!tok.equals("")) {
            setContentView(R.layout.activity_list_oc);
            badge = (TextView) findViewById(R.id.badge_notification);

            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            l = (ListView) findViewById(R.id.l);
            listNotifications();
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(1);
            menuItem.setChecked(true);

            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {

                        case R.id.ic_submOc:
                            Intent submeterOcIntent = new Intent(ListOcActivity.this, SubmeterOcMapActivity.class);
                            startActivity(submeterOcIntent);
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_listarOc:
                            Intent listarOcIntent = new Intent(ListOcActivity.this, ListOcActivity.class);
                            startActivity(listarOcIntent);
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_menu:
                            Intent menuIntent = new Intent(ListOcActivity.this, UserAreaActivity.class);
                            startActivity(menuIntent);
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_duvidas:
                            Intent duvidasIntent = new Intent(ListOcActivity.this, DuvidasActivity.class);
                            startActivity(duvidasIntent);
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_notifications:
                            Intent intent = new Intent(ListOcActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            break;

                    }
                    return false;
                }
            });
        } else {
            setContentView(R.layout.visitor_list_oc);
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
            l = (ListView) findViewById(R.id.lv);
            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
            Menu menu = bottomNavigationView.getMenu();
            MenuItem menuItem = menu.getItem(0);
            menuItem.setChecked(true);

            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.ic_listarOc:
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                            break;
                        case R.id.ic_main:
                            Intent menuIntent = new Intent(ListOcActivity.this, FrontPage.class);
                            startActivity(menuIntent);
                            overridePendingTransition(0, 0);

                            break;
                        case R.id.ic_duvidas:
                            overridePendingTransition(0, 0);
                            Intent duv = new Intent(ListOcActivity.this, DuvidasActivity.class);
                            startActivity(duv);
                            overridePendingTransition(0, 0);
                            break;

                    }
                    return false;


                }
            });

        }

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(ListOcActivity.this, SingleOccurrence.class);
                SharedPreferences loc = getSharedPreferences("LOCAL", 0);
                SharedPreferences.Editor editor = loc.edit();
                editor.putString("loc", occurrences[position].getLocation());
                editor.commit();


                SharedPreferences f = getSharedPreferences("FILE", 0);
                SharedPreferences.Editor e = f.edit();
                e.putString("file", occurrences[position].getFilename());
                e.commit();

                SharedPreferences occ = getSharedPreferences("OCC", 0);
                SharedPreferences.Editor edit = occ.edit();
                edit.putString("occID", occurrences[position].getId());
                edit.commit();

                SharedPreferences creator = getSharedPreferences("CREATOR", 0);
                SharedPreferences.Editor editCreator = creator.edit();
                editCreator.putString("creator", occurrences[position].getCreator());
                editCreator.commit();


                intent.putExtra("tipo", occurrences[position].getType());
                intent.putExtra("descricao", occurrences[position].getDescription());
                intent.putExtra("data", occurrences[position].getDate());
                intent.putExtra("likes", Integer.toString(occurrences[position].getLikes()));
                intent.putExtra("status", occurrences[position].getStatus());
                intent.putExtra("rating",occurrences[position].getRating());
                intent.putExtra("last","ListOc");


                startActivity(intent);
            }
        });


        l.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                    flag_loading = true;
                    listOcorrences(cursor);
                }
            }
        });


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        Spinner spinner = (Spinner) findViewById(R.id.spinner_filtros);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(this);

    }

    public void listOcorrences(String cr) {

        addDialog();
        mList = new UserListOcurrencesTask(cr);
        mList.execute();

    }

    public void listOccurrencesCategory(String category,String cr){

        addDialog();
        mListByCategory = new UserListOcurrencesByCategoryTask(category,cr);
        mListByCategory.execute((Void)null);
    }


    public void listOccurrencesStatus(String status,String cr){
        addDialog();
        mListByStatus = new UserListOcurrencesByStatusTask(status,cr);
        mListByStatus.execute((Void)null);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
        if (selected.equals("Todos")) {
            occurrList = new String[0];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOcorrences(cursor);

        }

        if (selected.equals("Minhas Ocorrências")) {
            occurrList = new String[0];
            filenameArray = new String[0];
            occurrences = new Occurrence[1000];
            statusArray = new String[0];
            cursor ="first";
            listMyOccurrences(cursor);
        l.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                    flag_loading = true;
                    listMyOccurrences(cursor);
                }
            }
        });
        }
        if (selected.equals("Abertos")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesStatus("Aberto",cursor);

            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesStatus("Aberto",cursor);
                    }
                }
            });
        }

        if (selected.equals("Resolvidos")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesStatus("Resolvido",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesStatus("Resolvido",cursor);
                    }
                }
            });
        }

        if (selected.equals("Sazonal")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Sazonal",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Sazonal",cursor);
                    }
                }
            });
        }

        if (selected.equals("Lixo")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Lixo",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Lixo",cursor);
                    }
                }
            });
        }

        if (selected.equals("Saúde Pública")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Saúde Pública",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Saúde Pública",cursor);
                    }
                }
            });
        }

        if (selected.equals("Rua/Parque Danificado")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Rua/Parque Danificado",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Rua/Parque Danificado",cursor);
                    }
                }
            });
        }

        if (selected.equals("Veículos/Estacionamento")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Veículos/Estacionamento",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Veículos/Estacionamento",cursor);
                    }
                }
            });
        }

        if (selected.equals("Graffiti Ilegal")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Graffiti Ilegal",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Graffiti Ilegal",cursor);
                    }
                }
            });
        }

        if (selected.equals("Luzes")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Luzes",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Luzes",cursor);
                    }
                }
            });
        }

        if (selected.equals("Árvores")){
            occurrList = new String[0];
            occurrences = new Occurrence[1000];
            filenameArray = new String[0];
            statusArray = new String[0];
            cursor ="first";
            listOccurrencesCategory("Árvores",cursor);
            l.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1 && l.getCount() >= 9 && flag_loading == false) {
                        flag_loading = true;
                        listOccurrencesCategory("Árvores",cursor);
                    }
                }
            });
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    public class UserListOcurrencesTask extends AsyncTask<Void, Void, String> {

          private String c;

        UserListOcurrencesTask(String c) {
            this.c = c;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllOccurrences/" + c);

                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mList = null;
            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    if(arr.length()==0){
                        progressDialog.dismiss();
                    }

                    int i = occurrList.length;

                    if (arr.length()==0 && i ==0){
                        Toast.makeText(ListOcActivity.this, "Vazio", Toast.LENGTH_SHORT).show();
                        l.setAdapter(null);
                    }

                    for(int j=0;j<arr.length();j++) {

                        if (i == occurrList.length)
                            occurrList = resize(occurrList);

                        if (i == filenameArray.length)
                            filenameArray = resize(filenameArray);

                        if (i == statusArray.length)
                            statusArray = resize(statusArray);

                            JSONObject jObj = arr.getJSONObject(j);
                            JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                            JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                            String ocId = propertyMap.getString("occurrence_id");
                            String ocType = propertyMap.getString("occurrence_type");
                            String creator = propertyMap.getString("occurrence_creator");
                            String masterType = propertyMap.getString("occurrence_type_master");
                            String date = key.getString("value");
                            String location = propertyMap.getString("occurrence_location");
                            String description = propertyMap.getString("occurrence_description");
                            String rating = propertyMap.getString("occurrence_rating");
                            int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                            String status = propertyMap.getString("occurrence_status");
                            if (!propertyMap.isNull("occurrence_filename")) {
                                String filename = propertyMap.getString("occurrence_filename");
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType,
                                        location, description, likes, filename, status,rating);
                                filenameArray[i] = filename;
                            } else {
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType,
                                        location, description, likes, "", status, rating);
                                filenameArray[i] = "";
                            }



                        statusArray[i] =" "+  occurrences[i].getStatus().toUpperCase() + " ";
                        occurrList[i] ="\n" + " " +occurrences[i].getType() + " " + "em" + " "
                                + occurrences[i].getLocation() +
                                "\n" + " " +occurrences[i].getDescription() + "\n" +
                                " " +occurrences[i].getDate() + "|" + " " + occurrences[i].getLikes() + " " + "\uD83D\uDC99" + "\n\n" +

                                " " +"Criado por: " + occurrences[i].getCreator()+ "\n";
                        i++;
                        }

                    if(arr.length()!=0) {
                        flag_loading = false;
                        listAdapter = new CostumAdapter(ListOcActivity.this, occurrList,filenameArray, statusArray);
                        Parcelable state = l.onSaveInstanceState();
                        progressDialog.dismiss();
                        l.setAdapter(listAdapter);
                        l.onRestoreInstanceState(state);
                        tv.setText(cursor);

                    }

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mList = null;
        }


    }

    public class UserListOcurrencesByCategoryTask extends AsyncTask<Void, Void, String> {

        private String c;
        private String category;

        UserListOcurrencesByCategoryTask(String category, String c) {
            this.c = c;
            this.category=category;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String encodedCategory = Uri.encode(category,ALLOWED_URI_CHARS);
                String urlString = "http://fit-sanctum-159416.appspot.com/rest/login/getAllOccurrencesCategory/"+encodedCategory+"/"+c;


                return RequestsREST.doGET(new URL(urlString));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mListByCategory = null;
            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    int i = occurrList.length;

                    if(arr.length() ==0 )
                        progressDialog.dismiss();

                    if (arr.length()==0 && i ==0){
                        Toast.makeText(ListOcActivity.this, "Vazio", Toast.LENGTH_SHORT).show();
                        l.setAdapter(null);
                    }
                        for (int j = 0; j < arr.length(); j++) {

                            if (i == occurrList.length)
                                occurrList = resize(occurrList);

                            if (i == filenameArray.length)
                                filenameArray = resize(filenameArray);

                            if (i == statusArray.length)
                                statusArray = resize(statusArray);

                            JSONObject jObj = arr.getJSONObject(j);
                            JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                            JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                            String ocId = propertyMap.getString("occurrence_id");
                            String ocType = propertyMap.getString("occurrence_type");
                            String creator = propertyMap.getString("occurrence_creator");
                            String masterType = propertyMap.getString("occurrence_type_master");
                            String date = key.getString("value");
                            String location = propertyMap.getString("occurrence_location");
                            String description = propertyMap.getString("occurrence_description");
                            String rating = propertyMap.getString("occurrence_rating");
                            int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                            String status = propertyMap.getString("occurrence_status");
                            if (!propertyMap.isNull("occurrence_filename")) {
                                String filename = propertyMap.getString("occurrence_filename");
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                        description, likes, filename, status, rating);
                                filenameArray[i] = filename;
                            } else {
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                        description, likes, "", status, rating);
                                filenameArray[i] = "";
                            }

                            statusArray[i] =" "+  occurrences[i].getStatus().toUpperCase() + " ";
                            occurrList[i] = "\n" + " " + occurrences[i].getType() + " " + "em" + " "
                                    + occurrences[i].getLocation() +
                                    "\n" + " " +occurrences[i].getDescription() + "\n" +
                                    " " +occurrences[i].getDate() + "|" + " " + occurrences[i].getLikes() + " " + "\uD83D\uDC99" + "\n\n" +

                                    " " + "Criado por: " + occurrences[i].getCreator()+ "\n";

                            i++;
                        }

                        if (arr.length() != 0) {
                            flag_loading = false;
                            listAdapter = new CostumAdapter(ListOcActivity.this, occurrList,filenameArray, statusArray);
                            Parcelable state = l.onSaveInstanceState();
                            progressDialog.dismiss();
                            l.setAdapter(listAdapter);
                            l.onRestoreInstanceState(state);
                            tv.setText(cursor);

                        }
                    //}



                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mListByCategory = null;
        }


    }

    public class UserListOcurrencesByStatusTask extends AsyncTask<Void, Void, String> {

        private String c;
        private String status;

        UserListOcurrencesByStatusTask(String status, String c) {
            this.c = c;
            this.status=status;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String urlString = "http://fit-sanctum-159416.appspot.com/rest/login/getAllOccurrencesStatus/"+status+"/"+c;


                return RequestsREST.doGET(new URL(urlString));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mListByCategory = null;
            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                        int i = occurrList.length;

                        if(arr.length() ==0 )
                            progressDialog.dismiss();


                    if (arr.length()==0 && i ==0){
                            Toast.makeText(ListOcActivity.this, "Vazio", Toast.LENGTH_SHORT).show();
                            l.setAdapter(null);
                        }
                        for (int j = 0; j < arr.length(); j++) {

                            if (i == occurrList.length)
                                occurrList = resize(occurrList);

                            if (i == filenameArray.length)
                                filenameArray = resize(filenameArray);

                            if (i == statusArray.length)
                                statusArray = resize(statusArray);

                            JSONObject jObj = arr.getJSONObject(j);
                            JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                            JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                            String ocId = propertyMap.getString("occurrence_id");
                            String ocType = propertyMap.getString("occurrence_type");
                            String creator = propertyMap.getString("occurrence_creator");
                            String masterType = propertyMap.getString("occurrence_type_master");
                            String date = key.getString("value");
                            String location = propertyMap.getString("occurrence_location");
                            String description = propertyMap.getString("occurrence_description");
                            int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                            String rating = propertyMap.getString("occurrence_rating");
                            String status = propertyMap.getString("occurrence_status");
                            if (!propertyMap.isNull("occurrence_filename")) {
                                String filename = propertyMap.getString("occurrence_filename");
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                        description, likes, filename, status, rating);
                                filenameArray[i]=filename;
                            } else {
                                occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType, location,
                                        description, likes, "", status, rating);
                                filenameArray[i]="";
                            }

                            statusArray[i] =" "+  occurrences[i].getStatus().toUpperCase() + " ";
                            occurrList[i] ="\n" + " " +occurrences[i].getType() + " " + "em" + " "
                                    + occurrences[i].getLocation() +
                                    "\n" + " " +occurrences[i].getDescription() + "\n" +
                                    " " +occurrences[i].getDate() + "|" + " " + occurrences[i].getLikes() + " " + "\uD83D\uDC99" + "\n\n" +

                                    " " +"Criado por: " + occurrences[i].getCreator()+ "\n";

                            i++;
                        }

                        if (arr.length() != 0) {
                            flag_loading = false;
                            listAdapter = new CostumAdapter(ListOcActivity.this, occurrList,filenameArray,statusArray);
                            Parcelable state = l.onSaveInstanceState();
                            progressDialog.dismiss();
                            l.setAdapter(listAdapter);
                            l.onRestoreInstanceState(state);
                            tv.setText(cursor);

                        }
                    //}



                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mListByStatus = null;
        }


    }


    public class GetNotifications extends AsyncTask<Void, Void, String> {

        GetNotifications() {
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllNotificationsAndroid/" + user);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mNotifications = null;

            if (result != null) {
                JSONArray response = null;
                try {

                    response = new JSONArray(result);

                    for (int i = 0; i < response.length(); i++) {
                        //if (i == notificationsArray.length)
                        //  resize();

                        JSONObject jObj = response.getJSONObject(i);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        String opened = propertyMap.getString("not_opened");

                        if(opened.equals("false"))
                            count++;
                    }

                    badge.setText(" "+count);

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mNotifications = null;
        }

    }

    public class GetUserLikedTask extends AsyncTask<Void, Void, String> {
        private String userID = "";
        private String occID = "";

        GetUserLikedTask(String userID, String occID) {
            this.userID = userID;
            this.occID = occID;
        }


        @Override
        protected void onPreExecute(){

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getUser/"+userID);
                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mGetUserTask = null;

            if (result != null) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    JSONObject propertyMap = response.getJSONObject("propertyMap");
                    JSONArray liked = new JSONArray(propertyMap.getString("user_liked"));
                    for (int i = 0; i<liked.length();i++) {
                        if (liked.getString(i).equals(occID)) {
                            likeText.setText("\uD83D\uDC99");
                        }
                        else
                            likeText.setText("\uD83D\uDDA4");
                    }

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }

        }

        @Override
        protected void onCancelled() {
            mGetUserTask = null;
        }


    }

    public class UserListMyOcurrencesTask extends AsyncTask<Void, Void, String> {

        private String c;

        UserListMyOcurrencesTask(String c) {
            this.c = c;
        }


        @Override
        protected void onPreExecute() {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL("http://fit-sanctum-159416.appspot.com/rest/login/getAllOccurrencesMinhas/"+ user +"/"+ c);

                return RequestsREST.doGET(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            mMyList = null;
            if (result != null) {
                JSONObject response = null;
                try {

                    response = new JSONObject(result);
                    JSONArray arr = response.getJSONArray("results");
                    cursor = response.getString("cursor");

                    if(arr.length()==0){
                        progressDialog.dismiss();
                    }

                    int i = occurrList.length;

                    if (arr.length()==0 && i ==0){
                        Toast.makeText(ListOcActivity.this, "Vazio", Toast.LENGTH_SHORT).show();
                        l.setAdapter(null);
                    }

                    for(int j=0;j<arr.length();j++) {

                        if (i == occurrList.length)
                            occurrList = resize(occurrList);

                        if (i == filenameArray.length)
                            filenameArray = resize(filenameArray);

                        if (i == statusArray.length)
                            statusArray = resize(statusArray);

                        JSONObject jObj = arr.getJSONObject(j);
                        JSONObject propertyMap = jObj.getJSONObject("propertyMap");
                        JSONObject key = propertyMap.getJSONObject("occurrence_creation_time");

                        String ocId = propertyMap.getString("occurrence_id");
                        String ocType = propertyMap.getString("occurrence_type");
                        String creator = propertyMap.getString("occurrence_creator");
                        String masterType = propertyMap.getString("occurrence_type_master");
                        String date = key.getString("value");
                        String location = propertyMap.getString("occurrence_location");
                        String description = propertyMap.getString("occurrence_description");
                        String rating = propertyMap.getString("occurrence_rating");
                        int likes = Integer.parseInt(propertyMap.getString("occurrence_likes"));
                        String status = propertyMap.getString("occurrence_status");
                        if (!propertyMap.isNull("occurrence_filename")) {
                            String filename = propertyMap.getString("occurrence_filename");
                            occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType,
                                    location, description, likes, filename, status,rating);
                            filenameArray[i] = filename;
                        } else {
                            occurrences[i] = new Occurrence(ocId, creator, date, ocType, masterType,
                                    location, description, likes, "", status, rating);
                            filenameArray[i] = "";
                        }



                        statusArray[i] =" "+  occurrences[i].getStatus().toUpperCase() + " ";
                        occurrList[i] ="\n" + " " +occurrences[i].getType() + " " + "em" + " "
                                + occurrences[i].getLocation() +
                                "\n" + " " +occurrences[i].getDescription() + "\n" +
                                " " +occurrences[i].getDate() + "|" + " " + occurrences[i].getLikes() + " " + "\uD83D\uDC99" + "\n\n" +

                                " " +"Criado por: " + occurrences[i].getCreator()+ "\n";
                        i++;
                    }

                    if(arr.length()!=0) {
                        flag_loading = false;
                        listAdapter = new CostumAdapter(ListOcActivity.this, occurrList,filenameArray, statusArray);
                        Parcelable state = l.onSaveInstanceState();
                        progressDialog.dismiss();
                        l.setAdapter(listAdapter);
                        l.onRestoreInstanceState(state);
                        tv.setText(cursor);

                    }

                } catch (JSONException e) {
                    Log.e("Authentication", e.toString());
                }
            }
        }

        @Override
        protected void onCancelled() {
            mMyList = null;
        }


    }


    public void listNotifications(){
        mNotifications = new GetNotifications();
        mNotifications.execute();
    }
    public void listMyOccurrences(String cr) {

        addDialog();
        mMyList = new UserListMyOcurrencesTask(cr);
        mMyList.execute();

    }

    private String[] resize(String[]v) {
        String[] newArray = new String[v.length + 1];
        for(int i =0;i<v.length;i++)
            newArray[i]=v[i];

        v = newArray;
        return v;
    }

    private void addDialog(){
        progressDialog = new ProgressDialog(ListOcActivity.this);
        progressDialog.setTitle("Carregar");
        progressDialog.setMessage("A carregar...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }



   /* private void resizeOccurrList() {
        String[] newArray = new String[occurrList.length + 1];
        for(int i =0;i<occurrList.length;i++)
            newArray[i]=occurrList[i];

        occurrList = newArray;
    }

    private void resizeFilename() {
        String[] newArray = new String[filenameArray.length + 1];
        for(int i =0;i<filenameArray.length;i++)
            newArray[i]=filenameArray[i];

        filenameArray = newArray;
    }

    private void resizeStatus() {
        String[] newArray = new String[statusArray.length + 1];
        for(int i =0;i<statusArray.length;i++)
            newArray[i]=statusArray[i];

        statusArray = newArray;
    }*/



}

