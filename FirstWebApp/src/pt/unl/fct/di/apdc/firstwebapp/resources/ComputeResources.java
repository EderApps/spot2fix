package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;



@Path("/utils")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ComputeResources {
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private final Gson g = new Gson();

	private static final DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ComputeResources() {} //nothing to be done here

	@POST
	@Path("/compute")
	public Response executeComputeTask() {
		LOG.fine("Starting to execute computation taks");
		Transaction txn = datastore.beginTransaction();
		int counter = 0;
		while(counter < 5){
			try{
				Query ctrQuery = new Query("Occurrence");
				int reported = 0;
				int resolved = 0;

				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
				for(Entity e: results){
					if(((String)e.getProperty("occurrence_status")).equals("Resolvido")){
						resolved++;
					}
					reported++;
				}
				int users = 0;
				Query ctrQueryUsers = new Query("User");
				List<Entity> resultsUsers = datastore.prepare(ctrQueryUsers).asList(FetchOptions.Builder.withDefaults());
				users = resultsUsers.size();
				
				Key userKey = KeyFactory.createKey("Statistics", 5);
				Entity user = null;
				try {
					user = datastore.get(userKey);
				} catch (Exception e1) {
					user = new Entity("Statistics", 5);
				}
				user.setProperty("stats_occurrencesReported", reported);
				user.setProperty("stats_occurrencesResolved", resolved);
				user.setProperty("stats_totalUsers", users);

				datastore.put(txn,user);
				txn.commit();
				break;
			}catch(ConcurrentModificationException e1){
				counter++;
			}
		}
		return Response.ok().build();
	}

	@GET
	@Path("/compute")
	public Response triggerExecuteComputeTask() {
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/utils/compute"));
		return Response.ok().build();
	}

}