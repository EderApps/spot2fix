package pt.unl.fct.di.apdc.firstwebapp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OccurrenceData {
	public String typeOc;
	public String location;
	public String description;
	public String whodid;
	public String urgency;
	public String occID;
	public String creator;
	public String fileName;
	public boolean anonim;
	public List<String> following;
	
	public OccurrenceData(){}
	
	public OccurrenceData(String typeOc, String location, String description, String urgency, String creator, boolean anonim) {
		this.typeOc = typeOc;
		this.location = location;
		this.description = description;
		this.whodid = "";
		this.occID = UUID.randomUUID().toString();
		this.urgency = urgency;
		this.creator = creator;
		this.anonim = anonim;
	}
	
	public OccurrenceData(String typeOc, String location, String description, String urgency, String creator, String fileName, boolean anonim) {
		this.typeOc = typeOc;
		this.location = location;
		this.description = description;
		this.whodid = "";
		this.occID = UUID.randomUUID().toString();
		this.urgency = urgency;
		this.creator = creator;
		this.fileName = fileName;
		this.anonim = anonim;

	}
}
