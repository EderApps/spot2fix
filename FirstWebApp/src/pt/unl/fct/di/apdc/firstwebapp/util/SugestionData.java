package pt.unl.fct.di.apdc.firstwebapp.util;

public class SugestionData {
	
	public String city;
	public String section;
	public double lat;
	public double lng;
			
	public SugestionData(){}
	
	public SugestionData(String city, String section, double lat, double lng) {
			this.city = city;
			this.section = section;
			this.lat = lat;
			this.lng = lng;
			
	}
	
	public SugestionData(String city, String section) {
		this.city = city;
		this.section = section;
		
}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return 
			   validField(city) &&
			   validField(section);		
	}
	
}
