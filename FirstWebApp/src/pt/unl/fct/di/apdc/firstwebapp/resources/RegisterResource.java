package pt.unl.fct.di.apdc.firstwebapp.resources;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.mail.smtp.SMTPTransport;

@Path("/register")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();
	private SecureRandom random = new SecureRandom();

	public RegisterResource() {
	} // Nothing to be done here...




	@PUT
	@Path("/{id}/confirmemail")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response emailConfirmation(@PathParam("id") String id) {
		String useremail="";
		Transaction txn = datastore.beginTransaction();
		try {
			Key emailKey = KeyFactory.createKey("Email-confirmation", id);
			Entity email = datastore.get(emailKey);
			useremail = (String) email.getProperty("user_email");
			email.setProperty("email_confirmed", true);
			email.setProperty("email_confirmation_time", new Date());
			datastore.put(email);
			txn.commit();

			Transaction txnp = datastore.beginTransaction();
			try {
				Key userKey = KeyFactory.createKey("User", useremail);
				Entity user = datastore.get(userKey);
				user.setProperty("user_confirmedEmail", true);
				datastore.put(user);
				txnp.commit();
				return Response.ok().build();

			}
			finally {
				if (txnp.isActive()) {
					txnp.rollback();
				}
			}

		} catch (EntityNotFoundException e) {
			// Username does not exist
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

	/*metodo que adiciona um colaborador*/
	@POST
	@Path("/collaborator")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerGovCollaborator(pt.unl.fct.di.apdc.firstwebapp.util.RegisterColaboratorData data) {

		if( ! data.validRegistration() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = datastore.get(userKey);

			Response r = addSection(data.city, data.section, data.email);

			if(r.getStatus() == Status.CONFLICT.getStatusCode()){
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).entity("User is already collaborating.").build();
			}
			else{
				return Response.ok("{}").build();
			}				

		} catch (EntityNotFoundException e) {

			Entity user = new Entity("User", data.email);
			user.setProperty("user_name", data.companyname);
			user.setProperty("user_incharge", data.name);
			user.setProperty("user_email", data.email);
			user.setProperty("user_type", "Colaborador");
			user.setProperty("user_active", false);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
			user.setProperty("user_address"," ");
			user.setProperty("user_confirmedEmail", true);
			String id = DigestUtils.sha512Hex(new BigInteger(130, random).toString(32));

			user.setUnindexedProperty("user_creation_time", new Date());
			ArrayList<String> s = new ArrayList<String>();
			s.add("test");
			user.setProperty("user_liked", s);
			user.setProperty("user_notifications", s);			
			datastore.put(txn,user);
			txn.commit();
			sendFromGMail(data.email,"Confirmacao de conta","Clique no link a seguir para confirmar a sua conta: https://fit-sanctum-159416.appspot.com/emailconfirmation.html?id=" + id);
			addSection(data.city, data.section, data.email); //associar colaborador a seccao e cidade

			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	private Response addSection(String city, String section, String email){
		Transaction txn = datastore.beginTransaction();
		String id =city+section+email;
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key collabKey = KeyFactory.createKey("Collaboration", id);
			Entity collab = datastore.get(collabKey);
			txn.rollback();
			return Response.status(Status.CONFLICT).entity("Colaboration already exists.").build();

		} catch (EntityNotFoundException e) {

			Entity collab = new Entity("Collaboration", id);
			collab.setProperty("collaboration_city", city);
			collab.setProperty("collaboration_section", section);
			collab.setProperty("collaboration_collaborator", email);
			collab.setProperty("collaboration_active", true);
			collab.setUnindexedProperty("collaboration_creation", new Date());
			datastore.put(txn,collab);
			txn.commit();
			return Response.ok("{}").build();

		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	/*metodo que adiciona uma colaboracao entre um gov e colaborador*/				
	@POST
	@Path("/add/{city}/collaborator/{userID}/{section}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addCollaboratorSection(@PathParam("city") String city, @PathParam("userID") String email,@PathParam("section") String section) {
		Transaction txn = datastore.beginTransaction();
		String id =city+section+email;
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key collabKey = KeyFactory.createKey("Collaboration", id);
			Entity collab = datastore.get(collabKey);
			txn.rollback();
			return Response.status(Status.CONFLICT).entity("Colaboration already exists.").build();

		} catch (EntityNotFoundException e) {

			Entity collab = new Entity("Collaboration", id);
			collab.setProperty("collaboration_city", city);
			collab.setProperty("collaboration_section", section);
			collab.setProperty("collaboration_collaborator", email);
			collab.setProperty("collaboration_active", true);
			collab.setUnindexedProperty("collaboration_creation", new Date());
			datastore.put(txn,collab);
			txn.commit();
			return Response.ok("{}").build();

		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	/*metodo que elimina uma colaboracao entre gov e colaborador*/
	@PUT
	@Path("/removeSection/{city}/collaborator/{userID}/{section}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeCollaboratorSection(@PathParam("city") String city, @PathParam("userID") String email, @PathParam("section") String section) {
		Transaction txn = datastore.beginTransaction();
		String id =city+section+email;
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key collabKey = KeyFactory.createKey("Collaboration", id);
			Entity collab = datastore.get(collabKey);
			collab.setProperty("collaboration_active", false);
			datastore.put(txn,collab);
			txn.commit();
			return Response.ok("{}").build();

		} catch (EntityNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity("Collaboration does not exist.").build();

		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}


	/*metodo que todas as colaboracoes com colaborador para aquele gov		
	@PUT
	@Path("/remove/{city}/collaborator/{userID}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeCollaborator(@PathParam("userID") String email, @PathParam("city") String city) {

			Filter propertyFilter = CompositeFilterOperator.and(CompositeFilterOperator.and(new FilterPredicate("collaboration_email", FilterOperator.EQUAL, email),
					new FilterPredicate("collaboration_active", FilterOperator.EQUAL, true)), new FilterPredicate("collaboration_city", FilterOperator.EQUAL, city));
			Query ctrQuery = new Query("Collaboration").setFilter(propertyFilter);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

			if(results.isEmpty()){
				return Response.status(Status.BAD_REQUEST).entity("Collaborator has no connections.").build();
			}

			Iterator<Entity> it = results.iterator();
			while(it.hasNext()){
				Transaction txn = datastore.beginTransaction();
				Entity x = it.next();
				x.setProperty("collaboration_active", false);
				datastore.put(x);
				txn.commit();
			}

			return Response.ok().build();
	}
	 */


	/*metodo que faz get dos colaboradores por seccao*/		
	@GET
	@Path("/getWorkers/{section}/{city}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getWorkers(@PathParam("section") String section, @PathParam("city") String city) {
		Filter propertyFilter1 = CompositeFilterOperator.and(CompositeFilterOperator.and(new FilterPredicate("collaboration_section", FilterOperator.EQUAL, section),
				new FilterPredicate("collaboration_city", FilterOperator.EQUAL, city)),new FilterPredicate("collaboration_active", FilterOperator.EQUAL, true));
		Query ctrQuery1 = new Query("Collaboration").setFilter(propertyFilter1);
		List<Entity> results1 = datastore.prepare(ctrQuery1).asList(FetchOptions.Builder.withDefaults());
		List<Entity> res =new ArrayList<Entity>();

		Iterator<Entity> it = results1.iterator();
		while(it.hasNext()){
			Entity x = (Entity) it.next();
			String username =(String) x.getProperty("collaboration_collaborator");
			Key userKey = KeyFactory.createKey("User", username);
			try {
				Entity user = datastore.get(userKey);
				res.add(user);
			} catch (EntityNotFoundException e) {
			}
		}

		if(res.isEmpty())
			return Response.status(Status.NOT_FOUND).build();
		else{
			System.out.println("RESULTS:::  "+ res);
			return Response.ok(g.toJson(res)).build();				
		}
	}



	@GET 
	@Path("/worksOn/{occurrence}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response isbeingWorkedOn(@PathParam("occurrence") String occurrence){
		Filter propertyFilter1 = new FilterPredicate("occurrence_id", FilterOperator.EQUAL, occurrence);
		Query ctrQuery1 = new Query("WorksOn").setFilter(propertyFilter1);
		List<Entity> results1 = datastore.prepare(ctrQuery1).asList(FetchOptions.Builder.withDefaults());

		if(!results1.isEmpty()){
			Entity e1 = null;
			for(Entity e: results1){
				e1 = e;
				break;
			}

			return Response.ok(g.toJson(e1)).build();
		}
		else
			return Response.status(Status.NOT_FOUND).build();
	}


	@POST
	@Path("/{userID}/workson/{occurrence}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response worksOn(@PathParam ("userID") String userID, @PathParam ("occurrence") String occurrence) {
		String id = DigestUtils.sha512Hex(new BigInteger(50, random).toString(32));
		Transaction txn = datastore.beginTransaction();

		Filter propertyFilter1 = CompositeFilterOperator.and(new FilterPredicate("user_email", FilterOperator.EQUAL, userID),
				new FilterPredicate("occurrence_id", FilterOperator.EQUAL, occurrence));
		Query ctrQuery1 = new Query("WorksOn").setFilter(propertyFilter1);
		List<Entity> results1 = datastore.prepare(ctrQuery1).asList(FetchOptions.Builder.withDefaults());

		if(!results1.isEmpty()){
			txn.rollback();
			return Response.status(Status.CONFLICT).entity("User is already working on the occurrence.").build();
		}
		else{
			Entity work = new Entity("WorksOn", id);
			work.setProperty("workson_id", id);
			work.setProperty("user_email", userID);
			work.setProperty("occurrence_id", occurrence);
			work.setProperty("work_finished", false);
			work.setUnindexedProperty("work_start_time", new Date());

			datastore.put(txn,work);
			txn.commit();
			
			Transaction txn1 = datastore.beginTransaction();

			String notID = UUID.randomUUID().toString();

			Entity notification = new Entity("Notification", notID);
			notification.setProperty("not_id", notID);
			notification.setProperty("not_message", "Foi lhe atribuída esta ocorrência para resolver!");
			notification.setUnindexedProperty("not_date", new Date());
			notification.setProperty("not_date_ms", new Date().getTime());
			notification.setProperty("not_occID", occurrence);					
			notification.setProperty("not_userToNotify", userID);				
			notification.setProperty("not_opened", false);				

			datastore.put(txn1,notification);

			txn1.commit();
			
		}
		return Response.ok("{}").build();
	}

	
	
	
	@PUT
	@Path("/{userID}/putNewSolver/{occurrence}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response putNewSolver(@PathParam ("userID") String userID, @PathParam ("occurrence") String occurrence) {
		int counter = 0;
		Transaction txn = datastore.beginTransaction();

		while(counter < 5){
			LOG.warning("000");
			try{
				Key userKey = KeyFactory.createKey("Occurrence", occurrence);
				Entity occ = datastore.get(userKey);
				occ.setProperty("occurrence_solver", userID);
				LOG.warning("111");

				datastore.put(txn,occ);
				txn.commit();
				break;
			}catch(ConcurrentModificationException e1){
				counter++;
				LOG.warning("222");

			} catch (EntityNotFoundException e) {
				// TODO Auto-generated catch block
				counter++;
				LOG.warning("333");

				e.printStackTrace();
			}
		}
		


		return Response.ok("{}").build();
	}

	/*metodo que faz get dos colaboradores por pesquisa de nome*/		
	@GET
	@Path("/searchWorker/{userId}/{city}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response searchWorker(@PathParam("userId") String userId, @PathParam("city") String city) {
		List<Entity> res =new ArrayList<Entity>();
		Filter propertyFilter = new FilterPredicate("user_type", FilterOperator.EQUAL, "Colaborador");

		Query ctrQuery = new Query("User").setFilter(propertyFilter);

		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		Iterator<Entity> it = results.iterator();

		Filter propertyFilter1 = CompositeFilterOperator.and(new FilterPredicate("collaboration_city", FilterOperator.EQUAL, city),
				new FilterPredicate("collaboration_active", FilterOperator.EQUAL, true));

		Query ctrQuery1 = new Query("Collaboration").setFilter(propertyFilter1);

		List<Entity> results1 = datastore.prepare(ctrQuery1).asList(FetchOptions.Builder.withDefaults());
		Iterator<Entity> it1 = results1.iterator();
		while(it.hasNext()){

			Entity x = (Entity) it.next();
			boolean flag = false;
			it1 = results1.iterator();
			while(it1.hasNext() && !flag){

				Entity y = (Entity) it1.next();

				if(x.getProperty("user_email").toString().toUpperCase().equals(y.getProperty("collaboration_collaborator").toString().toUpperCase()) && (((String) x.getProperty("user_incharge").toString().toUpperCase()).contains(userId.toUpperCase()) || ((String) x.getProperty("user_name").toString().toUpperCase()).contains(userId.toUpperCase()))){
					res.add(x);

					flag=true;
				}

			}
		}

		if(res.isEmpty())
			return Response.status(Status.NOT_FOUND).build();
		else{
			System.out.println("RESULTS:::  "+ res);
			return Response.ok(g.toJson(res)).build();				
		}
	}

	public Response setConfirmation(String email) throws AddressException, MessagingException {

		Transaction txn = datastore.beginTransaction();
		String id = DigestUtils.sha512Hex(new BigInteger(50, random).toString(32));
		try {		
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("Email-confirmation", id);
			Entity confirm = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Email it's already wainting for confirmation.").build(); 
		} catch (EntityNotFoundException e) {
			Entity confirm = new Entity("Email-confirmation", id);
			confirm.setProperty("user_email", email);
			confirm.setProperty("user_creation_time", new Date());
			confirm.setProperty("email_confirmed", false);
			datastore.put(txn,confirm);

			txn.commit();
			sendFromGMail(email,"Confirmacao de conta","Clique no link a seguir para confirmar a sua conta: https://fit-sanctum-159416.appspot.com/emailconfirmation.html?id=" + id);

			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	public void sendFromGMail(String to, String subject, String body) {

		String host = "smtp.gmail.com";
		final String from = "spot2fix@gmail.com";
		final String pass = "5eder1spot";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, pass);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(body);

			Transport.send(message);
		}
		catch (AddressException ae) {
			ae.printStackTrace();
		}
		catch (MessagingException me) {
			me.printStackTrace();
		}
	}

	@PUT
	@Path("/{id}/confirmpassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response passwordConfirmation(@PathParam("id") String id) {
		String useremail="";
		Transaction txn = datastore.beginTransaction();
		try {
			Key emailKey = KeyFactory.createKey("Email-confirmation", id);
			Entity email = datastore.get(emailKey);
			useremail = (String) email.getProperty("user_email");
			email.setProperty("email_confirmed", true);
			email.setProperty("email_confirmation_time", new Date());
			datastore.put(email);
			txn.commit();

			Transaction txnp = datastore.beginTransaction();
			try {
				Key userKey = KeyFactory.createKey("User", useremail);
				Entity user = datastore.get(userKey);
				user.setProperty("email_confirmation", true);
				datastore.put(user);
				txnp.commit();
				return Response.ok().build();

			}
			finally {
				if (txnp.isActive()) {
					txnp.rollback();
				}
			}

		} catch (EntityNotFoundException e) {
			// Username does not exist
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}



	@POST
	@Path("/v1")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// Basic code
	public Response doRegistrationV1(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data) {

		Entity user = new Entity("User", data.username);
		user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
		user.setUnindexedProperty("user_creation_time", new Date());
		datastore.put(user);
		LOG.info("User registered " + data.username);
		return Response.ok().build();

	}

	@POST
	@Path("/v2")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// Checks that the user exists but may have problems with concurrent transactions
	public Response doRegistrationV2(pt.unl.fct.di.apdc.firstwebapp.util.RegisterData data) {

		if( ! data.validRegistration() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = datastore.get(userKey);
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.email);
			user.setProperty("user_name", data.name);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
			user.setProperty("user_email", data.email);
			user.setUnindexedProperty("user_creation_time", new Date());
			datastore.put(user);
			LOG.info("User registered " + data.email);
			return Response.ok().build();
		}
	}


	//register gov users
	@POST
	@Path("/gov")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerGov(pt.unl.fct.di.apdc.firstwebapp.util.RegisterData data) {
		System.out.println("1111");
		if( !data.validRegistration() ) {
			System.out.println("2222");
			System.out.println("jfndnf "+data.address);

			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity user = new Entity("User", data.email);

					user.setProperty("user_name", data.name);
					user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
					user.setProperty("user_email", data.email);
					user.setProperty("user_type", "Governo");
					user.setProperty("user_nif", data.nif);
					user.setProperty("user_bi", data.bi);
					user.setProperty("user_city", data.city);
					ArrayList<String> s = new ArrayList<String>();
					s.add("test");
					user.setProperty("user_liked", s);
					user.setProperty("user_notifications", s);
					user.setProperty("user_confirmedEmail", true);

					user.setUnindexedProperty("user_creation_time", new Date());
					datastore.put(txn,user);
					LOG.info("User registered " + data.email);
					String id = DigestUtils.sha512Hex(new BigInteger(130, random).toString(32));

					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			registerGov1(data.city, data.email);
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	//register city
	public Response registerGov1(String cityname, String email) {

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key cityKey = KeyFactory.createKey("City", cityname);
			Entity city = datastore.get(cityKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity city = new Entity("City", cityname);
					city.setProperty("city_name", cityname);
					city.setProperty("city_email", email);
					city.setUnindexedProperty("user_creation_time", new Date());
					datastore.put(txn,city);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}


	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistrationV3(pt.unl.fct.di.apdc.firstwebapp.util.RegisterData data) {		

		if(data.address.contains("script") || data.name.contains("script") || data.email.contains("script") || data.password.contains("script") || data.confirm.contains("script")){
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		}

		if( ! data.validRegistration() ) {
			return Response.status(111).entity("Missing or wrong parameter.").build();
		}

		if(!data.validEmail(data.email)){
			return Response.status(222).entity("Missing or wrong parameter.").build();
		}

		if( ! data.validPassword(data.password) ) {
			return Response.status(333).entity("Missing or wrong parameter.").build();
		}

		if( ! data.validConfirm(data.confirm) ) {
			return Response.status(444).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity user = new Entity("User", data.email);
					user.setProperty("user_name", data.name);
					user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
					user.setProperty("user_email", data.email);
					user.setProperty("user_type", "Cidadão");
					user.setProperty("user_address",data.address);
					ArrayList<String> s = new ArrayList<String>();
					s.add("test");
					user.setProperty("user_liked", s);
					user.setProperty("user_notifications", s);
					user.setProperty("user_confirmedEmail", false);


					user.setUnindexedProperty("user_creation_time", new Date());
					datastore.put(txn,user);
					LOG.info("User registered " + data.email);

					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			try {
				setConfirmation(data.email);
			} catch (MessagingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	@GET 
	@Path("/getusername/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUser(@PathParam("username") String username) throws EntityNotFoundException {
		Key userKey = KeyFactory.createKey("User", username);
		System.out.println("eee "+username+" eee");
		try {
			Entity user = datastore.get(userKey);
			return Response.ok(g.toJson(user)).build();
		}
		catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity("User does not exist.").build(); 
		}
	}

	@PUT
	@Path("/{username}/attributes/password/{pass}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changePassword(@PathParam("username") String username, @PathParam("pass") String data) {
		Transaction txn = datastore.beginTransaction();
		if(!(data.length() > 5) || !(data.matches(".*\\d+.*")) || !(data.matches(".*[a-z].*"))){
			return Response.status(333).build();
		}
		try {
			int counter = 0;
			while(counter < 5){
				try{
					LOG.warning("PASSWORD IS "+data);
					Key userKey = KeyFactory.createKey("User", username);
					Entity user = datastore.get(userKey);
					user.setProperty("user_pwd", DigestUtils.sha512Hex(data));
					datastore.put(user);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + username);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	private void sendEmail1(String email, String id) throws AddressException, MessagingException{
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.fallback", "false");

		Session session = Session.getInstance(props, null);
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("spot2fix@gmail.com"));

		InternetAddress[] addrs = InternetAddress.parse(email, false);
		msg.setRecipients(Message.RecipientType.TO, addrs);

		msg.setSubject("Confirmação de email");
		msg.setText("https://fit-sanctum-159416.appspot.com/passwordconfirmation.html?id=" + id);
		msg.setSentDate(new Date());

		Transport t =
				session.getTransport("smtp");
		t.connect("smtp.gmail.com", "spot2fix@gmail.com", "5eder1spot");
		t.sendMessage(msg, msg.getAllRecipients());

		//System.out.println("Response: " + t.getLastServerResponse());

		t.close();		
	}

}
