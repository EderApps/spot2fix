package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

@Path("/ocurrences")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class OcurrenceRegistrationResource {
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();




	@GET
	@Path("/numberOfOccurrences/{city}/{section}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response numberOfOccurrences(@PathParam("city") String city, @PathParam("section") String section) {
		Filter propertyFilter;
		if(section.equalsIgnoreCase("todas"))
			propertyFilter = new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city);

		else
			propertyFilter = CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_type", FilterOperator.EQUAL, section));

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		int leng = 0;
		if(!results.isEmpty())
			leng = results.size();

		System.out.println("RESULT:  "+ leng);
		return Response.ok(g.toJson(leng)).build();				
	}

	@GET
	@Path("/nrResolvedOccurrences/{city}/{section}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response numberOfResolvedOccurrences(@PathParam("city") String city, @PathParam("section") String section) {
		Filter propertyFilter;
		if(section.equalsIgnoreCase("todas"))
			propertyFilter = CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_status", FilterOperator.EQUAL, "Resolvido")); //verificar o nome 

		else
			propertyFilter = CompositeFilterOperator.and(CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_type", FilterOperator.EQUAL, section)), new FilterPredicate("occurrence_status", FilterOperator.EQUAL, "Resolvido"));//verificar se e "resolvido"

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		int leng = 0;
		if(!results.isEmpty())
			leng = results.size();

		System.out.println("RESULT:  "+ leng);
		return Response.ok(g.toJson(leng)).build();				
	}

	@GET
	@Path("/satisfactionpercentage/{city}/{section}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response satisfactionPercentage(@PathParam("city") String city, @PathParam("section") String section) {
		Filter propertyFilter;
		if(section.equalsIgnoreCase("todas"))
			propertyFilter = CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_status", FilterOperator.EQUAL, "Resolvido")); //verificar o nome 

		else
			propertyFilter = CompositeFilterOperator.and(CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_type", FilterOperator.EQUAL, section)), new FilterPredicate("occurrence_status", FilterOperator.EQUAL, "Resolvido"));//verificar se e "resolvido"

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		int sat = 0;
		if(!results.isEmpty()){
			int counter = 0;
			for(Entity e : results){
				sat = (sat*counter + (int) e.getProperty("occurrence_rating"))/++counter;

			}
		}

		System.out.println("RESULT:  "+ sat);
		return Response.ok(g.toJson(sat)).build();				
	}

	@GET
	@Path("/cityStats/{city}/{section}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response cityStats(@PathParam("city") String city, @PathParam("section") String section) {
		Filter propertyFilter;
		if(section.equalsIgnoreCase("todas")){
			propertyFilter = new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city); //verificar o nome 
			LOG.warning("hmhjmjm");
		}else{
			propertyFilter = CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
					new FilterPredicate("occurrence_type", FilterOperator.EQUAL, section));
		}
		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		int nrofoccurrences = 0;
		int nrofresolved = 0;
		int sat = 0;
		LOG.warning("size is "+results.size());
		if(!results.isEmpty()){
			int counter = 0;
			for(Entity e : results){
				LOG.warning("1 + + + + "+((String)e.getProperty("occurrence_status")));
				if(((String)e.getProperty("occurrence_status")).equals("Resolvido")){
					LOG.warning("0000");
					//LOG.warning("EHEHEHE "+ ((String)e.getProperty("occurrence_rating")));
					//sat = (sat*counter)/++counter;
					nrofresolved++;
				}
			}
			nrofoccurrences = results.size();
		}
		
		LOG.warning("nrofocc is "+nrofoccurrences);
		LOG.warning("nrofres is "+nrofresolved);
		LOG.warning("sat is "+sat);

		int [] w = new int [3];
		w[0] = nrofoccurrences;
		w[1] = nrofresolved;
		w[2] = sat;
		LOG.warning("RESULT:  "+ w);
		return Response.ok(g.toJson(w)).build();				
	}
	
	
	
	
	
	@GET
	@Path("/colabStats/{userID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response colabStats(@PathParam("userID") String userID) {
		Filter propertyFilter = new FilterPredicate("user_email", FilterOperator.EQUAL, userID); //verificar o nome 
		LOG.warning("hmhjmjm");
		
		Query ctrQuery = new Query("WorksOn").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		int nrofoccurrences = 0;
		int nrofresolved = 0;
		LOG.warning("size is "+results.size());
		if(!results.isEmpty()){
			for(Entity e : results){
				if(((boolean)e.getProperty("work_finished")) == true){
					LOG.warning("0000");
					//LOG.warning("EHEHEHE "+ ((String)e.getProperty("occurrence_rating")));
					//sat = (sat*counter)/++counter;
					nrofresolved++;
				}
			}
			nrofoccurrences = results.size();
		}
		
		LOG.warning("nrofocc is "+nrofoccurrences);
		LOG.warning("nrofres is "+nrofresolved);

		int [] w = new int [2];
		w[0] = nrofoccurrences;
		w[1] = nrofresolved;
		LOG.warning("RESULT:  "+ w);
		return Response.ok(g.toJson(w)).build();				
	}
	

	public static final double R = 6372.8; // In kilometers
	public double haversine(double lat1, double lon1, double lat2, double lon2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return R * c;
	}

	@GET
	@Path("/searchsuggestions/{city}/{section}/{lat}/{lng}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response seekSuggestions(@PathParam("city") String city, @PathParam("section") String section, @PathParam("lat") double lat, @PathParam("lng") double lng) {
		LOG.warning("000");
		Filter propertyFilter = CompositeFilterOperator.and(new FilterPredicate("occurrence_city", FilterOperator.EQUAL, city),
				new FilterPredicate("occurrence_type", FilterOperator.EQUAL, section));
		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter);
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		List<Entity> res =new ArrayList<Entity>();
		LOG.warning("111");

		Iterator<Entity> it = results.iterator();
		LOG.warning("2222");

		while(it.hasNext()){		
			LOG.warning("333");

			Entity x = (Entity) it.next();
			LOG.warning("444");

			double latt =(double) x.getProperty("occurrence_lat");
			LOG.warning("555");

			double lngg =(double) x.getProperty("occurrence_long");
			LOG.warning("666");

			double val = haversine(lat,lng, latt,lngg); 
			LOG.warning("7777");

			if(val<0.3){
				LOG.warning("888");

				res.add(x);
				LOG.warning("999");

				x.setProperty("occurrence_havers", val);
				LOG.warning("1010101010");

			}
		}

		if(res.isEmpty())
			return Response.status(Status.NOT_FOUND).build();
		else{
			Comparator<Entity> cmp = new Comparator<Entity>() {
				public int compare(Entity e1, Entity e2) {
					if((double)e1.getProperty("occurrence_havers") < (double) e1.getProperty("occurrence_havers"))
						return -1;
					else if((double)e1.getProperty("occurrence_havers") > (double) e1.getProperty("occurrence_havers"))
						return 1;
					else
						return 0;
				}
			};
			Collections.sort(res, cmp);
			List<Entity> l = new ArrayList<Entity>();	      
			l.add(res.get(0));
			l.add(res.get(1));
			l.add(res.get(2));
			l.add(res.get(3));
			return Response.ok(g.toJson(res)).build();				
		}
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("/checkfollower/{oid}/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkFollower(@PathParam("oid") String oid, @PathParam("id") String id) {

		Key occKey = KeyFactory.createKey("Occurrence", oid);
		try {
			Entity occurrence = datastore.get(occKey);
			List <String> f = (List<String>) occurrence.getProperty("occurrence_following");
			if(f.contains(id))
				return Response.ok().build();
			else
				return Response.status(222).entity("User its not following").build(); 
		}
		catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity("Occurrence does not exist.").build(); 
		}	
	}

	@SuppressWarnings("unchecked")
	@PUT
	@Path("/follow/{id}/{userID}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response follow(@PathParam("id") String id, @PathParam("userID") String username) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					Key ocurrenceKey = KeyFactory.createKey("Occurrence", id);
					Entity ocurrence = datastore.get(ocurrenceKey);
					List <String> f = (List<String>) ocurrence.getProperty("occurrence_following");
					if(!f.contains(username)){
						f.add(username);
						ocurrence.setProperty("occurrence_following", f);
						datastore.put(ocurrence);
					}
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Ocurrence does not exist
			LOG.warning("Failed attempt ocurrence for id: " + id);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@PUT
	@Path("/unfollow/{id}/{userID}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unfollow(@PathParam("id") String id, @PathParam("userID") String username) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					Key ocurrenceKey = KeyFactory.createKey("Occurrence", id);
					Entity ocurrence = datastore.get(ocurrenceKey);
					List <String> f = (List<String>) ocurrence.getProperty("occurrence_following");
					if(f.contains(username)){
						f.remove(username);
						ocurrence.setProperty("occurrence_following", f);
						datastore.put(ocurrence);
					}
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Ocurrence does not exist
			LOG.warning("Failed attempt ocurrence for id: " + id);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

}
