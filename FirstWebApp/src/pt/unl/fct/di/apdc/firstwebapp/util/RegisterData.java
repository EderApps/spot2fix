package pt.unl.fct.di.apdc.firstwebapp.util;


public class RegisterData {

	public String name;
	public String email;
	public String address;
	public String type;
	public String password;
	public String confirm;
	public String city;
	public String nif;
	public String bi;
	
	public RegisterData() {
		
	}
	
	
	public RegisterData(String name, String email, String address,
			String password, String confirm) {
		
			this.name = name;
			this.email = email;
			this.address = address;
			this.password = password;
			this.confirm = confirm;
			this.city = "";
			this.nif = "";
			this.bi = "";
			
	}

public RegisterData(String city, String name, String email, String nif, String bi,
			String password, String confirm) {

			this.city = city;
			this.name = name;
			this.email = email;
			this.nif = nif;
			this.bi = bi;
			this.password = password;
			this.confirm = confirm;
			this.address = city;

	}
	
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validPassword(String p){
		return p.length() > 5 && p.matches(".*\\d+.*") && p.matches(".*[a-z].*");
	}
	
	public boolean validEmail(String e){
		return email.contains("@");
	}
	
	public boolean validConfirm(String c){
		return password.equals(confirm);
	}
	
	public boolean validRegistration() {
		if(city != null){
			address = city;
		}
		return 
			   validField(name) &&
			   validField(email) &&
			   validField(password) &&
			   validField(confirm) && validField(address);
	}
	
}