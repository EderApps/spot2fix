package pt.unl.fct.di.apdc.firstwebapp.util;

public class LoginData {
	
	public String username;
	public String password;
	public boolean checked;
	
	public LoginData(){}
	
	public LoginData(String username, String password, boolean checked) {
		this.username = username;
		this.password = password;
		this.checked = checked;
	}
}
