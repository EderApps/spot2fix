package pt.unl.fct.di.apdc.firstwebapp.resources;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.mail.smtp.SMTPTransport;

import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;

@Path("/recover")
public class PasswordRecoverResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(PasswordRecoverResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();
	private SecureRandom random = new SecureRandom();

	public PasswordRecoverResource() { } //Nothing to be done here...

	private void sendEmail(String email, String id) throws AddressException, MessagingException{
		Properties props = System.getProperties();
		props.put("mail.smtps.host", "smtp.mailgun.org");
		props.put("mail.smtps.auth", "true");

		Session session = Session.getInstance(props, null);
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("spot2fix@sandboxefe0283964a644c4a52027af6cb943a3.mailgun.org"));

		InternetAddress[] addrs = InternetAddress.parse(email, false);
		msg.setRecipients(Message.RecipientType.TO, addrs);

		msg.setSubject("Recuperacao da senha.");
		msg.setText("Abra o seguinte link para proceder a recuperacao da sua senha: http://fit-sanctum-159416.appspot.com/passwordrecovery.html?id=" + id);
		msg.setSentDate(new Date());

		SMTPTransport t =
				(SMTPTransport) session.getTransport("smtps");
		t.connect("smtp.mailgun.com", "postmaster@sandboxefe0283964a644c4a52027af6cb943a3.mailgun.org", "5eder1spot");
		t.sendMessage(msg, msg.getAllRecipients());

		System.out.println("Response: " + t.getLastServerResponse());

		t.close();		
	}

	@POST
	@Path("/{email}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response recoverPassword(@PathParam ("email") String username) throws MessagingException {


		Transaction txn = datastore.beginTransaction();
		String id = DigestUtils.sha512Hex(new BigInteger(130, random).toString(32));

		try {
			int counter = 0;
			while(counter < 5){
				try{
					// If the entity does not exist an Exception is thrown. Otherwise,
					Key userKey = KeyFactory.createKey("UserRecoveryInfo", id);
					Entity user = datastore.get(userKey);
					txn.rollback();
					return Response.status(Status.BAD_REQUEST).entity("User already tryed to recover password, please check your email.").build(); 
				} catch (EntityNotFoundException e) {
					Entity user = new Entity("UserRecoveryInfo", id);
					user.setProperty("user_id", id);
					user.setProperty("user_username", username);
					user.setProperty("user_creation_time", System.currentTimeMillis() + 1000*60*60);
					datastore.put(user);
					LOG.info("User registered " + username);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}

			sendFromGMail(username,"Recuperacao de Senha","Clique no link a seguir para recuperar a sua senha: https://fit-sanctum-159416.appspot.com/passwordrecovery.html?id=" + id);
			return Response.ok(g.toJson(id)).build();	
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	@GET 
	@Path("/getusername/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUsername(@PathParam("id") String id) throws EntityNotFoundException {
		try{
			Key userKey = KeyFactory.createKey("UserRecoveryInfo", id);
			Entity user = datastore.get(userKey);
			Object username = user.getProperty("user_username");
			LOG.info("User in password changing process " + username);
			return Response.ok(g.toJson(username)).build();
		}
		catch (EntityNotFoundException e) {

			return Response.status(Status.BAD_REQUEST).entity("User does not exist.").build(); 
		}
	}

	@DELETE
	@Path("/remove/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response logout(@PathParam ("id") String id) throws EntityNotFoundException {

		Transaction txn = datastore.beginTransaction();
		int counter = 0;
		while(counter < 5){
			try{
				Key userKey = KeyFactory.createKey("UserRecoveryInfo", id);
				Entity user = datastore.get(userKey);
				datastore.delete(txn, userKey );
				txn.commit();
				break;
			}catch(ConcurrentModificationException e1){
				counter++;
			}
		}
		return Response.ok("{}").build();

	}

	@DELETE
	@Path("/checkTimes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkTimes(){

		Transaction txn = null;
		Query ctrQuery = new Query("UserRecoveryInfo");
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		for(int i = 0; i < results.size();i++){
			if ((long)(results.get(i).getProperty("user_creation_time")) <= System.currentTimeMillis() ){
				txn = datastore.beginTransaction();
				Key key = KeyFactory.createKey("UserRecoveryInfo", (String)(results.get(i).getProperty("user_id")));
				datastore.delete(txn, key);
				txn.commit();
			}
		}
		System.out.println(results.size());

		return Response.ok().build();
	}
	
	public void sendFromGMail(String to, String subject, String body) {
	       
	       String host = "smtp.gmail.com";
	       final String from = "spot2fix@gmail.com";
	       final String pass = "5eder1spot";
	       
	       Properties props = new Properties();
	       props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
	       
	       Session session = Session.getInstance(props,
	    			  new javax.mail.Authenticator() {
	    				protected PasswordAuthentication getPasswordAuthentication() {
	    					return new PasswordAuthentication(from, pass);
	    				}
	    			  });
	       try {
	    	   Message message = new MimeMessage(session);
	           message.setFrom(new InternetAddress(from));
	           message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	           message.setSubject(subject);
	           message.setText(body);
	           
	           Transport.send(message);
	       }
	       catch (AddressException ae) {
	           ae.printStackTrace();
	       }
	       catch (MessagingException me) {
	           me.printStackTrace();
	       }
	   }

}
