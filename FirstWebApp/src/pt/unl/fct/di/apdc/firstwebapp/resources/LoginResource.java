package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;
import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;


@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public LoginResource() { } //Nothing to be done here...


	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data, 
			@Context HttpServletRequest request,
			@Context HttpHeaders headers) {

		if(data.username.contains("script") || data.password.contains("script")){
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		}
		int counter = 0;

		LOG.fine("Attempt to login user: " + data.username);
		boolean confirmed = true;
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			confirmed = (boolean) user.getProperty("user_confirmedEmail");
			LOG.warning("confirmed? "+confirmed);
			if(!confirmed){
				return Response.status(333).entity("Missing or wrong parameter.").build();
			}
			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}
			AuthToken token = null;
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct
				while(counter < 5){
					try{

						// Construct the logs
						Entity log = new Entity("UserLog", user.getKey());
						log.setProperty("user_login_ip", request.getRemoteAddr());
						log.setProperty("user_login_host", request.getRemoteHost());
						log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
						log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
						log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
						log.setProperty("user_login_time", new Date());
						// Get the user statistics and updates it
						ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
						ustats.setProperty("user_stats_failed", 0L );
						ustats.setProperty("user_stats_last", new Date());		

						// Batch operation
						List<Entity> logs = Arrays.asList(log,ustats);
						datastore.put(txn,logs);
						txn.commit();
						Transaction txn1 = datastore.beginTransaction();

						token = new AuthToken(data.username, data.checked);

						Entity auth = new Entity("Token", token.tokenID);

						auth.setProperty("auth_tokenID", token.tokenID);
						auth.setProperty("auth_username", token.username);
						auth.setProperty("auth_expirData", token.expirationData);
						auth.setProperty("auth_creationData", token.creationData);

						datastore.put(txn1,auth);

						txn1.commit();
						// Return token
						break;
					}catch(ConcurrentModificationException e1){
						counter++;
					}
				}

				LOG.info("User '" + data.username + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();				
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn,ustats);				
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				LOG.warning("LOOOOOL");
				if(!confirmed){
					return Response.status(333).entity("Missing or wrong parameter.").build();
				}else{
					return Response.status(Status.FORBIDDEN).build();
				}
			}
		}

	}


	@POST
	@Path("/logingoogle/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginGoogle(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data, @PathParam("name") String name, 
			@Context HttpServletRequest request,
			@Context HttpHeaders headers) {
		System.out.println("TESTE");

		LOG.fine("Attempt to login user: " + data.username);
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);

			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			//if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
			// Password correct
			AuthToken token = null;
			int counter = 0;
			while(counter < 5){
				try{
					// Construct the logs
					Entity log = new Entity("UserLog", user.getKey());
					log.setProperty("user_login_ip", request.getRemoteAddr());
					log.setProperty("user_login_host", request.getRemoteHost());
					log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
					log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
					log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
					log.setProperty("user_login_time", new Date());
					// Get the user statistics and updates it
					ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
					ustats.setProperty("user_stats_failed", 0L );
					ustats.setProperty("user_stats_last", new Date());		

					// Batch operation
					List<Entity> logs = Arrays.asList(log,ustats);
					datastore.put(txn,logs);
					txn.commit();
					Transaction txn1 = datastore.beginTransaction();

					token = new AuthToken(data.username, data.checked);

					Entity auth = new Entity("Token", token.tokenID);

					auth.setProperty("auth_tokenID", token.tokenID);
					auth.setProperty("auth_username", token.username);
					auth.setProperty("auth_expirData", token.expirationData);
					auth.setProperty("auth_creationData", token.creationData);

					datastore.put(txn1,auth);

					txn1.commit();
					// Return token
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}

			LOG.info("User '" + data.username + "' logged in sucessfully.");
			return Response.ok(g.toJson(token)).build();				
			//} else {
			// Incorrect password
			/*ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn,ustats);				
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();	*/			
			//}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			int counter = 0;
			AuthToken token = null;
			while(counter < 5){
				try{
					Entity user = new Entity("User", data.username);
					user.setProperty("user_email", data.username);

					user.setProperty("user_name", name);

					user.setProperty("user_type", "Cidadão");

					user.setProperty("user_city", " ");

					user.setProperty("user_address", " ");
					ArrayList<String> s = new ArrayList<String>();
					s.add("test");
					user.setProperty("user_liked", s);
					user.setProperty("user_confirmedEmail", true);

					user.setProperty("user_notifications", s);
					user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));

					user.setUnindexedProperty("user_creation_time", new Date());

					datastore.put(txn,user);
					LOG.info("User registered " + data.username);

					txn.commit();

					Transaction txn1 = datastore.beginTransaction();

					token = new AuthToken(data.username, data.checked);

					Entity auth = new Entity("Token", token.tokenID);

					auth.setProperty("auth_tokenID", token.tokenID);
					auth.setProperty("auth_username", token.username);
					auth.setProperty("auth_expirData", token.expirationData);
					auth.setProperty("auth_creationData", token.creationData);

					datastore.put(txn1,auth);

					txn1.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}	
			return Response.ok(g.toJson(token)).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@DELETE
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogout(pt.unl.fct.di.apdc.firstwebapp.util.AuthToken token) {
		System.out.println("tokenid "+token.tokenID);
		System.out.println("Attempt to logout user: " + token.username);

		LOG.fine("Attempt to logout user: " + token.username);
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					Key tokenKey = KeyFactory.createKey("Token", token.tokenID);

					//Entity tokenEnt = datastore.get(tokenKey);

					datastore.delete(txn, tokenKey);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok("{}").build();		

		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/registerOccurrence/{email}/{city}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAddress(pt.unl.fct.di.apdc.firstwebapp.util.OccurrenceData occurrence, @PathParam("email") String email, @PathParam("city") String city) {
		String occID = UUID.randomUUID().toString();
		System.out.println("Attempt to register Occurrence : " + occID);

		Transaction txn = datastore.beginTransaction();
		if(occurrence.description.contains("<div") || occurrence.location.contains("<div") || occurrence.description.contains("script") || occurrence.location.contains("script") || occurrence.description.contains(".js") || occurrence.location.contains(".js") || occurrence.description.length() > 200){
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		}
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("Occurrence", occID);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity user = new Entity("Occurrence", occID);
					user.setProperty("occurrence_description", occurrence.description);
					user.setProperty("occurrence_location", occurrence.location);
					user.setProperty("occurrence_id", occID);
					user.setProperty("occurrence_type",occurrence.typeOc);
					user.setProperty("occurrence_city",city);
					user.setProperty("occurrence_solver","");

					String mastertype = "";
					if(occurrence.typeOc.equals("Inundação") || occurrence.typeOc.equals("Excesso de Neve")){
						mastertype = "Sazonal";
					}else if(occurrence.typeOc.equals("Lixo a transbordar de caixote")){
						mastertype = "Lixo";
					}else if(occurrence.typeOc.equals("Animais Mortos")){
						mastertype = "Saúde Pública";
					}else if(occurrence.typeOc.equals("Buraco no Chão") || occurrence.typeOc.equals("Passeio Danificado") || occurrence.typeOc.equals("Sinal Danificado") || occurrence.typeOc.equals("Material do Parque danificado")){
						mastertype = "Rua/Parque Danificado";
					}else if(occurrence.typeOc.equals("Estacionamento Ilegal") || occurrence.typeOc.equals("Bicicleta Abandonada") || occurrence.typeOc.equals("Veículo Abandonado")){
						mastertype = "Veículos/Estacionamento";
					}else if(occurrence.typeOc.equals("Semáforo") || occurrence.typeOc.equals("Luzes do Parque") || occurrence.typeOc.equals("Luzes da Rua")){
						mastertype = "Luzes";
					}else if(occurrence.typeOc.equals("Graffiti Ilegal")){
						mastertype = "Graffiti Ilegal";
					}else if(occurrence.typeOc.equals("Risco de queda")){
						mastertype = "Árvores";
					}

					user.setProperty("occurrence_type_master", mastertype);

					user.setProperty("occurrence_urgency",occurrence.urgency);
					user.setProperty("occurrence_whodid",occurrence.whodid);
					user.setProperty("occurrence_likes", 0);
					user.setProperty("occurrence_anonim", occurrence.anonim);
					user.setProperty("occurrence_status", "Aberto");
					user.setProperty("occurrence_rating", -1.0);
					user.setProperty("occurrence_creator", occurrence.creator);
					user.setProperty("occurrence_creator_id", email);
					user.setProperty("occurrence_filename", occurrence.fileName);
					ArrayList<String> s = new ArrayList<String>();
					s.add("test");
					s.add(email);
					user.setProperty("occurrence_following", s);

					user.setUnindexedProperty("occurrence_creation_time", new Date());
					user.setProperty("occurrence_creation_time_ms", new Date().getTime());

					datastore.put(txn,user);
					LOG.info("Occurrence registered " + occurrence.description);

					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	@POST
	@Path("/addComment/{occID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addComment(pt.unl.fct.di.apdc.firstwebapp.util.CommentData comment, @PathParam("occID") String occID) {
		String comID = UUID.randomUUID().toString();
		System.out.println("Attempt to register Comment : " + comment.commentmessage);

		if(comment.commentmessage.contains("script") || comment.commentmessage.length() > 200){
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("Comment", comID);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity user = new Entity("Comment", comID);
					user.setProperty("comment_text", comment.commentmessage);
					user.setProperty("comment_id", comID);
					user.setProperty("comment_creator", comment.creator);
					user.setProperty("comment_occID", occID);

					user.setUnindexedProperty("comment_creation_time", new Date());
					user.setProperty("comment_creation_time_ms", new Date().getTime());

					datastore.put(txn,user);
					LOG.info("Comment registered " + comment.commentmessage);

					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	@GET
	@Path("/getAllOccurrences/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrences(@PathParam("cursor") String cursor) {

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(9);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Occurrence").addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);;
		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::sss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();
		System.out.println("{results: "+g.toJson(results)+"}");
		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);
		inputObj.addProperty("cursor", cursorString);
		return Response.ok(inputObj.toString()).build();		
	}


	@GET
	@Path("/getAllOccurrencesStatus/{status}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrencesStatus(@PathParam("status") String status, @PathParam("cursor") String cursor) {
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(9);
		Filter propertyFilter = new FilterPredicate("occurrence_status", FilterOperator.EQUAL, status);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter).addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);

		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::sss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();

		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);

		inputObj.addProperty("cursor", cursorString);

		return Response.ok(inputObj.toString()).build();		
	}



	@GET
	@Path("/getAllOccurrencesSolving/{solver}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrencesSolving(@PathParam("solver") String solver, @PathParam("cursor") String cursor) {
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(9);
		Filter propertyFilter = new FilterPredicate("occurrence_solver", FilterOperator.EQUAL, solver);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter).addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);

		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);

		String cursorString = results.getCursor().toWebSafeString();

		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);

		inputObj.addProperty("cursor", cursorString);

		return Response.ok(inputObj.toString()).build();		
	}



	@GET
	@Path("/getAllOccurrencesMinhas/{userID}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrencesMinhas(@PathParam("userID") String userID, @PathParam("cursor") String cursor) {
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(9);
		Filter propertyFilter = new FilterPredicate("occurrence_creator_id", FilterOperator.EQUAL, userID);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter).addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);

		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::sss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();

		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);

		inputObj.addProperty("cursor", cursorString);

		return Response.ok(inputObj.toString()).build();		
	}





	@GET
	@Path("/getAllOccurrencesCategory/{category}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrencesCategory(@PathParam("category") String category, @PathParam("cursor") String cursor) {

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(9);

		Filter propertyFilter = new FilterPredicate("occurrence_type_master", FilterOperator.EQUAL, category);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Occurrence").setFilter(propertyFilter).addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);

		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::sss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();

		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);

		inputObj.addProperty("cursor", cursorString);

		return Response.ok(inputObj.toString()).build();		
	}



	@GET
	@Path("/getOneOccurrence/{occID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getOneOccurrence(@PathParam("occID") String occID) {

		Key userKey = KeyFactory.createKey("Occurrence", occID);
		Entity e;
		try {
			e = datastore.get(userKey);
			return Response.ok(g.toJson(e)).build();
		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).entity("Occurrence doesn't exist").build(); 
		}

	}

	@GET
	@Path("/getAllComments/{occID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllComments(@PathParam("occID") String occID) {

		Filter propertyFilter = new FilterPredicate("comment_occID", FilterOperator.EQUAL, occID);

		Query ctrQuery = new Query("Comment").setFilter(propertyFilter).addSort("comment_creation_time_ms", SortDirection.DESCENDING);;
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		System.out.println("RESULTS:::commentsss  "+ results);
		return Response.ok(g.toJson(results)).build();

	}

	@GET
	@Path("/getAllCommentsPage/{occID}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllCommentsPage(@PathParam("occID") String occID,@PathParam("cursor") String cursor) {


		Filter propertyFilter = new FilterPredicate("comment_occID", FilterOperator.EQUAL, occID);

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(4);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Comment").setFilter(propertyFilter).addSort("comment_creation_time_ms", SortDirection.DESCENDING);;
		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::commmentttttsssss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();
		System.out.println("{results: "+g.toJson(results)+"}");
		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);
		inputObj.addProperty("cursor", cursorString);
		return Response.ok(inputObj.toString()).build();


	}



	@GET
	@Path("/getAllOccurrencesDiogo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllOccurrencesDiogo() {

		Query ctrQuery = new Query("Occurrence").addSort("occurrence_creation_time_ms", SortDirection.DESCENDING);;
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		System.out.println("RESULTS:::occccccsssss  "+ results);
		return Response.ok(g.toJson(results)).build();

	}



	@GET
	@Path("/getAllNotificationsPage/{userEmail}/{cursor}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllNotificationsPage(@PathParam("userEmail") String userEmail, @PathParam("cursor") String cursor) {


		Filter propertyFilter = new FilterPredicate("not_userToNotify", FilterOperator.EQUAL, userEmail);

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(4);

		// If this servlet is passed a cursor parameter, let's use it.
		String startCursor = cursor;
		if (!startCursor.equals("first")) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		}

		Query ctrQuery = new Query("Notification").setFilter(propertyFilter).addSort("not_date_ms", SortDirection.DESCENDING);;
		QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fetchOptions);
		System.out.println("RESULTS:::commmentttttsssss  "+ results);

		String cursorString = results.getCursor().toWebSafeString();
		System.out.println("{results: "+g.toJson(results)+"}");
		JsonObject inputObj  = g.fromJson("{results: "+g.toJson(results)+"}", JsonObject.class);
		inputObj.addProperty("cursor", cursorString);
		return Response.ok(inputObj.toString()).build();
	}


	@GET
	@Path("/getAllNotifications/{userEmail}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllNotifications(@PathParam("userEmail") String userEmail) {

		Filter propertyFilter = new FilterPredicate("not_userToNotify", FilterOperator.EQUAL, userEmail);

		Query ctrQuery = new Query("Notification").setFilter(propertyFilter).addSort("not_date_ms", SortDirection.DESCENDING);;
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(4));
		System.out.println("RESULTS:::notsssss  "+ results);
		return Response.ok(g.toJson(results)).build();

	}

	@GET
	@Path("/getAllNotificationsAndroid/{userEmail}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllNotificationsAndroid(@PathParam("userEmail") String userEmail) {

		Filter propertyFilter = new FilterPredicate("not_userToNotify", FilterOperator.EQUAL, userEmail);

		Query ctrQuery = new Query("Notification").setFilter(propertyFilter).addSort("not_date_ms", SortDirection.DESCENDING);;
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(50));
		System.out.println("RESULTS:::notsssss  "+ results);
		return Response.ok(g.toJson(results)).build();

	}


	@GET
	@Path("/getStatistics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getStatistics() {
		Key userKey = KeyFactory.createKey("Statistics", 5);
		Entity e;
		try {
			e = datastore.get(userKey);
		} catch (EntityNotFoundException e1) {
			e = new Entity("Statistics", 5);
			e.setProperty("stats_occurrencesReported",0);
			e.setProperty("stats_occurrencesResolved",0);
			e.setProperty("stats_totalUsers",0);
		}

		return Response.ok(g.toJson(e)).build();		

	}

	@DELETE
	@Path("/checkTimes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkTimes(){

		Transaction txn = null;
		Query ctrQuery = new Query("Token");
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		for(int i = 0; i < results.size();i++){
			if ((long)(results.get(i).getProperty("auth_expirData")) <= System.currentTimeMillis() ){
				txn = datastore.beginTransaction();
				Key key = KeyFactory.createKey("Token", (String)(results.get(i).getProperty("auth_tokenID")));
				datastore.delete(txn, key);
				txn.commit();
			}
		}
		System.out.println(results.size());

		return Response.ok().build();
	}

	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUsernameAvailable( pt.unl.fct.di.apdc.firstwebapp.util.LoginData data ) {

		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date yesterday = cal.getTime();

				// Obtain the user login statistics
				Filter propertyFilter =
						new FilterPredicate("user_login_time", FilterOperator.GREATER_THAN_OR_EQUAL, yesterday);
				Query ctrQuery = new Query("UserLog").setAncestor(KeyFactory.createKey("User", data.username))
						.setFilter(propertyFilter)
						.addSort("user_login_time", SortDirection.DESCENDING);
				ctrQuery.addProjection(new PropertyProjection("user_login_time", Date.class));
				ctrQuery.addProjection(new PropertyProjection("user_login_ip", String.class));
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(30));

				return Response.ok(g.toJson(results)).build();

			} else {
				LOG.warning("Wrong password for username: " + data.username );
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		}
	}


	@GET
	@Path("/getUser/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUserWithId( @PathParam("id") String id ) {
		LOG.info("EPAaaaa "+ id);
		Key userKey = KeyFactory.createKey("User", id);
		try {
			Entity user = datastore.get(userKey);
			return Response.ok(g.toJson(user)).build();
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Status.FORBIDDEN).build();				

		}
	}

	@PUT
	@Path("/updateRating/{id}/{rating}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changeOccRating(@PathParam("rating") String rating, @PathParam("id") String id) {
		System.out.println("rating is "+rating);
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					Key userKey = KeyFactory.createKey("Occurrence", id);
					Entity user = datastore.get(userKey);
					user.setProperty("occurrence_rating", rating);

					datastore.put(user);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + id);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	@SuppressWarnings("unchecked")
	@POST
	@Path("/updateOcc/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changeOccStatus(@PathParam("id") String id) {
		Transaction txn = datastore.beginTransaction();

		try {
			Key userKey = KeyFactory.createKey("Occurrence", id);
			Entity user = datastore.get(userKey);
			user.setProperty("occurrence_status", "Resolvido");
			user.setUnindexedProperty("occurrence_change_time", new Date());
			String locemtype = (String)user.getProperty("occurrence_type") + " em " + (String)user.getProperty("occurrence_location");
			System.out.println("NOTIFICATION IS " + locemtype);
			List <String> f = (List<String>) user.getProperty("occurrence_following");
			int counter = 0;

			while(counter < 5){

				try{
					for(String s: f){

						if(!s.equals("test")){

							Transaction txn1 = datastore.beginTransaction();

							String notID = UUID.randomUUID().toString();

							Entity notification = new Entity("Notification", notID);
							notification.setProperty("not_id", notID);
							notification.setProperty("not_message", locemtype);
							notification.setUnindexedProperty("not_date", new Date());
							notification.setProperty("not_date_ms", new Date().getTime());
							notification.setProperty("not_occID", id);					
							notification.setProperty("not_userToNotify", s);				
							notification.setProperty("not_opened", false);				

							datastore.put(txn1,notification);

							txn1.commit();

						}
					}
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			datastore.put(txn, user);
			txn.commit();
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + id);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	@PUT
	@Path("/updatePassword/{id}/{current}/{new}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changeUserPass(@PathParam("current") String currentPass, @PathParam("new") String newPass, @PathParam("id") String email) {

		if( newPass.equals(currentPass)) {
			return Response.status(111).entity("Missing or wrong parameter.").build();
		}

		if( !(newPass.length() > 5 && newPass.matches(".*\\d+.*") && newPass.matches(".*[a-z].*"))) {
			return Response.status(222).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();

		try {

			Key userKey = KeyFactory.createKey("User", email);
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(currentPass))) {
				int counter = 0;
				while(counter < 5){
					try{
						user.setProperty("user_pwd", DigestUtils.sha512Hex(newPass));
						datastore.put(user);
						txn.commit();
						break;
					}catch(ConcurrentModificationException e1){
						counter++;
					}
				}
				return Response.ok().build();
			} else {
				// Incorrect password
				LOG.warning("Wrong password for username: " + email);
				return Response.status(Status.FORBIDDEN).build();				
			}


		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + email);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	@PUT
	@Path("/updateWorkFinished/{occ}/{solver}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateWorkFinished(@PathParam("occ") String occ, @PathParam("solver") String solver) {

		Transaction txn = datastore.beginTransaction();
		Filter propertyFilter1 = CompositeFilterOperator.and(new FilterPredicate("user_email", FilterOperator.EQUAL, solver),
				new FilterPredicate("occurrence_id", FilterOperator.EQUAL, occ));
		Query ctrQuery1 = new Query("WorksOn").setFilter(propertyFilter1);
		List<Entity> results = datastore.prepare(ctrQuery1).asList(FetchOptions.Builder.withLimit(1));
		try {
			if(results.isEmpty()){
				LOG.warning("HJDNNDKE");
				return Response.status(Status.NOT_FOUND).build();				
			}
			String idd = "";
			for(Entity e: results){
				idd = (String)e.getProperty("workson_id");
				break;
			}
			LOG.warning("iddd issss "+idd);

			Key userKey = KeyFactory.createKey("WorksOn", idd);
			Entity user = datastore.get(userKey);
			int counter = 0;
			while(counter < 5){
				try{
					user.setProperty("work_finished", true);
					datastore.put(txn, user);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();



		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + solver);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	@PUT
	@Path("/updateProfile/{id}/{name}/{morada}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response changeUserProfile(@PathParam("id") String email, @PathParam("name") String name, @PathParam("morada") String morada) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					Key userKey = KeyFactory.createKey("User", email);
					Entity user = datastore.get(userKey);
					user.setProperty("user_name", name);
					user.setProperty("user_address", morada);
					datastore.put(user);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed attempt to get the address for username: " + email);
			return Response.status(Status.NOT_FOUND).build();
		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}





	@SuppressWarnings("unchecked")
	@PUT
	@Path("/like/{occid}/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response likeOcc(@PathParam("occid") String occID, @PathParam("userid") String userID) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					// If the entity does not exist an Exception is thrown. Otherwise,
					Key userKey = KeyFactory.createKey("User", userID);

					Entity user = datastore.get(userKey);

					ArrayList<String> s = (ArrayList<String>) user.getProperty("user_liked");
					if(s.size() != 0){
						if(s.contains(occID)){
							return Response.status(Status.BAD_REQUEST).entity("Like already exists.").build();
						}
					}

					s.add(occID);

					user.setProperty("user_liked", s);

					datastore.put(txn,user);

					txn.commit();


					Transaction txn1 = datastore.beginTransaction();

					Key occKey = KeyFactory.createKey("Occurrence", occID);
					Entity occ = datastore.get(occKey);
					long likes = (long) occ.getProperty("occurrence_likes");
					likes += 1;
					occ.setProperty("occurrence_likes", likes);

					datastore.put(txn1,occ);

					txn1.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}

			return Response.ok("{}").build();
		} catch (EntityNotFoundException e) {

			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}


	@SuppressWarnings("unchecked")
	@PUT
	@Path("/unlike/{occid}/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unlikeOcc(@PathParam("occid") String occID, @PathParam("userid") String userID) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					// If the entity does not exist an Exception is thrown. Otherwise,
					Key userKey = KeyFactory.createKey("User", userID);

					Entity user = datastore.get(userKey);

					ArrayList<String> s = (ArrayList<String>) user.getProperty("user_liked");
					if(s.size() != 0){
						if(!s.contains(occID)){
							return Response.status(Status.BAD_REQUEST).entity("Like already exists.").build();
						}
					}

					s.remove(occID);

					user.setProperty("user_liked", s);

					datastore.put(txn,user);

					txn.commit();


					Transaction txn1 = datastore.beginTransaction();

					Key occKey = KeyFactory.createKey("Occurrence", occID);
					Entity occ = datastore.get(occKey);
					long likes = (long) occ.getProperty("occurrence_likes");
					likes -= 1;
					occ.setProperty("occurrence_likes", likes);

					datastore.put(txn1,occ);

					txn1.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}

			return Response.ok("{}").build();
		} catch (EntityNotFoundException e) {

			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}


	@PUT
	@Path("/openNotification/{notID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response openNotification(@PathParam("notID") String notID) {
		Transaction txn = datastore.beginTransaction();
		try {
			int counter = 0;
			while(counter < 5){
				try{
					// If the entity does not exist an Exception is thrown. Otherwise,
					Key userKey = KeyFactory.createKey("Notification", notID);

					Entity user = datastore.get(userKey);

					user.setProperty("not_opened", true);

					datastore.put(txn,user);

					txn.commit();

					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}

			return Response.ok("{}").build();
		} catch (EntityNotFoundException e) {

			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("Occurrence already exists.").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}


}

