package pt.unl.fct.di.apdc.firstwebapp.util;

public class CommentData {

	public String creator;
	public String commentmessage;
	
	public CommentData(){}
	
	public CommentData(String creator, String commentmessage) {
		this.commentmessage = commentmessage;
		this.creator = creator;
	}
	
}
