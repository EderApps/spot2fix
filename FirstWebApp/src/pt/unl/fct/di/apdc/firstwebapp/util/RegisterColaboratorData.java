package pt.unl.fct.di.apdc.firstwebapp.util;

public class RegisterColaboratorData {
	public String city;
	public String section;
	public String companyname;
	public String email;
	public String name;
	public String password;
	public String confirmPass;
	
	
	public RegisterColaboratorData() { } //Nothing to be done here...

	
	public RegisterColaboratorData(String companyname, String name) {
		this.companyname = companyname;
		this.name = name;
	}
	
	public RegisterColaboratorData(String section, String companyname, String email, String name,
			String password, String confirmPass, String city) {
		
		this.city = city;
		this.section = section;
		this.companyname = companyname;
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmPass = confirmPass;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return 
			   validField(name) &&
			   validField(section) &&
			   validField(companyname) &&
			   validField(email) &&
			   validField(password) &&
			   validField(confirmPass) && validField(city) &&
			   password.equals(confirmPass) &&
			   email.contains("@");		
	}
}
