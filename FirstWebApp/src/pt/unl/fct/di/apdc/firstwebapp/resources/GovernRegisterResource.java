package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.mail.smtp.SMTPTransport;

@Path("/govReg")
public class GovernRegisterResource {
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(PasswordRecoverResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();
	private SecureRandom random = new SecureRandom();

	@POST
	@Path("/{city}/{email}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response recoverPassword(@PathParam ("city") String city, @PathParam ("email") String email) {
		/*if( ! data.validRegistration() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}*/

		Transaction txn = datastore.beginTransaction();
		String id = DigestUtils.sha512Hex(new BigInteger(130, random).toString(32));

		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key govKey = KeyFactory.createKey("GovRegisterInfo", id);
			Entity gov = datastore.get(govKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already tryed to recover password, please check your email.").build(); 
		} catch (EntityNotFoundException e) {
			int counter = 0;
			while(counter < 5){
				try{
					Entity gov = new Entity("GovRegisterInfo", id);
					gov.setProperty("gov_city", city);
					gov.setProperty("gov_username", email);
					gov.setProperty("user_creation_time", new Date());
					datastore.put(gov);
					txn.commit();
					break;
				}catch(ConcurrentModificationException e1){
					counter++;
				}
			}
			sendFromGMail(email,"Confirmacao de conta","Clique no link a seguir para confirmar a sua conta: https://fit-sanctum-159416.appspot.com/govregist.html?id=" + id);
			return Response.ok(g.toJson(id)).build();	
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}

	@GET 
	@Path("/getCity/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUsername(@PathParam("id") String id) throws EntityNotFoundException {
		try{
			Key govKey = KeyFactory.createKey("GovRegisterInfo", id);
			Entity gov = datastore.get(govKey);
			return Response.ok(g.toJson(gov)).build();
		}
		catch (EntityNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity("O link poderá ter expirado.").build(); 
		}
	}

	@DELETE
	@Path("/remove/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response logout(@PathParam ("id") String id) throws EntityNotFoundException {

		Transaction txn = datastore.beginTransaction();
		int counter = 0;
		while(counter < 5){
			try{
				Key govKey = KeyFactory.createKey("GovRegisterInfo", id);
				Entity user = datastore.get(govKey);
				datastore.delete(txn, govKey );
				txn.commit();
				break;
			}catch(ConcurrentModificationException e1){
				counter++;
			}
		}
		return Response.ok("{}").build();

	}

	public void sendFromGMail(String to, String subject, String body) {

		String host = "smtp.gmail.com";
		final String from = "spot2fix@gmail.com";
		final String pass = "5eder1spot";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, pass);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(body);

			Transport.send(message);
		}
		catch (AddressException ae) {
			ae.printStackTrace();
		}
		catch (MessagingException me) {
			me.printStackTrace();
		}
	}
}
