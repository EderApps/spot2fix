var idU;
var username;
var check ;

checkID = function(){
	console.log(idU);
	$.ajax({
		type: "GET",
		url: "/rest/recover/getusername/" + idU,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			console.log("here");
            username = response;
            check = true;
		},
		error: function(response) {
			console.log("not here");
			alert("Pedimos desculpa mas este link de recuperação não existe, poderá ter expirado.");
	        window.location.href = "/";
			check = false;
		}
	});

	event.preventDefault();
};

removeIDEntry = function(id){
    $.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/recover/remove/" + idU,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		}
	});
}

captureData = function(event) {

	var data = $('form[name="recovery"]').jsonify();
	
	data.passwordrecovery = document.getElementById("passwordrecovery").value;
	data.passwordrecoveryc = document.getElementById("passwordrecoveryc").value;

    if(data.passwordrecovery != data.passwordrecoveryc){
        alert("Ambas as passwords têm de ser iguais.");
    }
    else{
            console.log(data);
            console.log(JSON.stringify(data));
            $.ajax({
                type: "PUT",
                url: "/rest/register/" + username + "/attributes/password/"+data.passwordrecovery,
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                //dataType: "json",
                success: function(response) {
                    removeIDEntry();
                },
                error: function(response) {
                	alert("Password insegura, tente de novo.");
                }
                //data: JSON.stringify(data.passwordrecovery)
            });

            event.preventDefault();
           
    }
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};
 

window.onload = function() {
    idU = getQueryVariable("id");
    checkID();
    
    console.log(idU);
    var frms0 = $('form[name="recovery"]');
    frms0[0].onsubmit = captureData;
}