function checkScroll(){
	var startY = $('.navbar').height() * 0.1; //The point where the navbar changes in px

	if($(window).scrollTop() > startY){
		$('.navbar').addClass("scrolled");
		$('.dropdown').addClass("scrolled");
	}else{
		$('.navbar').removeClass("scrolled");
	}
}
logout = function(event) {
	var token = localStorage.getItem('tokenID');
	var tokenname = localStorage.getItem('tokenIDname');
	var tokenexp = localStorage.getItem('tokenIDexpir');
	var tokencre = localStorage.getItem('tokenIDcreate');
	var data = {};
	data.tokenID = token;
	data.username = tokenname;
	data.expirationData = tokenexp;
	data.creationData = tokencre;
	console.log(data);

	console.log(JSON.stringify(data));
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/logout/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
			localStorage.setItem('userLocation', "");
			localStorage.setItem('userType', "");
			localStorage.setItem('userEmail', "");
			localStorage.setItem('userName', "");
			localStorage.setItem('userLikes', "");

			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
	signOut();

}


captureData = function(event) {

	console.log("ekfmkrnkn");

	var data = $('form[name="login"]').jsonify();

	var checked = document.getElementById("check").checked;
	data.checked = checked;
	console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			// Store token id for later use in localStorage
			console.log(response.username);
			console.log(response.tokenID);
			console.log(response.creationData);
			console.log(response.expirationData);

			localStorage.setItem('tokenIDname', response.username);
			localStorage.setItem('tokenID', response.tokenID);
			localStorage.setItem('tokenIDcreate', response.creationData);
			localStorage.setItem('tokenIDexpir', response.expirationData);


			window.location.href = "/userarea.html";

		},
		error: function(response) {
			if(response.status == 333){
				alert("A sua conta ainda não foi activada. Por favor vá à sua caixa de correio e confirme o seu email.");
			}else{
				alert("O email/password que inseriu está incorreto.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function onFailure(error) {
	alert(error);
}
function renderButton() {
	gapi.signin2.render('gSignIn', {
		'scope': 'profile email',
		'width': 290,
		'height': 50,
		'longtitle': true,
		'theme': 'dark',
		'onsuccess': onSuccess,
		'onfailure': onFailure
	});
}
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		$('.userContent').html('');
		$('#gSignIn').slideDown('slow');
	});
}

function onSuccess(googleUser) {

	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	var checked = document.getElementById("check").checked;
	var data = {};
	data.username = profile.getEmail();
	data.password = profile.getId();
	data.checked = checked;
	console.log(JSON.stringify(data));
	if(localStorage.getItem('tokenID') == ""){

		$.ajax({
			type: "POST",
			url: "/rest/login/logingoogle/" + profile.getEmail(),
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				// Store token id for later use in localStorage
				console.log(response.username);
				console.log(response.tokenID);
				console.log(response.creationData);
				console.log(response.expirationData);

				localStorage.setItem('tokenIDname', response.username);
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenIDcreate', response.creationData);
				localStorage.setItem('tokenIDexpir', response.expirationData);

				window.location.href = "/userarea.html";

			},
			error: function(response) {
				//alert("Error: "+ response.status);
			},
			data: JSON.stringify(data)
		});
	}
}

captureDataReg = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Enviámos um mail para o endereço que registou. Por favor confirme para poder entrar.");
			window.location.href = "/";
		},
		error: function(response) {
			if(response.status == 111){
				alert("Registo inválido. Por favor verifique todos os campos.");
			}else if(response.status == 222){
				alert("Email inválido. Os emails têm de conter '@'.");
			}else if(response.status == 333){
				alert("Password inválida. As passwords têm de ter 6 caracteres ou mais, e conter números e letras.");
			}else if(response.status == 444){
				alert("A password que inseriu não coincide com a confirmação da password. Tente de novo.");
			}else if(response.status == 400){
				alert("Já existe um utilizador registado com o email que inseriu.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

var nome;
var email;
var cidade;
var morada;

sendEmail = function(email, id){
	/*  var obj, dbParam, xmlhttp, myObj, x, txt = "";
	obj = { "email": email, "id": id};
	dbParam = JSON.stringify(obj);
	    console.log(dbParam);
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	        console.log("Email sent");
	    }
	    xmlhttp.open("POST", "json_email_recovery.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(dbParam);
	};*/
	var obj = { "email": email, "id": id};
	$.ajax({ 
		type: "POST",
		url: "json_email_recovery.php",
		data: {x: obj},
		success: function(response){
			console.log(JSON.stringify(response));
			alert("Email WAS Sent!");
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});

};

var isValidUser;
isValid = function(username){
	console.log("marian "+username);
	$.ajax({
		type: "GET",
		url: "/rest/register/getusername/" + username,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			isValidUser = true;
			$.ajax({
				type: "POST",
				url: "/rest/recover/" + username,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					//var lin = response;
					console.log(response);
					//enviar mail para o php
					alert("Mail enviado!");
				},
				error: function(response) {
					alert("Error: "+ response.status);
				}
			});
		},
		error: function(response) {
			isValidUser = false;
		}
	});

	event.preventDefault();
};

recWord = function(event) {
	var data = $('form[name="recovery"]').jsonify();
	isValid(data.recovemail);
};

var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}


var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}

recovHide = function(event){
	document.getElementById("acc-recovery").style.display = "none";
}

editPerfil = function(event){
	event.preventDefault();
	console.log("perfil");
	var data = $('form[name="changeperfil"]').jsonify();
	$.ajax({
		type: "PUT",
		url: "/rest/login/updateProfile/" + localStorage.getItem('userEmail') + "/" + data.cgname +"/" + data.cglocation ,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Informação atualizada com sucesso.");
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
		}
	});
}

editSenha = function(event){
	event.preventDefault();
	console.log("senha");
	var data = $('form[name="changepassword"]').jsonify();
	console.log(data.newpassword + " "+ data.newconfirm);
	if(data.newpassword == data.newconfirm){
		$.ajax({
			type: "PUT",
			url: "/rest/login/updatePassword/" + localStorage.getItem('userEmail') + "/" + encodeURIComponent(data.currentpassword) +"/" + encodeURIComponent(data.newpassword) ,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				alert("Password atualizada com sucesso.");
				window.location.href = "/";

			},
			error: function(response) {
				if(response.status == 111){
					alert("Nova password é igual à password antiga. Insira uma password diferente.");
				}

				if(response.status == 222){
					alert("Nova password inválida. A sua password tem de ter pelo menos 6 carateres e tem de conter números e letras.");
				}
			}
		});
	}else{
		alert("Nova password e a sua confirmação não são iguais.");
	}
}
undisplayNumNotifications = function(){
	document.getElementById("numNotifications").innerHTML = "0";
	var i;
	for(i = 0; i < myNotifications.length; i++){
		var notIDcurr = myNotifications[i].propertyMap.not_id;
		var openedNot = myNotifications[i].propertyMap.not_opened;
		if(!openedNot){
		$.ajax({
			type: "PUT",
			url: "/rest/login/openNotification/" + notIDcurr ,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
			},
			error: function(response) {
				//alert("Error: "+ response.status);
			}
		});
		}
	}
}


clickedNotification = function(event){
	console.log(this.id);
	console.log(this.value);
	var vall = Number(this.value);
	
	var notIDcurr = myNotifications[vall].propertyMap.not_id;
	var occIDcurr = myNotifications[vall].propertyMap.not_occID;
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getOneOccurrence/" + occIDcurr,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response1){        
					localStorage.setItem("currReport_type",response1.propertyMap.occurrence_type);
					localStorage.setItem("currReport_location",response1.propertyMap.occurrence_location);
					localStorage.setItem("currReport_description",response1.propertyMap.occurrence_description);
					localStorage.setItem("currReport_creation_time",response1.propertyMap.occurrence_creation_time.value);
					localStorage.setItem("currReport_likes",response1.propertyMap.occurrence_likes);
					localStorage.setItem("currReport_urgency",response1.propertyMap.occurrence_urgency);
					localStorage.setItem("currReport_status",response1.propertyMap.occurrence_status);
					localStorage.setItem("currReport_creator",response1.propertyMap.occurrence_creator);
					localStorage.setItem("currReport_id",response1.propertyMap.occurrence_id);
					localStorage.setItem("currReport_filename",response1.propertyMap.occurrence_filename);
					localStorage.setItem("currReport_creationTime",response1.propertyMap.occurrence_creation_time.value);
					localStorage.setItem("currReport_rating",response1.propertyMap.occurrence_rating);
					localStorage.setItem("currReport_city",response1.propertyMap.occurrence_city);

					console.log(response1.propertyMap.occurrence_creation_time.value);
					if(response1.propertyMap.occurrence_status == "Resolvido"){
						localStorage.setItem("currReport_changeTime",response1.propertyMap.occurrence_change_time.value);
					}

					window.location.href = "/occurrence.html";
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		
	
}

var notifications;
var myNotifications = [];
var numNotificationsTotal;

window.onload = function() {
	if($('.navbar').length > 0){
		$(window).on("scroll load resize", function(){
			checkScroll();
		});
	}
	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){
		var now = +new Date();
		if(now > localStorage.getItem("tokenIDexpir")){
			logout();
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
		}else{
			document.getElementById("notifications").style.display = "block";

		$.ajax({
			type: "GET",
			url: "/rest/login/getUser/" + localStorage.getItem("tokenIDname") ,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				console.log(response);
				console.log(response.propertyMap);
				console.log(response.propertyMap.user_email);
				localStorage.setItem('userLocation', response.propertyMap.user_address);
				localStorage.setItem('userType', response.propertyMap.user_type);
				if(localStorage.getItem("userType") == "Governo"){
					localStorage.setItem("userCity",response.propertyMap.user_city);
				}
				if(localStorage.getItem("userType") == "Admin"){
					document.getElementById("addGovOrNot").style.display = "block";
					//document.getElementById("statsOrNot").style.display = "block";

				}else if(localStorage.getItem("userType") == "Governo"){
					document.getElementById("colabOrNot").style.display = "block";
				}
				localStorage.setItem('userEmail', response.propertyMap.user_email);
				localStorage.setItem('userName', response.propertyMap.user_name);
				$.ajax({ 
					type: "GET",
					//dataType: "json",
					url: "/rest/login/getAllNotificationsPage/" + response.propertyMap.user_email+"/"+"first",
					contentType: "application/json; charset=utf-8",
					crossDomain: true,
					success: function(response1){  

						if(response1.results.length != 0){
							currentCursorNoti = response1.cursor;
							responseCurrPageNoti = response1.results;
							console.log(response1.results.length);
							console.log("martaaaa");
							responseGlobalNoti = responseGlobalNoti.concat(response1.results);


							console.log(response1.results.length);
							var numNotifications = 0;
							document.getElementById("notifications").onclick = undisplayNumNotifications;

							for (i = 0; i < 4; i++) { 


								if(response1.results.length == i){
									break;
								}

								var readAlready = response1.results[i].propertyMap.not_opened;
								var message = response1.results[i].propertyMap.not_message;

								if(readAlready){
									document.getElementById("title"+i).innerHTML = "<strong>"+ message +"</strong>";
								}else{
									numNotifications++;
									document.getElementById("title"+i).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
								}
								myNotifications.push(response1.results[i]);
								var datei = response1.results[i].propertyMap.not_date.value;
								var idnot = response1.results[i].propertyMap.not_id;

								document.getElementById("notification"+i).style.display = "block";
								document.getElementById("notification"+i).onclick = clickedNotification;
								document.getElementById("date"+i).style.display = "block";
								document.getElementById("date"+i).innerHTML = datei;
								document.getElementById("title"+i).value = idnot;

								if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
									document.getElementById("text"+i).innerHTML = "";
								}else{
									document.getElementById("text"+i).innerHTML = "foi resolvido!";
								}
								
							}
							numNotificationsTotal = numNotifications;
							document.getElementById("numNotifications").innerHTML = numNotifications;
						}else{
							document.getElementById("prevPageNoti").style.display = "none";
							document.getElementById("nextPageNoti").style.display = "none";
							document.getElementById("numNotifications").innerHTML = "0";
						}
					},
					error: function(response) {
						alert("Error: "+ response.status);
						console.log(JSON.stringify(response));
					},
				});

				if(localStorage.getItem("userType") == "Cidadão"){
					document.getElementById("submitxi").style.display = "block";
				}
				document.getElementById("registar").style.display = "none";
				document.getElementById("entrar").style.display = "none";
				document.getElementById("sair").style.display = "block";
				document.getElementById("utilizador").style.display = "block";
				document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userName");
				if(localStorage.getItem("userType") == "Governo"){
					document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userCity");
				}
			},
			error: function(response) {
				//alert("Error: "+ response.status);
			}
		});
	}
	}
	if(localStorage.getItem('tokenID') == null || localStorage.getItem('tokenID') == ""){ //not logged in
		document.getElementById("registar").style.display = "block";
		document.getElementById("entrar").style.display = "block";
		document.getElementById("sair").style.display = "none";
		document.getElementById("utilizador").style.display = "none";
	}


	var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = captureData;

	var frms1 = $('form[name="register"]');     //var frms = document.getElementsByName("login");
	frms1[0].onsubmit = captureDataReg;
	document.getElementById("sair").onclick = logout;
	document.getElementById("prevPageNoti").onclick = prevPageNoti;
	document.getElementById("nextPageNoti").onclick = nextPageNoti;

	var frms3 = $('form[name="recovery"]'); //recuperacaodepassword
	frms3[0].onsubmit = recWord;

	nome = localStorage.getItem("userName");
	email = localStorage.getItem("userEmail");
	morada = localStorage.getItem("userLocation");

	document.getElementById("nome").innerHTML = nome;
	document.getElementById("email").innerHTML = email;
	document.getElementById("morada").innerHTML = morada;
	document.getElementById("cgname").value = nome;
	document.getElementById("cglocation").value = morada;

	var frms4 = $('form[name="changepassword"]'); //recuperacaodepassword
	frms4[0].onsubmit = editSenha;
	var frms5 = $('form[name="changeperfil"]'); //recuperacaodepassword
	frms5[0].onsubmit = editPerfil;
	
	var ic = 'http://maps.google.com/mapfiles/ms/micons/red-dot.png';


	geocoder.geocode( { 'address': morada}, function(results, status) {
		if (status == 'OK') {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: map,
				icon: ic,
				animation: google.maps.Animation.DROP,
				position: results[0].geometry.location
			});
		} else {
		}
	});

	document.getElementById("map_canvas").style.display = "block";

}

var hidden = true;
changeShow = function(event){
	if(hidden){
		document.getElementById("cgInfo").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("cgInfo").style.display = "none";
		hidden=true;
	}
};

changeHide = function(event){
	document.getElementById("cgInfo").style.display = "none";
	hidden=true;
};

var map;
var geocoder;
function showMapInit() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(38.736946, -9.142685);
	var myOptions = {
			zoom: 15,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

}


prevPageNoti = function(event) {
	console.log("prev TA AQUIIII");
	event.stopPropagation();

	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	if (currPage > 0){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage--;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		document.getElementById("notifications").onclick = undisplayNumNotifications;

		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;

			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
						r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}

}

var responseGlobalNoti = [];
var responseCurrPageNoti = [];
var currentCursorNoti = "first";

nextPageNoti = function(event) {
	console.log("next TA AQUIIII");

	event.stopPropagation();
	
	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	var t = (r.length / 4) - 1;


	if (currPage < t){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage++;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		undisplayNumNotifications;
		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;

			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
						r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}else{

		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllNotificationsPage/"+ localStorage.getItem('userEmail') +"/"+currentCursorNoti,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					document.getElementById("notification0").style.display = "none";
					document.getElementById("notification1").style.display = "none";
					document.getElementById("notification2").style.display = "none";
					document.getElementById("notification3").style.display = "none";
					currPage++;
					localStorage.setItem("currPageNoti",currPage);
					currentCursorNoti = response.cursor;
					responseCurrPageNoti = response.results;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobalNoti = responseGlobalNoti.concat(response.results);
					var j = 0;
					var numNotifications = 0;
					undisplayNumNotifications;
					for (i = 0; i < 4; i++) { 


						if(response.results.length == i){
							break;
						}

						var readAlready = response.results[i].propertyMap.not_opened;
						var message = response.results[i].propertyMap.not_message;

						if(readAlready){
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
						}else{
							numNotifications++;
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
						}
						myNotifications.push(response.results[i]);
						var datei = response.results[i].propertyMap.not_date.value;
						var idnot = response.results[i].propertyMap.not_id;

						document.getElementById("notification"+j).style.display = "block";
						document.getElementById("notification"+j).onclick = clickedNotification;
						document.getElementById("date"+j).style.display = "block";
						document.getElementById("date"+j).innerHTML = datei;
						document.getElementById("title"+j).value = idnot;

						if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
							document.getElementById("text"+j).innerHTML = "";
						}else{
							document.getElementById("text"+j).innerHTML = "foi resolvido!";
						}
												//r1.push(response.results[i]);
						j++;

					}
					console.log(response.results);
					responseCurrPageNoti = response.results;
					numNotificationsTotal = numNotifications;
					document.getElementById("numNotifications").innerHTML = numNotifications;
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});

	}
}
