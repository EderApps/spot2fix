function checkScroll(){
	var startY = $('.navbar').height() * 0.1; //The point where the navbar changes in px

	if($(window).scrollTop() > startY){
		$('.navbar').addClass("scrolled");
		$('.dropdown').addClass("scrolled");
	}else{
		$('.navbar').removeClass("scrolled");
	}
}


function CenterControl(controlDiv, map) {

	// Set CSS for the control border.
	var controlUI = document.createElement('div');
	controlUI.style.backgroundColor = '#fff';
	controlUI.style.border = '2px solid #fff';
	controlUI.style.borderRadius = '3px';
	controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	controlUI.style.cursor = 'pointer';
	controlUI.style.marginBottom = '22px';
	controlUI.style.textAlign = 'center';
	controlUI.title = 'Carregue para ver mais ocorrências';
	controlDiv.appendChild(controlUI);

	// Set CSS for the control interior.
	var controlText = document.createElement('div');
	controlText.style.color = 'rgb(25,25,25)';
	controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
	controlText.style.fontSize = '16px';
	controlText.style.lineHeight = '38px';
	controlText.style.paddingLeft = '5px';
	controlText.style.paddingRight = '5px';
	controlText.innerHTML = 'Ver mais!';
	controlUI.appendChild(controlText);

	// Setup the click event listeners: simply set the map to Chicago.
	controlUI.addEventListener('click', function() {
		see9MoreMap();
	});

}


var markers = [];
var infowindow = null;
typeSelected = function(event){

	var data = $('form[name="sectionSelect"]').jsonify();
	var typeList = data.typeOc;

	if(typeList == "map"){
		console.log("map");
		document.getElementById("listHideMe").style.display = "none";
		document.getElementById("mapHideMe").style.display = "block";
		document.getElementById("filtersHideMe").style.display = "none";

		google.maps.event.trigger(map, 'resize');

		var r = responseGlobal;
		if(whichType == 1){
			r = responseByStatus;
		}else if(whichType == 2){
			r = responseByCategory;
		}else if(whichType == 3){
			r = responseByMine;
		}else if(whichType == 4){
			r = responseBySolving;
		}
		var i;
		infowindow = new google.maps.InfoWindow({
			content: "a carregar..."
		});
		var marker;
		var kk = 0;
		for(i = 0; i<r.length;i++){
			var address = r[kk].propertyMap.occurrence_location;
			var type = r[kk].propertyMap.occurrence_type;
			var description = r[kk].propertyMap.occurrence_description;
			var datei = r[kk].propertyMap.occurrence_creation_time.value;
			var likes = r[kk].propertyMap.occurrence_likes;
			var urgency = r[kk].propertyMap.occurrence_urgency;
			var status = r[kk].propertyMap.occurrence_status;
			var creator = r[kk].propertyMap.occurrence_creator;
			var occid = r[kk].propertyMap.occurrence_id;
			var filename = r[kk].propertyMap.occurrence_filename;


			var jj = 0;
			geocoder.geocode( { 'address': r[i].propertyMap.occurrence_location}, function(results, status) {
				jj++;

				if (status == 'OK') {
					var content = "test ssss "+ i + " " + type +" "+ description;
					var ic = {
							url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
							scaledSize: new google.maps.Size(50, 50), // scaled size
							origin: new google.maps.Point(0,0) // origin
					};
					if (r[kk].propertyMap.occurrence_status == "Resolvido"){
						var ic = {
								url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
								scaledSize: new google.maps.Size(50, 50), // scaled size
								origin: new google.maps.Point(0,0) // origin
						};
					}
					marker = new google.maps.Marker({
						map: map,
						icon: ic,
						animation: google.maps.Animation.DROP,
						position: results[0].geometry.location
					});
					kk++;
					markers.push(marker);
					console.log("i "+jj);
					console.log("rlength "+r.length);
					var hm = r.length;
					console.log(hm);
					if(jj==hm){
						console.log();
						infoPopPliz();
					}
				} else {
				}
			});
		}

	}else{
		var j;
		for(j = 0; j<markers.length;j++){
			console.log("okkk hmhm "+j);
			markers[j].setMap(null);

		}
		console.log("type is " +typeList);
		document.getElementById("listHideMe").style.display = "block";
		document.getElementById("mapHideMe").style.display = "none";
		document.getElementById("filtersHideMe").style.display = "block";
		localStorage.setItem("currPage",0);
	}

}

function infoPopPliz() {
	console.log("hereeee");
	var r = responseGlobal;
	if(whichType == 1){
		r = responseByStatus;
	}else if(whichType == 2){
		r = responseByCategory;
	}else if(whichType == 3){
		r = responseByMine;
	}else if(whichType == 4){
		r = responseBySolving;
	}
	for (var i = 0; i < markers.length; i++) {
		console.log("sss "+i);

		var address = r[i].propertyMap.occurrence_location;
		var type = r[i].propertyMap.occurrence_type;
		var description = r[i].propertyMap.occurrence_description;
		var datei = r[i].propertyMap.occurrence_creation_time.value;
		var likes = r[i].propertyMap.occurrence_likes;
		var urgency = r[i].propertyMap.occurrence_urgency;
		var status = r[i].propertyMap.occurrence_status;
		var creator = r[i].propertyMap.occurrence_creator;
		var occid = r[i].propertyMap.occurrence_id;
		var filename = r[i].propertyMap.occurrence_filename;
		var anonim = r[i].propertyMap.occurrence_anonim;

		if (anonim){
			creator = "Anónimo";
		}
		
		var marker = markers[i];
		var content = "<b>Título: </b>"+ type+" em "+address +"<br /><b>Descrição: </b>"+ description +"<br /><b>Data: </b>"+ datei +"<br /><b>Estado: </b>"+ status +"<br /><b>Criador: </b>"+ creator +"<br /><button onclick=test(this.id) id="+occid+" value="+occid+">Ver Ocorrência</button>";
		google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
			return function() {
				//infowindow.close();
				infowindow.setContent(content);
				infowindow.open(map,marker);
			};
		})(marker,content,infowindow)); 
	}
}

function test(test_id){
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getOneOccurrence/" + test_id,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response1){        
			localStorage.setItem("currReport_type",response1.propertyMap.occurrence_type);
			localStorage.setItem("currReport_location",response1.propertyMap.occurrence_location);
			localStorage.setItem("currReport_description",response1.propertyMap.occurrence_description);
			localStorage.setItem("currReport_creation_time",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_likes",response1.propertyMap.occurrence_likes);
			localStorage.setItem("currReport_urgency",response1.propertyMap.occurrence_urgency);
			localStorage.setItem("currReport_status",response1.propertyMap.occurrence_status);
			localStorage.setItem("currReport_creator",response1.propertyMap.occurrence_creator);
			localStorage.setItem("currReport_id",response1.propertyMap.occurrence_id);
			localStorage.setItem("currReport_filename",response1.propertyMap.occurrence_filename);
			localStorage.setItem("currReport_creationTime",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_rating",response1.propertyMap.occurrence_rating);
			localStorage.setItem("currReport_city",response1.propertyMap.occurrence_city);

			console.log(response1.propertyMap.occurrence_creation_time.value);
			if(response1.propertyMap.occurrence_status == "Resolvido"){
				localStorage.setItem("currReport_changeTime",response1.propertyMap.occurrence_change_time.value);
			}

			window.location.href = "/occurrence.html";
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}

see9MoreMap = function(event){	
	console.log("lelelelelelelelellelelelelellel");
	var r = responseGlobal;
	if(whichType == 1){
		r = responseByStatus;
	}else if(whichType == 2){
		r = responseByCategory;
	}else if(whichType == 3){
		r = responseByMine;
	}else if(whichType == 4){
		r = responseBySolving;
	}

	//var currPage = localStorage.getItem("currPage");
	//var t = (r.length / 9) - 1;

	var currPage = localStorage.getItem("currPage");


	if(whichType == 1){
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrencesStatus/"+ encodeURIComponent(currStatus) +"/"+currentCursor,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					window.location.href = "#";
					currPage++;
					localStorage.setItem("currPage",currPage);
					var start = currPage * 9;
					var end = start + 9;
					var j = 0;
					console.log("stuff "+r.length);
					console.log("starttt "+start);
					console.log("endddd "+end);
					var r1 = [];

					console.log(response.cursor);
					currentCursor = response.cursor;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobal = responseGlobal.concat(response.results);
					responseByStatus = responseByStatus.concat(response.results);
					console.log("status length: "+responseByStatus.length);

					/*for (i = start; i < end; i++) { 
						if (i < responseByStatus.length){


							var address = responseByStatus[i].propertyMap.occurrence_location;
							var type = responseByStatus[i].propertyMap.occurrence_type;
							var description = responseByStatus[i].propertyMap.occurrence_description;
							var datei = responseByStatus[i].propertyMap.occurrence_creation_time.value;
							var likes = responseByStatus[i].propertyMap.occurrence_likes;
							var urgency = responseByStatus[i].propertyMap.occurrence_urgency;
							var status = responseByStatus[i].propertyMap.occurrence_status;
							var creator = responseByStatus[i].propertyMap.occurrence_creator;
							var occid = responseByStatus[i].propertyMap.occurrence_id;
							var filename = responseByStatus[i].propertyMap.occurrence_filename;
							console.log(filename);
							if (filename != ""){
							}else{
							}

							reportedTotal++;
							if(status == "Aberto"){
							}else{
								resolvedTotal++;
							}
							r1.push(responseByStatus[i]);
						}
						j++;
					}
					responseCurrPage = r1;
					localStorage.setItem("resolvedTotal",resolvedTotal);
					localStorage.setItem("reportedTotal",reportedTotal);*/

					var i;
					infowindow = new google.maps.InfoWindow({
						content: "a carregar..."
					});
					var marker;
					var kk = start;
					for(i = start; i<end;i++){
						if (i < responseByStatus.length){

							var address = responseByStatus[kk].propertyMap.occurrence_location;
							var type = responseByStatus[kk].propertyMap.occurrence_type;
							var description = responseByStatus[kk].propertyMap.occurrence_description;
							var datei = responseByStatus[kk].propertyMap.occurrence_creation_time.value;
							var likes = responseByStatus[kk].propertyMap.occurrence_likes;
							var urgency = responseByStatus[kk].propertyMap.occurrence_urgency;
							var status = responseByStatus[kk].propertyMap.occurrence_status;
							var creator = responseByStatus[kk].propertyMap.occurrence_creator;
							var occid = responseByStatus[kk].propertyMap.occurrence_id;
							var filename = responseByStatus[kk].propertyMap.occurrence_filename;
							var anonim = responseByStatus[kk].propertyMap.occurrence_anonim;


							var jj = start;
							geocoder.geocode( { 'address': responseByStatus[i].propertyMap.occurrence_location}, function(results, status) {
								jj++;

								if (status == 'OK') {
									var content = "test ssss "+ i + " " + type +" "+ description;
									var ic = {
											url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
											scaledSize: new google.maps.Size(50, 50), // scaled size
											origin: new google.maps.Point(0,0)
									};
									if (responseByStatus[kk].propertyMap.occurrence_status == "Resolvido"){
										var ic = {
												url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
												scaledSize: new google.maps.Size(50, 50), // scaled size
												origin: new google.maps.Point(0,0)
										};
									}
									marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
									kk++;
									markers.push(marker);
									console.log("i "+jj);
									console.log("rlength "+r.length);
									var hm = responseByStatus.length-1;
									console.log(hm);
									if(jj==hm){
										infoPopPliz();
									}
								} else {
								}
							});
						}


					}
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}else if(whichType == 3){
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrencesMinhas/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+currentCursor,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					window.location.href = "#";
					document.getElementById("00").style.display = "none";
					document.getElementById("10").style.display = "none";
					document.getElementById("20").style.display = "none";
					document.getElementById("30").style.display = "none";
					document.getElementById("40").style.display = "none";
					document.getElementById("50").style.display = "none";
					document.getElementById("60").style.display = "none";
					document.getElementById("70").style.display = "none";
					document.getElementById("80").style.display = "none";
					currPage++;
					localStorage.setItem("currPage",currPage);
					var start = currPage * 9;
					var end = start + 9;
					var j = 0;
					console.log("stuff "+r.length);
					console.log("starttt "+start);
					console.log("endddd "+end);
					var r1 = [];

					console.log(response.cursor);
					currentCursor = response.cursor;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobal = responseGlobal.concat(response.results);
					responseByMine = responseByMine.concat(response.results);
					console.log("status length: "+responseByMine.length);
					/*
					for (i = start; i < end; i++) { 
						if (i < responseByMine.length){

							var address = responseByMine[i].propertyMap.occurrence_location;
							var type = responseByMine[i].propertyMap.occurrence_type;
							var description = responseByMine[i].propertyMap.occurrence_description;
							var datei = responseByMine[i].propertyMap.occurrence_creation_time.value;
							var likes = responseByMine[i].propertyMap.occurrence_likes;
							var urgency = responseByMine[i].propertyMap.occurrence_urgency;
							var status = responseByMine[i].propertyMap.occurrence_status;
							var creator = responseByMine[i].propertyMap.occurrence_creator;
							var occid = responseByMine[i].propertyMap.occurrence_id;
							var filename = responseByMine[i].propertyMap.occurrence_filename;
							console.log(filename);
							if (filename != ""){
							}else{
							}
							reportedTotal++;
							if(status == "Aberto"){
							}else{
								resolvedTotal++;
							}
							r1.push(responseByMine[i]);
						}
						j++;
					}
					responseCurrPage = r1;
					localStorage.setItem("resolvedTotal",resolvedTotal);
					localStorage.setItem("reportedTotal",reportedTotal);*/
					var i;
					infowindow = new google.maps.InfoWindow({
						content: "a carregar..."
					});
					var marker;
					var kk = start;
					for(i = start; i<end;i++){
						if (i < responseByMine.length){

							var address = responseByMine[kk].propertyMap.occurrence_location;
							var type = responseByMine[kk].propertyMap.occurrence_type;
							var description = responseByMine[kk].propertyMap.occurrence_description;
							var datei = responseByMine[kk].propertyMap.occurrence_creation_time.value;
							var likes = responseByMine[kk].propertyMap.occurrence_likes;
							var urgency = responseByMine[kk].propertyMap.occurrence_urgency;
							var status = responseByMine[kk].propertyMap.occurrence_status;
							var creator = responseByMine[kk].propertyMap.occurrence_creator;
							var occid = responseByMine[kk].propertyMap.occurrence_id;
							var filename = responseByMine[kk].propertyMap.occurrence_filename;
							var anonim = responseByMine[kk].propertyMap.occurrence_anonim;


							var jj = start;
							geocoder.geocode( { 'address': responseByMine[i].propertyMap.occurrence_location}, function(results, status) {
								jj++;
								if (status == 'OK') {
									var content = "test ssss "+ i + " " + type +" "+ description;
									var ic = {
											url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
											scaledSize: new google.maps.Size(50, 50), // scaled size
											origin: new google.maps.Point(0,0) // origin
									};
									if (responseByMine[kk].propertyMap.occurrence_status == "Resolvido"){
										var ic = {
												url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
												scaledSize: new google.maps.Size(50, 50), // scaled size
												origin: new google.maps.Point(0,0) // origin
										};
									}
									marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
									kk++;
									markers.push(marker);
									console.log("i "+jj);
									console.log("rlength "+r.length);
									var hm = responseByMine.length-1;
									console.log(hm);
									if(jj==hm){
										infoPopPliz();
									}
								} else {
								}
							});
						}


					}
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}else if(whichType == 2){
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrencesCategory/"+ encodeURIComponent(currCategory) +"/"+ currentCursor,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					window.location.href = "#";

					currPage++;
					localStorage.setItem("currPage",currPage);
					var start = currPage * 9;
					var end = start + 9;
					var j = 0;
					console.log("stuff "+r.length);
					console.log("starttt "+start);
					console.log("endddd "+end);
					var r1 = [];

					console.log(response.cursor);
					currentCursor = response.cursor;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobal = responseGlobal.concat(response.results);
					responseByCategory = responseByCategory.concat(response.results);
					console.log("category length: "+responseByCategory.length);
					/*
					for (i = start; i < end; i++) { 
						if (i < responseByCategory.length){

							var address = responseByCategory[i].propertyMap.occurrence_location;
							var type = responseByCategory[i].propertyMap.occurrence_type;
							var description = responseByCategory[i].propertyMap.occurrence_description;
							var datei = responseByCategory[i].propertyMap.occurrence_creation_time.value;
							var likes = responseByCategory[i].propertyMap.occurrence_likes;
							var urgency = responseByCategory[i].propertyMap.occurrence_urgency;
							var status = responseByCategory[i].propertyMap.occurrence_status;
							var creator = responseByCategory[i].propertyMap.occurrence_creator;
							var occid = responseByCategory[i].propertyMap.occurrence_id;
							var filename = responseByCategory[i].propertyMap.occurrence_filename;
							console.log(filename);
							if (filename != ""){
							}else{
							}
							reportedTotal++;
							if(status == "Aberto"){
							}else{
								resolvedTotal++;
							}
							r1.push(responseByCategory[i]);
						}
						j++;
					}
					responseCurrPage = r1;
					localStorage.setItem("resolvedTotal",resolvedTotal);
					localStorage.setItem("reportedTotal",reportedTotal);*/
					var i;
					infowindow = new google.maps.InfoWindow({
						content: "a carregar..."
					});
					var marker;
					var kk = start;
					for(i = start; i<end;i++){
						if (i < responseByCategory.length){

							var address = responseByCategory[kk].propertyMap.occurrence_location;
							var type = responseByCategory[kk].propertyMap.occurrence_type;
							var description = responseByCategory[kk].propertyMap.occurrence_description;
							var datei = responseByCategory[kk].propertyMap.occurrence_creation_time.value;
							var likes = responseByCategory[kk].propertyMap.occurrence_likes;
							var urgency = responseByCategory[kk].propertyMap.occurrence_urgency;
							var status = responseByCategory[kk].propertyMap.occurrence_status;
							var creator = responseByCategory[kk].propertyMap.occurrence_creator;
							var occid = responseByCategory[kk].propertyMap.occurrence_id;
							var filename = responseByCategory[kk].propertyMap.occurrence_filename;
							var anonim = responseByCategory[kk].propertyMap.occurrence_anonim;


							var jj = start;
							geocoder.geocode( { 'address': responseByCategory[i].propertyMap.occurrence_location}, function(results, status) {
								jj++;
								if (status == 'OK') {
									var content = "test ssss "+ i + " " + type +" "+ description;
									var ic = {
											url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
											scaledSize: new google.maps.Size(50, 50), // scaled size
											origin: new google.maps.Point(0,0) // origin
									};
									if (responseByCategory[kk].propertyMap.occurrence_status == "Resolvido"){
										var ic = {
												url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
												scaledSize: new google.maps.Size(50, 50), // scaled size
												origin: new google.maps.Point(0,0) // origin
										};
									}
									marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
									kk++;
									markers.push(marker);
									console.log("i "+jj);
									console.log("rlength "+r.length);
									var hm = responseByCategory.length-1;
									console.log(hm);
									if(jj==hm){
										infoPopPliz();
									}
								} else {
								}
							});
						}


					}
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}else if(whichType == 4){
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrencesSolving/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+ currentCursor,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					window.location.href = "#";

					currPage++;
					localStorage.setItem("currPage",currPage);
					var start = currPage * 9;
					var end = start + 9;
					var j = 0;
					console.log("stuff "+r.length);
					console.log("starttt "+start);
					console.log("endddd "+end);
					var r1 = [];

					console.log(response.cursor);
					currentCursor = response.cursor;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobal = responseGlobal.concat(response.results);
					responseBySolving = responseBySolving.concat(response.results);
					console.log("category length: "+responseBySolving.length);
					/*
					for (i = start; i < end; i++) { 
						if (i < responseBySolving.length){


							var address = responseBySolving[i].propertyMap.occurrence_location;
							var type = responseBySolving[i].propertyMap.occurrence_type;
							var description = responseBySolving[i].propertyMap.occurrence_description;
							var datei = responseBySolving[i].propertyMap.occurrence_creation_time.value;
							var likes = responseBySolving[i].propertyMap.occurrence_likes;
							var urgency = responseBySolving[i].propertyMap.occurrence_urgency;
							var status = responseBySolving[i].propertyMap.occurrence_status;
							var creator = responseBySolving[i].propertyMap.occurrence_creator;
							var occid = responseBySolving[i].propertyMap.occurrence_id;
							var filename = responseBySolving[i].propertyMap.occurrence_filename;
							console.log(filename);
							if (filename != ""){
							}else{
							}
							reportedTotal++;
							if(status == "Aberto"){
							}else{
								resolvedTotal++;
							}
							r1.push(responseBySolving[i]);
						}
						j++;
					}
					responseCurrPage = r1;
					localStorage.setItem("resolvedTotal",resolvedTotal);
					localStorage.setItem("reportedTotal",reportedTotal);*/
					var i;
					infowindow = new google.maps.InfoWindow({
						content: "a carregar..."
					});
					var marker;
					var kk = start;
					for(i = start; i<end;i++){
						if (i < responseBySolving.length){

							var address = responseBySolving[kk].propertyMap.occurrence_location;
							var type = responseBySolving[kk].propertyMap.occurrence_type;
							var description = responseBySolving[kk].propertyMap.occurrence_description;
							var datei = responseBySolving[kk].propertyMap.occurrence_creation_time.value;
							var likes = responseBySolving[kk].propertyMap.occurrence_likes;
							var urgency = responseBySolving[kk].propertyMap.occurrence_urgency;
							var status = responseBySolving[kk].propertyMap.occurrence_status;
							var creator = responseBySolving[kk].propertyMap.occurrence_creator;
							var occid = responseBySolving[kk].propertyMap.occurrence_id;
							var filename = responseBySolving[kk].propertyMap.occurrence_filename;
							var anonim = responseBySolving[kk].propertyMap.occurrence_anonim;


							var jj = start;
							geocoder.geocode( { 'address': responseBySolving[i].propertyMap.occurrence_location}, function(results, status) {
								jj++;
								if (status == 'OK') {
									var content = "test ssss "+ i + " " + type +" "+ description;
									var ic = {
											url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
											scaledSize: new google.maps.Size(50, 50), // scaled size
											origin: new google.maps.Point(0,0) // origin
									};
									if (responseBySolving[kk].propertyMap.occurrence_status == "Resolvido"){
										var ic = {
												url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
												scaledSize: new google.maps.Size(50, 50), // scaled size
												origin: new google.maps.Point(0,0) // origin
										};
									}
									marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
									kk++;
									markers.push(marker);
									console.log("i "+jj);
									console.log("rlength "+r.length);
									var hm = responseBySolving.length-1;
									console.log(hm);
									if(jj==hm){
										infoPopPliz();
									}
								} else {
								}
							});
						}


					}
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}else{
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrences/"+ currentCursor,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					window.location.href = "#";

					currPage++;
					localStorage.setItem("currPage",currPage);
					var start = currPage * 9;
					var end = start + 9;
					var j = 0;
					console.log("stuff "+r.length);
					console.log("starttt "+start);
					console.log("endddd "+end);
					var r1 = [];

					console.log(response.cursor);
					currentCursor = response.cursor;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobal = responseGlobal.concat(response.results);
					/*
					for (i = start; i < end; i++) { 
						if (i < responseGlobal.length){

							var address = responseGlobal[i].propertyMap.occurrence_location;
							var type = responseGlobal[i].propertyMap.occurrence_type;
							var description = responseGlobal[i].propertyMap.occurrence_description;
							var datei = responseGlobal[i].propertyMap.occurrence_creation_time.value;
							var likes = responseGlobal[i].propertyMap.occurrence_likes;
							var urgency = responseGlobal[i].propertyMap.occurrence_urgency;
							var status = responseGlobal[i].propertyMap.occurrence_status;
							var creator = responseGlobal[i].propertyMap.occurrence_creator;
							var occid = responseGlobal[i].propertyMap.occurrence_id;
							var filename = responseGlobal[i].propertyMap.occurrence_filename;
							console.log(filename);
							if (filename != ""){
							}else{
							}
							reportedTotal++;
							if(status == "Aberto"){
							}else{
								resolvedTotal++;
							}
							r1.push(responseGlobal[i]);

							var ic = {
									url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
									scaledSize: new google.maps.Size(50, 50), // scaled size
									origin: new google.maps.Point(0,0), // origin
									anchor: new google.maps.Point(0, 0) // anchor
							};
							if (responseGlobal[i].propertyMap.occurrence_status == "Resolvido"){
								var ic = {
										url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
										scaledSize: new google.maps.Size(50, 50), // scaled size
										origin: new google.maps.Point(0,0), // origin
										anchor: new google.maps.Point(0, 0) // anchor
								};
							}
							geocoder.geocode( { 'address': responseGlobal[i].propertyMap.occurrence_location}, function(results, status) {
								if (status == 'OK') {
									var marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
								} else {
								}
							});

						}
						j++;
					}
					responseCurrPage = r1;
					localStorage.setItem("resolvedTotal",resolvedTotal);
					localStorage.setItem("reportedTotal",reportedTotal);*/

					var i;
					infowindow = new google.maps.InfoWindow({
						content: "a carregar..."
					});
					var marker;
					var kk = start;
					for(i = start; i<end;i++){
						if (i < responseGlobal.length){

							var address = responseGlobal[kk].propertyMap.occurrence_location;
							var type = responseGlobal[kk].propertyMap.occurrence_type;
							var description = responseGlobal[kk].propertyMap.occurrence_description;
							var datei = responseGlobal[kk].propertyMap.occurrence_creation_time.value;
							var likes = responseGlobal[kk].propertyMap.occurrence_likes;
							var urgency = responseGlobal[kk].propertyMap.occurrence_urgency;
							var status = responseGlobal[kk].propertyMap.occurrence_status;
							var creator = responseGlobal[kk].propertyMap.occurrence_creator;
							var occid = responseGlobal[kk].propertyMap.occurrence_id;
							var filename = responseGlobal[kk].propertyMap.occurrence_filename;
							var anonim = responseGlobal[kk].propertyMap.occurrence_anonim;


							var jj = start;
							console.log("outside YES");
							geocoder.geocode( { 'address': responseGlobal[kk].propertyMap.occurrence_location}, function(results, status) {
								jj++;
								console.log("inside OKKK");
								if (status == 'OK') {
									var content = "test ssss "+ i + " " + type +" "+ description;
									var ic = {
											url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/opened.png', // url
											scaledSize: new google.maps.Size(50, 50), // scaled size
											origin: new google.maps.Point(0,0) // origin
									};
									if (responseGlobal[kk].propertyMap.occurrence_status == "Resolvido"){
										var ic = {
												url: 'https://storage.googleapis.com/fit-sanctum-159416.appspot.com/markers/closed.png', // url
												scaledSize: new google.maps.Size(50, 50), // scaled size
												origin: new google.maps.Point(0,0) // origin
										};
									}
									marker = new google.maps.Marker({
										map: map,
										icon: ic,
										animation: google.maps.Animation.DROP,
										position: results[0].geometry.location
									});
									kk++;
									markers.push(marker);
									console.log("i "+jj);
									console.log("rlength "+r.length);
									console.log("globallength "+responseGlobal.length-1);

									console.log("end "+end);

									var hm = responseGlobal.length-1;
									console.log(hm);
									if(jj==hm){
										infoPopPliz();
									}
								} else {
								}
							});
						}


					}
				}

			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}


}

var map;
var geocoder;
function showMapInit() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(38.710128, -9.37526);
	var myOptions = {
			zoom: 12,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var centerControlDiv = document.createElement('div');
	var centerControl = new CenterControl(centerControlDiv, map);

	centerControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
}



logout = function(event) {
	var token = localStorage.getItem('tokenID');
	var tokenname = localStorage.getItem('tokenIDname');
	var tokenexp = localStorage.getItem('tokenIDexpir');
	var tokencre = localStorage.getItem('tokenIDcreate');
	var data = {};
	data.tokenID = token;
	data.username = tokenname;
	data.expirationData = tokenexp;
	data.creationData = tokencre;
	console.log(data);

	console.log(JSON.stringify(data));
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/logout/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
			localStorage.setItem('userLocation', "");
			localStorage.setItem('userType', "");
			localStorage.setItem('userEmail', "");
			localStorage.setItem('userName', "");
			localStorage.setItem('userLikes', "");

			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
	signOut();

}


captureData = function(event) {

	console.log("ekfmkrnkn");
	console.log($('form[name="login"]'));
	console.log($('form[name="login"]').jsonify());
	var data = $('form[name="login"]').jsonify();

	var checked = document.getElementById("check").checked;
	data.checked = checked;
	console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			// Store token id for later use in localStorage
			console.log(response.username);
			console.log(response.tokenID);
			console.log(response.creationData);
			console.log(response.expirationData);

			localStorage.setItem('tokenIDname', response.username);
			localStorage.setItem('tokenID', response.tokenID);
			localStorage.setItem('tokenIDcreate', response.creationData);
			localStorage.setItem('tokenIDexpir', response.expirationData);

			window.location.href = "/occurrences.html";

		},
		error: function(response) {
			if(response.status == 333){
				alert("A sua conta ainda não foi activada. Por favor vá à sua caixa de correio e confirme o seu email.");
			}else{
				alert("O email/password que inseriu está incorreto.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function onFailure(error) {
	alert(error);
}
function renderButton() {
	gapi.signin2.render('gSignIn', {
		'scope': 'profile email',
		'width': 290,
		'height': 50,
		'longtitle': true,
		'theme': 'dark',
		'onsuccess': onSuccess,
		'onfailure': onFailure
	});
}
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		$('.userContent').html('');
		$('#gSignIn').slideDown('slow');
	});
}

function onSuccess(googleUser) {

	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	var checked = document.getElementById("check").checked;
	var data = {};
	data.username = profile.getEmail();
	data.password = profile.getId();
	data.checked = checked;
	console.log(JSON.stringify(data));
	if(localStorage.getItem('tokenID') == ""){

		$.ajax({
			type: "POST",
			url: "/rest/login/logingoogle/" + profile.getEmail(),
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				// Store token id for later use in localStorage
				console.log(response.username);
				console.log(response.tokenID);
				console.log(response.creationData);
				console.log(response.expirationData);

				localStorage.setItem('tokenIDname', response.username);
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenIDcreate', response.creationData);
				localStorage.setItem('tokenIDexpir', response.expirationData);

				window.location.href = "/occurrences.html";

			},
			error: function(response) {
				//alert("Error: "+ response.status);
			},
			data: JSON.stringify(data)
		});
	}
}

captureDataReg = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Enviámos um mail para o endereço que registou. Por favor confirme para poder entrar.");

			window.location.href = "/";
		},
		error: function(response) {
			if(response.status == 111){
				alert("Registo inválido. Por favor verifique todos os campos.");
			}else if(response.status == 222){
				alert("Email inválido. Os emails têm de conter '@'.");
			}else if(response.status == 333){
				alert("Password inválida. As passwords têm de ter 6 caracteres ou mais, e conter números e letras.");
			}else if(response.status == 444){
				alert("A password que inseriu não coincide com a confirmação da password. Tente de novo.");
			}else if(response.status == 400){
				alert("Já existe um utilizador registado com o email que inseriu.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

var whichType = 0; //all is 0, status is 1, category is 2

prevPage = function(event) {
	var r = responseGlobal;
	if(whichType == 1){
		r = responseByStatus;
	}else if(whichType == 2){
		r = responseByCategory;
	}else if(whichType == 3){
		r = responseByMine;
	}else if(whichType == 4){
		r = responseBySolving;
	}

	var currPage = localStorage.getItem("currPage");
	if (currPage > 0){
		window.location.href = "#";

		document.getElementById("00").style.display = "none";
		document.getElementById("10").style.display = "none";
		document.getElementById("20").style.display = "none";
		document.getElementById("30").style.display = "none";
		document.getElementById("40").style.display = "none";
		document.getElementById("50").style.display = "none";
		document.getElementById("60").style.display = "none";
		document.getElementById("70").style.display = "none";
		document.getElementById("80").style.display = "none";
		currPage--;
		localStorage.setItem("currPage",currPage);
		var start = currPage *9;
		var end = start + 9;
		var j = 0;
		var r1 = [];
		for (i = start; i < end; i++) { 
			if (i < responseGlobal.length){

				document.getElementById(j+'0').style.display = "block";
				document.getElementById(j).style.display = "block";

				var address = r[i].propertyMap.occurrence_location;
				var type = r[i].propertyMap.occurrence_type;
				var description = r[i].propertyMap.occurrence_description;
				var datei = r[i].propertyMap.occurrence_creation_time.value;
				var likes = r[i].propertyMap.occurrence_likes;
				var urgency = r[i].propertyMap.occurrence_urgency;
				var status = r[i].propertyMap.occurrence_status;
				var creator = r[i].propertyMap.occurrence_creator;
				var occid = r[i].propertyMap.occurrence_id;
				var anonim = r[i].propertyMap.occurrence_anonim;

				if(contains(userLikes,occid)){
					document.getElementById(j+"likeme").style.color = "#66ccdb";
					document.getElementById(j+"likeme").value = "liked";
				}else{
					document.getElementById(j+"likeme").value = "not liked";

				}
				var filename = r[i].propertyMap.occurrence_filename;
				console.log(filename);
				if (filename != ""){
					document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
				}else{
					document.getElementById(j+"img").src = "img/post-img1.jpg";
				}
				document.getElementById(j+"p").innerHTML = description;
				document.getElementById(j+"hr").innerHTML = type + " em " + address;
				document.getElementById(j+"likes").innerHTML = likes;
				document.getElementById(j+"date").innerHTML = datei;
				if(anonim){
					document.getElementById(j+"user").innerHTML = " | por Anónimo";
				}else{
					document.getElementById(j+"user").innerHTML = " | por "+creator;
				}
				if(status == "Aberto"){
					document.getElementById(j+"status").className = "label label-danger";
				}else{
					document.getElementById(j+"status").className = "label label-success";
				}

				document.getElementById(j+"status").innerHTML = status ;
				r1.push(r[i]);
			}
			j++;
		}
	}
	responseCurrPage = r1;

}


nextPage = function(event) {
	var r = responseGlobal;
	if(whichType == 1){
		r = responseByStatus;
	}else if(whichType == 2){
		r = responseByCategory;
	}else if(whichType == 3){
		r = responseByMine;
	}else if(whichType == 4){
		r = responseBySolving;
	}

	var currPage = localStorage.getItem("currPage");
	var t = (r.length / 9) - 1;


	if (currPage < t){
		window.location.href = "#";

		document.getElementById("00").style.display = "none";
		document.getElementById("10").style.display = "none";
		document.getElementById("20").style.display = "none";
		document.getElementById("30").style.display = "none";
		document.getElementById("40").style.display = "none";
		document.getElementById("50").style.display = "none";
		document.getElementById("60").style.display = "none";
		document.getElementById("70").style.display = "none";
		document.getElementById("80").style.display = "none";
		currPage++;
		localStorage.setItem("currPage",currPage);
		var start = currPage * 9;
		var end = start + 9;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		for (i = start; i < end; i++) { 
			if (i < r.length){

				document.getElementById(j+'0').style.display = "block";
				document.getElementById(j).style.display = "block";

				var address = r[i].propertyMap.occurrence_location;
				var type = r[i].propertyMap.occurrence_type;
				var description = r[i].propertyMap.occurrence_description;
				var datei = r[i].propertyMap.occurrence_creation_time.value;
				var likes = r[i].propertyMap.occurrence_likes;
				var urgency = r[i].propertyMap.occurrence_urgency;
				var status = r[i].propertyMap.occurrence_status;
				var creator = r[i].propertyMap.occurrence_creator;
				var occid = r[i].propertyMap.occurrence_id;
				var anonim = r[i].propertyMap.occurrence_anonim;

				if(contains(userLikes,occid)){
					document.getElementById(j+"likeme").style.color = "#66ccdb";
					document.getElementById(j+"likeme").value = "liked";
				}else{
					document.getElementById(j+"likeme").value = "not liked";
				}
				var filename = r[i].propertyMap.occurrence_filename;
				console.log(filename);
				if (filename != ""){
					document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
				}else{
					document.getElementById(j+"img").src = "img/post-img1.jpg";
				}
				document.getElementById(j+"p").innerHTML = description;
				document.getElementById(j+"hr").innerHTML = type + " em " + address;
				document.getElementById(j+"likes").innerHTML = likes;
				document.getElementById(j+"date").innerHTML = datei;
				if(anonim){
					document.getElementById(j+"user").innerHTML = " | por Anónimo";
				}else{
					document.getElementById(j+"user").innerHTML = " | por "+creator;
				}
				if(status == "Aberto"){
					document.getElementById(j+"status").className = "label label-danger";
				}else{
					document.getElementById(j+"status").className = "label label-success";
				}

				document.getElementById(j+"status").innerHTML = status ;
				r1.push(r[i]);
			}
			j++;
		}
	}else{

		if(whichType == 1){
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getAllOccurrencesStatus/"+ encodeURIComponent(currStatus) +"/"+currentCursor,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response){ 

					if(response.results.length != 0){
						window.location.href = "#";

						document.getElementById("00").style.display = "none";
						document.getElementById("10").style.display = "none";
						document.getElementById("20").style.display = "none";
						document.getElementById("30").style.display = "none";
						document.getElementById("40").style.display = "none";
						document.getElementById("50").style.display = "none";
						document.getElementById("60").style.display = "none";
						document.getElementById("70").style.display = "none";
						document.getElementById("80").style.display = "none";
						currPage++;
						localStorage.setItem("currPage",currPage);
						var start = currPage * 9;
						var end = start + 9;
						var j = 0;
						console.log("stuff "+r.length);
						console.log("starttt "+start);
						console.log("endddd "+end);
						var r1 = [];

						console.log(response.cursor);
						currentCursor = response.cursor;
						console.log(response.results.length);
						console.log("martaaaa");
						responseGlobal = responseGlobal.concat(response.results);
						responseByStatus = responseByStatus.concat(response.results);
						console.log("status length: "+responseByStatus.length);

						for (i = start; i < end; i++) { 
							if (i < responseByStatus.length){

								document.getElementById(j+'0').style.display = "block";
								document.getElementById(j).style.display = "block";

								var address = responseByStatus[i].propertyMap.occurrence_location;
								var type = responseByStatus[i].propertyMap.occurrence_type;
								var description = responseByStatus[i].propertyMap.occurrence_description;
								var datei = responseByStatus[i].propertyMap.occurrence_creation_time.value;
								var likes = responseByStatus[i].propertyMap.occurrence_likes;
								var urgency = responseByStatus[i].propertyMap.occurrence_urgency;
								var status = responseByStatus[i].propertyMap.occurrence_status;
								var creator = responseByStatus[i].propertyMap.occurrence_creator;
								var occid = responseByStatus[i].propertyMap.occurrence_id;
								var filename = responseByStatus[i].propertyMap.occurrence_filename;
								var anonim = responseByStatus[i].propertyMap.occurrence_anonim;

								console.log(filename);
								if (filename != ""){
									document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
								}else{
									document.getElementById(j+"img").src = "img/post-img1.jpg";
								}
								document.getElementById(j+"p").innerHTML = description;
								document.getElementById(j+"hr").innerHTML = type + " em " + address;


								document.getElementById(j+"likes").innerHTML = likes;
								document.getElementById(j+"date").innerHTML = datei;

								if(anonim){
									document.getElementById(j+"user").innerHTML = " | por Anónimo";
								}else{
									document.getElementById(j+"user").innerHTML = " | por "+creator;
								}
								//document.getElementById(i+"img").src = "img/post-img1.jpg";
								document.getElementById(j+"status").className = "label label-success";
								reportedTotal++;
								if(status == "Aberto"){
									document.getElementById(j+"status").className = "label label-danger";
								}else{
									resolvedTotal++;
									document.getElementById(j+"status").className = "label label-success";
								}
								document.getElementById(j+"status").innerHTML = status ;
								r1.push(responseByStatus[i]);
							}
							j++;
						}
						responseCurrPage = r1;
						localStorage.setItem("resolvedTotal",resolvedTotal);
						localStorage.setItem("reportedTotal",reportedTotal);
					}
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		}else if(whichType == 3){
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getAllOccurrencesMinhas/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+currentCursor,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response){ 

					if(response.results.length != 0){
						window.location.href = "#";
						document.getElementById("00").style.display = "none";
						document.getElementById("10").style.display = "none";
						document.getElementById("20").style.display = "none";
						document.getElementById("30").style.display = "none";
						document.getElementById("40").style.display = "none";
						document.getElementById("50").style.display = "none";
						document.getElementById("60").style.display = "none";
						document.getElementById("70").style.display = "none";
						document.getElementById("80").style.display = "none";
						currPage++;
						localStorage.setItem("currPage",currPage);
						var start = currPage * 9;
						var end = start + 9;
						var j = 0;
						console.log("stuff "+r.length);
						console.log("starttt "+start);
						console.log("endddd "+end);
						var r1 = [];

						console.log(response.cursor);
						currentCursor = response.cursor;
						console.log(response.results.length);
						console.log("martaaaa");
						responseGlobal = responseGlobal.concat(response.results);
						responseByMine = responseByMine.concat(response.results);
						console.log("status length: "+responseByMine.length);

						for (i = start; i < end; i++) { 
							if (i < responseByMine.length){

								document.getElementById(j+'0').style.display = "block";
								document.getElementById(j).style.display = "block";

								var address = responseByMine[i].propertyMap.occurrence_location;
								var type = responseByMine[i].propertyMap.occurrence_type;
								var description = responseByMine[i].propertyMap.occurrence_description;
								var datei = responseByMine[i].propertyMap.occurrence_creation_time.value;
								var likes = responseByMine[i].propertyMap.occurrence_likes;
								var urgency = responseByMine[i].propertyMap.occurrence_urgency;
								var status = responseByMine[i].propertyMap.occurrence_status;
								var creator = responseByMine[i].propertyMap.occurrence_creator;
								var occid = responseByMine[i].propertyMap.occurrence_id;
								var filename = responseByMine[i].propertyMap.occurrence_filename;
								var anonim = responseByMine[i].propertyMap.occurrence_anonim;

								console.log(filename);
								if (filename != ""){
									document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
								}else{
									document.getElementById(j+"img").src = "img/post-img1.jpg";
								}
								document.getElementById(j+"p").innerHTML = description;
								document.getElementById(j+"hr").innerHTML = type + " em " + address;


								document.getElementById(j+"likes").innerHTML = likes;
								document.getElementById(j+"date").innerHTML = datei;

								if(anonim){
									document.getElementById(j+"user").innerHTML = " | por Anónimo";
								}else{
									document.getElementById(j+"user").innerHTML = " | por "+creator;
								}
								//document.getElementById(i+"img").src = "img/post-img1.jpg";
								document.getElementById(j+"status").className = "label label-success";
								reportedTotal++;
								if(status == "Aberto"){
									document.getElementById(j+"status").className = "label label-danger";
								}else{
									resolvedTotal++;
									document.getElementById(j+"status").className = "label label-success";
								}
								document.getElementById(j+"status").innerHTML = status ;
								r1.push(responseByMine[i]);
							}
							j++;
						}
						responseCurrPage = r1;
						localStorage.setItem("resolvedTotal",resolvedTotal);
						localStorage.setItem("reportedTotal",reportedTotal);
					}
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		}else if(whichType == 2){
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getAllOccurrencesCategory/"+ encodeURIComponent(currCategory) +"/"+ currentCursor,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response){ 

					if(response.results.length != 0){
						window.location.href = "#";

						document.getElementById("00").style.display = "none";
						document.getElementById("10").style.display = "none";
						document.getElementById("20").style.display = "none";
						document.getElementById("30").style.display = "none";
						document.getElementById("40").style.display = "none";
						document.getElementById("50").style.display = "none";
						document.getElementById("60").style.display = "none";
						document.getElementById("70").style.display = "none";
						document.getElementById("80").style.display = "none";
						currPage++;
						localStorage.setItem("currPage",currPage);
						var start = currPage * 9;
						var end = start + 9;
						var j = 0;
						console.log("stuff "+r.length);
						console.log("starttt "+start);
						console.log("endddd "+end);
						var r1 = [];

						console.log(response.cursor);
						currentCursor = response.cursor;
						console.log(response.results.length);
						console.log("martaaaa");
						responseGlobal = responseGlobal.concat(response.results);
						responseByCategory = responseByCategory.concat(response.results);
						console.log("category length: "+responseByCategory.length);

						for (i = start; i < end; i++) { 
							if (i < responseByCategory.length){

								document.getElementById(j+'0').style.display = "block";
								document.getElementById(j).style.display = "block";

								var address = responseByCategory[i].propertyMap.occurrence_location;
								var type = responseByCategory[i].propertyMap.occurrence_type;
								var description = responseByCategory[i].propertyMap.occurrence_description;
								var datei = responseByCategory[i].propertyMap.occurrence_creation_time.value;
								var likes = responseByCategory[i].propertyMap.occurrence_likes;
								var urgency = responseByCategory[i].propertyMap.occurrence_urgency;
								var status = responseByCategory[i].propertyMap.occurrence_status;
								var creator = responseByCategory[i].propertyMap.occurrence_creator;
								var occid = responseByCategory[i].propertyMap.occurrence_id;
								var filename = responseByCategory[i].propertyMap.occurrence_filename;
								var anonim = responseByCategory[i].propertyMap.occurrence_anonim;

								console.log(filename);
								if (filename != ""){
									document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
								}else{
									document.getElementById(j+"img").src = "img/post-img1.jpg";
								}
								document.getElementById(j+"p").innerHTML = description;
								document.getElementById(j+"hr").innerHTML = type + " em " + address;


								document.getElementById(j+"likes").innerHTML = likes;
								document.getElementById(j+"date").innerHTML = datei;

								if(anonim){
									document.getElementById(j+"user").innerHTML = " | por Anónimo";
								}else{
									document.getElementById(j+"user").innerHTML = " | por "+creator;

								}
								//document.getElementById(i+"img").src = "img/post-img1.jpg";
								document.getElementById(j+"status").className = "label label-success";
								reportedTotal++;
								if(status == "Aberto"){
									document.getElementById(j+"status").className = "label label-danger";
								}else{
									resolvedTotal++;
									document.getElementById(j+"status").className = "label label-success";
								}
								document.getElementById(j+"status").innerHTML = status ;
								r1.push(responseByCategory[i]);
							}
							j++;
						}
						responseCurrPage = r1;
						localStorage.setItem("resolvedTotal",resolvedTotal);
						localStorage.setItem("reportedTotal",reportedTotal);
					}
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		}else if(whichType == 4){
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getAllOccurrencesSolving/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+ currentCursor,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response){ 

					if(response.results.length != 0){
						window.location.href = "#";

						document.getElementById("00").style.display = "none";
						document.getElementById("10").style.display = "none";
						document.getElementById("20").style.display = "none";
						document.getElementById("30").style.display = "none";
						document.getElementById("40").style.display = "none";
						document.getElementById("50").style.display = "none";
						document.getElementById("60").style.display = "none";
						document.getElementById("70").style.display = "none";
						document.getElementById("80").style.display = "none";
						currPage++;
						localStorage.setItem("currPage",currPage);
						var start = currPage * 9;
						var end = start + 9;
						var j = 0;
						console.log("stuff "+r.length);
						console.log("starttt "+start);
						console.log("endddd "+end);
						var r1 = [];

						console.log(response.cursor);
						currentCursor = response.cursor;
						console.log(response.results.length);
						console.log("martaaaa");
						responseGlobal = responseGlobal.concat(response.results);
						responseBySolving = responseBySolving.concat(response.results);
						console.log("category length: "+responseBySolving.length);

						for (i = start; i < end; i++) { 
							if (i < responseBySolving.length){

								document.getElementById(j+'0').style.display = "block";
								document.getElementById(j).style.display = "block";

								var address = responseBySolving[i].propertyMap.occurrence_location;
								var type = responseBySolving[i].propertyMap.occurrence_type;
								var description = responseBySolving[i].propertyMap.occurrence_description;
								var datei = responseBySolving[i].propertyMap.occurrence_creation_time.value;
								var likes = responseBySolving[i].propertyMap.occurrence_likes;
								var urgency = responseBySolving[i].propertyMap.occurrence_urgency;
								var status = responseBySolving[i].propertyMap.occurrence_status;
								var creator = responseBySolving[i].propertyMap.occurrence_creator;
								var occid = responseBySolving[i].propertyMap.occurrence_id;
								var filename = responseBySolving[i].propertyMap.occurrence_filename;
								var anonim = responseBySolving[i].propertyMap.occurrence_anonim;

								console.log(filename);
								if (filename != ""){
									document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
								}else{
									document.getElementById(j+"img").src = "img/post-img1.jpg";
								}
								document.getElementById(j+"p").innerHTML = description;
								document.getElementById(j+"hr").innerHTML = type + " em " + address;


								document.getElementById(j+"likes").innerHTML = likes;
								document.getElementById(j+"date").innerHTML = datei;

								if(anonim){
									document.getElementById(j+"user").innerHTML = " | por Anónimo";
								}else{
									document.getElementById(j+"user").innerHTML = " | por "+creator;
								}
								//document.getElementById(i+"img").src = "img/post-img1.jpg";
								document.getElementById(j+"status").className = "label label-success";
								reportedTotal++;
								if(status == "Aberto"){
									document.getElementById(j+"status").className = "label label-danger";
								}else{
									resolvedTotal++;
									document.getElementById(j+"status").className = "label label-success";
								}
								document.getElementById(j+"status").innerHTML = status ;
								r1.push(responseBySolving[i]);
							}
							j++;
						}
						responseCurrPage = r1;
						localStorage.setItem("resolvedTotal",resolvedTotal);
						localStorage.setItem("reportedTotal",reportedTotal);
					}
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		}else{
			$.ajax({ 
				type: "GET",
				//dataType: "json",
				url: "/rest/login/getAllOccurrences/"+ currentCursor,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				success: function(response){ 

					if(response.results.length != 0){
						window.location.href = "#";

						document.getElementById("00").style.display = "none";
						document.getElementById("10").style.display = "none";
						document.getElementById("20").style.display = "none";
						document.getElementById("30").style.display = "none";
						document.getElementById("40").style.display = "none";
						document.getElementById("50").style.display = "none";
						document.getElementById("60").style.display = "none";
						document.getElementById("70").style.display = "none";
						document.getElementById("80").style.display = "none";
						currPage++;
						localStorage.setItem("currPage",currPage);
						var start = currPage * 9;
						var end = start + 9;
						var j = 0;
						console.log("stuff "+r.length);
						console.log("starttt "+start);
						console.log("endddd "+end);
						var r1 = [];

						console.log(response.cursor);
						currentCursor = response.cursor;
						console.log(response.results.length);
						console.log("martaaaa");
						responseGlobal = responseGlobal.concat(response.results);

						for (i = start; i < end; i++) { 
							if (i < responseGlobal.length){

								document.getElementById(j+'0').style.display = "block";
								document.getElementById(j).style.display = "block";

								var address = responseGlobal[i].propertyMap.occurrence_location;
								var type = responseGlobal[i].propertyMap.occurrence_type;
								var description = responseGlobal[i].propertyMap.occurrence_description;
								var datei = responseGlobal[i].propertyMap.occurrence_creation_time.value;
								var likes = responseGlobal[i].propertyMap.occurrence_likes;
								var urgency = responseGlobal[i].propertyMap.occurrence_urgency;
								var status = responseGlobal[i].propertyMap.occurrence_status;
								var creator = responseGlobal[i].propertyMap.occurrence_creator;
								var occid = responseGlobal[i].propertyMap.occurrence_id;
								var filename = responseGlobal[i].propertyMap.occurrence_filename;
								var anonim = responseGlobal[i].propertyMap.occurrence_anonim;

								console.log(filename);
								if (filename != ""){
									document.getElementById(j+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
								}else{
									document.getElementById(j+"img").src = "img/post-img1.jpg";
								}
								document.getElementById(j+"p").innerHTML = description;
								document.getElementById(j+"hr").innerHTML = type + " em " + address;


								document.getElementById(j+"likes").innerHTML = likes;
								document.getElementById(j+"date").innerHTML = datei;

								if(anonim){
									document.getElementById(j+"user").innerHTML = " | por Anónimo";
								}else{
									document.getElementById(j+"user").innerHTML = " | por "+creator;
								}
								//document.getElementById(i+"img").src = "img/post-img1.jpg";
								document.getElementById(j+"status").className = "label label-success";
								reportedTotal++;
								if(status == "Aberto"){
									document.getElementById(j+"status").className = "label label-danger";
								}else{
									resolvedTotal++;
									document.getElementById(j+"status").className = "label label-success";
								}
								document.getElementById(j+"status").innerHTML = status ;
								r1.push(responseGlobal[i]);
							}
							j++;
						}
						responseCurrPage = r1;
						localStorage.setItem("resolvedTotal",resolvedTotal);
						localStorage.setItem("reportedTotal",reportedTotal);
					}
				},
				error: function(response) {
					alert("Error: "+ response.status);
					console.log(JSON.stringify(response));
				},
			});
		}


	}

	responseCurrPage = r1;

}


var responseBySolving = [];
listSolving = function(event) {

	localStorage.setItem("currPage",0);
	currPage = 0;
	whichType = 4
	console.log("okok "+this.id);
	var r = [];

	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";

	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrencesSolving/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+"first",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){ 
			console.log(response.cursor);
			currentCursor = response.cursor;
			console.log(response.results.length);
			responseGlobal = responseGlobal.concat(response.results);
			responseBySolving = response.results;
			console.log("status length: "+responseBySolving.length);
			//var start = currPage *9;
			//var end = start + 9;
			var j = 0;
			var r1 = [];
			for (i = 0; i < 9; i++) { 
				if (i < responseBySolving.length){

					document.getElementById(j+'0').style.display = "block";
					document.getElementById(j).style.display = "block";

					var address = responseBySolving[j].propertyMap.occurrence_location;
					var type = responseBySolving[j].propertyMap.occurrence_type;
					var description = responseBySolving[j].propertyMap.occurrence_description;
					var datei = responseBySolving[j].propertyMap.occurrence_creation_time.value;
					var likes = responseBySolving[j].propertyMap.occurrence_likes;
					var urgency = responseBySolving[j].propertyMap.occurrence_urgency;
					var status = responseBySolving[j].propertyMap.occurrence_status;
					var creator = responseBySolving[j].propertyMap.occurrence_creator;
					var occid = responseBySolving[j].propertyMap.occurrence_id;
					var filename = responseBySolving[j].propertyMap.occurrence_filename;
					var anonim = responseBySolving[j].propertyMap.occurrence_anonim;

					console.log(filename);
					if (filename != ""){
						document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
					}else{
						document.getElementById(i+"img").src = "img/post-img1.jpg";
					}
					document.getElementById(i+"p").innerHTML = description;
					document.getElementById(i+"hr").innerHTML = type + " em " + address;


					document.getElementById(i+"likes").innerHTML = likes;
					document.getElementById(i+"date").innerHTML = datei;

					if(anonim){
						document.getElementById(i+"user").innerHTML = " | por Anónimo";
					}else{
						document.getElementById(i+"user").innerHTML = " | por "+creator;
					}
					//document.getElementById(i+"img").src = "img/post-img1.jpg";
					document.getElementById(i+"status").className = "label label-success";
					reportedTotal++;
					if(status == "Aberto"){
						document.getElementById(i+"status").className = "label label-danger";
					}else{
						resolvedTotal++;
						document.getElementById(i+"status").className = "label label-success";
					}
					document.getElementById(i+"status").innerHTML = status ;
					r1.push(responseBySolving[j]);
				}
				j++;
			}
			responseCurrPage = r1;
			localStorage.setItem("resolvedTotal",resolvedTotal);
			localStorage.setItem("reportedTotal",reportedTotal);
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}







var responseByMine = [];
listMine = function(event) {

	localStorage.setItem("currPage",0);
	currPage = 0;
	whichType = 3
	console.log("okok "+this.id);
	var r = [];

	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";

	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrencesMinhas/"+ encodeURIComponent(localStorage.getItem("userEmail")) +"/"+"first",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){ 
			console.log(response.cursor);
			currentCursor = response.cursor;
			console.log(response.results.length);
			responseGlobal = responseGlobal.concat(response.results);
			responseByMine = response.results;
			console.log("status length: "+responseByMine.length);
			//var start = currPage *9;
			//var end = start + 9;
			var j = 0;
			var r1 = [];
			for (i = 0; i < 9; i++) { 
				if (i < responseByMine.length){

					document.getElementById(j+'0').style.display = "block";
					document.getElementById(j).style.display = "block";

					var address = responseByMine[j].propertyMap.occurrence_location;
					var type = responseByMine[j].propertyMap.occurrence_type;
					var description = responseByMine[j].propertyMap.occurrence_description;
					var datei = responseByMine[j].propertyMap.occurrence_creation_time.value;
					var likes = responseByMine[j].propertyMap.occurrence_likes;
					var urgency = responseByMine[j].propertyMap.occurrence_urgency;
					var status = responseByMine[j].propertyMap.occurrence_status;
					var creator = responseByMine[j].propertyMap.occurrence_creator;
					var occid = responseByMine[j].propertyMap.occurrence_id;
					var filename = responseByMine[j].propertyMap.occurrence_filename;
					var anonim = responseByMine[j].propertyMap.occurrence_anonim;
					console.log(filename);
					if (filename != ""){
						document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
					}else{
						document.getElementById(i+"img").src = "img/post-img1.jpg";
					}
					document.getElementById(i+"p").innerHTML = description;
					document.getElementById(i+"hr").innerHTML = type + " em " + address;


					document.getElementById(i+"likes").innerHTML = likes;
					document.getElementById(i+"date").innerHTML = datei;

					if(anonim){
						document.getElementById(i+"user").innerHTML = " | por Anónimo";
					}else{
						document.getElementById(i+"user").innerHTML = " | por "+creator;
					}
					//document.getElementById(i+"img").src = "img/post-img1.jpg";
					document.getElementById(i+"status").className = "label label-success";
					reportedTotal++;
					if(status == "Aberto"){
						document.getElementById(i+"status").className = "label label-danger";
					}else{
						resolvedTotal++;
						document.getElementById(i+"status").className = "label label-success";
					}
					document.getElementById(i+"status").innerHTML = status ;
					r1.push(responseByMine[j]);
				}
				j++;
			}
			responseCurrPage = r1;
			localStorage.setItem("resolvedTotal",resolvedTotal);
			localStorage.setItem("reportedTotal",reportedTotal);
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}
var responseSeguindo = [];
listSeguindo = function(event) {

	localStorage.setItem("currPage",0);
	whichType = 1
	console.log("okok "+this.id);
	var r = [];
	var seguindoOcorrencia = [];
	for(i = 0; i<responseGlobal.length;i++){
		if (contains(responseGlobal[i].results.propertyMap.occurrence_following,localStorage.getItem("userEmail"))){
			r.push(responseGlobal[i]);
		}
	}
	console.log(r);
	responseSeguindo = r;
	responseCurrPage = r;

	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";
	if (r.length == 0){
		alert("Não existem ocorrências que satisfaçam os filtros que escolheu.");
	}else{

		for (i = 0; i < 9; i++) { 
			if (i < r.length){

				document.getElementById(i+'0').style.display = "block";
				document.getElementById(i).style.display = "block";

				var address = r[i].results.propertyMap.occurrence_location;
				var type = r[i].results.propertyMap.occurrence_type;
				var description = r[i].results.propertyMap.occurrence_description;
				var datei = r[i].results.propertyMap.occurrence_creation_time.value;
				var likes = r[i].results.propertyMap.occurrence_likes;
				var urgency = r[i].results.propertyMap.occurrence_urgency;
				var status = r[i].results.propertyMap.occurrence_status;
				var creator = r[i].results.propertyMap.occurrence_creator;
				var occid = r[i].results.propertyMap.occurrence_id;
				var anonim = r[i].results.propertyMap.occurrence_anonim;

				if(contains(userLikes,occid)){
					document.getElementById(i+"likeme").style.color = "#66ccdb";
					document.getElementById(i+"likeme").value = "liked";
				}else{
					document.getElementById(i+"likeme").value = "not liked";
				}

				var filename = r[i].results.propertyMap.occurrence_filename;
				console.log(filename);
				if (filename != ""){
					document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
				}else{
					document.getElementById(i+"img").src = "img/post-img1.jpg";
				}
				document.getElementById(i+"p").innerHTML = description;
				document.getElementById(i+"hr").innerHTML = type + " em " + address;
				document.getElementById(i+"likes").innerHTML = likes;
				document.getElementById(i+"date").innerHTML = datei;
				if(anonim){
					document.getElementById(i+"user").innerHTML = " | por Anónimo";
				}else{
					document.getElementById(i+"user").innerHTML = " | por "+creator;
				}
				if(status == "Aberto"){
					document.getElementById(i+"status").className = "label label-danger";
				}else{
					document.getElementById(i+"status").className = "label label-success";
				}

				document.getElementById(i+"status").innerHTML = status ;
			}
		}
	}
}



var currStatus;
var currCategory;

byStatus = function(event) {

	localStorage.setItem("currPage",0);
	currPage = 0;
	whichType = 1
	console.log("okok "+this.id);
	var r = [];

	currStatus = this.id;

	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";

	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrencesStatus/"+ encodeURIComponent(currStatus) +"/"+"first",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){ 
			console.log(response.cursor);
			currentCursor = response.cursor;
			console.log(response.results.length);
			responseGlobal = responseGlobal.concat(response.results);
			responseByStatus = response.results;
			console.log("status length: "+responseByStatus.length);
			//var start = currPage *9;
			//var end = start + 9;
			var j = 0;
			var r1 = [];
			for (i = 0; i < 9; i++) { 
				if (i < responseByStatus.length){

					document.getElementById(j+'0').style.display = "block";
					document.getElementById(j).style.display = "block";

					var address = responseByStatus[j].propertyMap.occurrence_location;
					var type = responseByStatus[j].propertyMap.occurrence_type;
					var description = responseByStatus[j].propertyMap.occurrence_description;
					var datei = responseByStatus[j].propertyMap.occurrence_creation_time.value;
					var likes = responseByStatus[j].propertyMap.occurrence_likes;
					var urgency = responseByStatus[j].propertyMap.occurrence_urgency;
					var status = responseByStatus[j].propertyMap.occurrence_status;
					var creator = responseByStatus[j].propertyMap.occurrence_creator;
					var occid = responseByStatus[j].propertyMap.occurrence_id;
					var anonim = responseByStatus[j].propertyMap.occurrence_anonim;
					var filename = responseByStatus[j].propertyMap.occurrence_filename;
					console.log(filename);
					if (filename != ""){
						document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
					}else{
						document.getElementById(i+"img").src = "img/post-img1.jpg";
					}
					document.getElementById(i+"p").innerHTML = description;
					document.getElementById(i+"hr").innerHTML = type + " em " + address;


					document.getElementById(i+"likes").innerHTML = likes;
					document.getElementById(i+"date").innerHTML = datei;

					if(anonim){
						document.getElementById(i+"user").innerHTML = " | por Anónimo";
					}else{
						document.getElementById(i+"user").innerHTML = " | por "+creator;
					}
					//document.getElementById(i+"img").src = "img/post-img1.jpg";
					document.getElementById(i+"status").className = "label label-success";
					reportedTotal++;
					if(status == "Aberto"){
						document.getElementById(i+"status").className = "label label-danger";
					}else{
						resolvedTotal++;
						document.getElementById(i+"status").className = "label label-success";
					}
					document.getElementById(i+"status").innerHTML = status ;
					r1.push(responseByStatus[j]);
				}
				j++;
			}
			responseCurrPage = r1;
			localStorage.setItem("resolvedTotal",resolvedTotal);
			localStorage.setItem("reportedTotal",reportedTotal);
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}

function contains(a, obj) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] === obj) {
			return true;
		}
	}
	return false;
}

byCategory = function(event) {
	localStorage.setItem("currPage",0);
	currPage = 0;
	whichType = 2
	console.log("okocmckk "+this.id);
	var cat = [];

	currCategory = this.id;
	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";

	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrencesCategory/"+ encodeURIComponent(currCategory) +"/"+"first",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){ 
			console.log(response.cursor);
			currentCursor = response.cursor;
			console.log(response.results.length);
			responseGlobal = responseGlobal.concat(response.results);
			responseByCategory = response.results;

			console.log("status length: "+responseByCategory.length);
			//var start = currPage *9;
			//var end = start + 9;
			var j = 0;
			var r1 = [];
			for (i = 0; i < 9; i++) { 
				if (i < responseByCategory.length){

					document.getElementById(j+'0').style.display = "block";
					document.getElementById(j).style.display = "block";

					var address = responseByCategory[j].propertyMap.occurrence_location;
					var type = responseByCategory[j].propertyMap.occurrence_type;
					var description = responseByCategory[j].propertyMap.occurrence_description;
					var datei = responseByCategory[j].propertyMap.occurrence_creation_time.value;
					var likes = responseByCategory[j].propertyMap.occurrence_likes;
					var urgency = responseByCategory[j].propertyMap.occurrence_urgency;
					var status = responseByCategory[j].propertyMap.occurrence_status;
					var creator = responseByCategory[j].propertyMap.occurrence_creator;
					var occid = responseByCategory[j].propertyMap.occurrence_id;
					var anonim = responseByCategory[j].propertyMap.occurrence_anonim;
					var filename = responseByCategory[j].propertyMap.occurrence_filename;
					console.log(filename);
					if (filename != ""){
						document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
					}else{
						document.getElementById(i+"img").src = "img/post-img1.jpg";
					}
					document.getElementById(i+"p").innerHTML = description;
					document.getElementById(i+"hr").innerHTML = type + " em " + address;


					document.getElementById(i+"likes").innerHTML = likes;
					document.getElementById(i+"date").innerHTML = datei;

					if(anonim){
						document.getElementById(i+"user").innerHTML = " | por Anónimo";
					}else{
						document.getElementById(i+"user").innerHTML = " | por "+creator;
					}
					//document.getElementById(i+"img").src = "img/post-img1.jpg";
					document.getElementById(i+"status").className = "label label-success";
					reportedTotal++;
					if(status == "Aberto"){
						document.getElementById(i+"status").className = "label label-danger";
					}else{
						resolvedTotal++;
						document.getElementById(i+"status").className = "label label-success";
					}
					document.getElementById(i+"status").innerHTML = status ;
					r1.push(responseByCategory[j]);
				}
				j++;
			}
			responseCurrPage = r1;
			localStorage.setItem("resolvedTotal",resolvedTotal);
			localStorage.setItem("reportedTotal",reportedTotal);
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}




listAll = function(event) {
	localStorage.setItem("currPage",0);
	whichType = 0
	currPage = 0;

	document.getElementById("00").style.display = "none";
	document.getElementById("10").style.display = "none";
	document.getElementById("20").style.display = "none";
	document.getElementById("30").style.display = "none";
	document.getElementById("40").style.display = "none";
	document.getElementById("50").style.display = "none";
	document.getElementById("60").style.display = "none";
	document.getElementById("70").style.display = "none";
	document.getElementById("80").style.display = "none";
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getAllOccurrences/"+"first",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){      
			console.log(response.cursor);
			currentCursor = response.cursor;
			responseGlobal = response.results;
			responseCurrPage = response.results;
			var start = currPage *9;
			var end = start + 9;
			var j = 0;
			var r1 = [];
			for (i = start; i < end; i++) { 
				if (i < response.results.length){

					document.getElementById(j+'0').style.display = "block";
					document.getElementById(j).style.display = "block";

					var address = response.results[j].propertyMap.occurrence_location;
					var type = response.results[j].propertyMap.occurrence_type;
					var description = response.results[j].propertyMap.occurrence_description;
					var datei = response.results[j].propertyMap.occurrence_creation_time.value;
					var likes = response.results[j].propertyMap.occurrence_likes;
					var urgency = response.results[j].propertyMap.occurrence_urgency;
					var status = response.results[j].propertyMap.occurrence_status;
					var creator = response.results[j].propertyMap.occurrence_creator;
					var occid = response.results[j].propertyMap.occurrence_id;
					var anonim = response.results[j].propertyMap.occurrence_anonim;
					var filename = response.results[j].propertyMap.occurrence_filename;
					console.log(filename);
					if (filename != ""){
						document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
					}else{
						document.getElementById(i+"img").src = "img/post-img1.jpg";
					}
					document.getElementById(i+"p").innerHTML = description;
					document.getElementById(i+"hr").innerHTML = type + " em " + address;

					if(contains(userLikes,occid)){
						document.getElementById(i+"likeme").style.color = "#66ccdb";
						document.getElementById(i+"likeme").value = "liked";

					}else{
						document.getElementById(i+"likeme").value = "not liked";
					}


					document.getElementById(i+"likes").innerHTML = likes;
					document.getElementById(i+"date").innerHTML = datei;

					if(anonim){
						document.getElementById(i+"user").innerHTML = " | por Anónimo";
					}else{
						document.getElementById(i+"user").innerHTML = " | por "+creator;
					}
					//document.getElementById(i+"img").src = "img/post-img1.jpg";
					document.getElementById(i+"status").className = "label label-success";
					reportedTotal++;
					if(status == "Aberto"){
						document.getElementById(i+"status").className = "label label-danger";
					}else{
						resolvedTotal++;
						document.getElementById(i+"status").className = "label label-success";
					}
					document.getElementById(i+"status").innerHTML = status ;
					r1.push(response.results[j]);
				}
				j++;
			}
			//responseCurrPage = r1;
			localStorage.setItem("resolvedTotal",resolvedTotal);
			localStorage.setItem("reportedTotal",reportedTotal);
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});
}

goIntoReport = function(event) {
	console.log("llellelelelelelel" + this.id);
	var co = this.id;
	var idd = co.split('')[0];
	console.log(co.split('') +" co");

	console.log(idd +" idd");
	console.log(responseCurrPage.length);
	localStorage.setItem("currReport_type",responseCurrPage[idd].propertyMap.occurrence_type);
	localStorage.setItem("currReport_location",responseCurrPage[idd].propertyMap.occurrence_location);
	localStorage.setItem("currReport_description",responseCurrPage[idd].propertyMap.occurrence_description);
	localStorage.setItem("currReport_creation_time",responseCurrPage[idd].propertyMap.occurrence_creation_time.value);
	localStorage.setItem("currReport_likes",responseCurrPage[idd].propertyMap.occurrence_likes);
	localStorage.setItem("currReport_urgency",responseCurrPage[idd].propertyMap.occurrence_urgency);
	localStorage.setItem("currReport_status",responseCurrPage[idd].propertyMap.occurrence_status);
	localStorage.setItem("currReport_creator",responseCurrPage[idd].propertyMap.occurrence_creator);
	localStorage.setItem("currReport_id",responseCurrPage[idd].propertyMap.occurrence_id);
	localStorage.setItem("currReport_filename",responseCurrPage[idd].propertyMap.occurrence_filename);
	localStorage.setItem("currReport_creationTime",responseCurrPage[idd].propertyMap.occurrence_creation_time.value);
	localStorage.setItem("currReport_rating",responseCurrPage[idd].propertyMap.occurrence_rating);
	localStorage.setItem("currReport_city",responseCurrPage[idd].propertyMap.occurrence_city);
	localStorage.setItem("currReport_anonim",responseCurrPage[idd].propertyMap.occurrence_anonim);

	console.log(responseCurrPage[idd].propertyMap.occurrence_creation_time.value);
	if(responseCurrPage[idd].propertyMap.occurrence_status == "Resolvido"){
		localStorage.setItem("currReport_changeTime",responseCurrPage[idd].propertyMap.occurrence_change_time.value);
	}

	window.location.href = "/occurrence.html";
}


var responseGlobal;
var responseByStatus;
var responseByCategory;
var responseCurrPage;
sendEmail = function(email, id){
	var obj = { "email": email, "id": id};
	$.ajax({ 
		type: "POST",
		url: "json_email_recovery.php",
		data: {x: obj},
		success: function(response){
			console.log(JSON.stringify(response));
			alert("Email WAS Sent!");
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});

};

undisplayNumNotifications = function(){
	document.getElementById("numNotifications").innerHTML = "0";
	var i;
	for(i = 0; i < myNotifications.length; i++){
		var notIDcurr = myNotifications[i].propertyMap.not_id;
		var openedNot = myNotifications[i].propertyMap.not_opened;
		if(!openedNot){
			$.ajax({
				type: "PUT",
				url: "/rest/login/openNotification/" + notIDcurr ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}
}


clickedNotification = function(event){
	console.log(this.id);
	console.log(this.value);
	var vall = Number(this.value);

	var notIDcurr = myNotifications[vall].propertyMap.not_id;
	var occIDcurr = myNotifications[vall].propertyMap.not_occID;
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getOneOccurrence/" + occIDcurr,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response1){        
			localStorage.setItem("currReport_type",response1.propertyMap.occurrence_type);
			localStorage.setItem("currReport_location",response1.propertyMap.occurrence_location);
			localStorage.setItem("currReport_description",response1.propertyMap.occurrence_description);
			localStorage.setItem("currReport_creation_time",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_likes",response1.propertyMap.occurrence_likes);
			localStorage.setItem("currReport_urgency",response1.propertyMap.occurrence_urgency);
			localStorage.setItem("currReport_status",response1.propertyMap.occurrence_status);
			localStorage.setItem("currReport_creator",response1.propertyMap.occurrence_creator);
			localStorage.setItem("currReport_id",response1.propertyMap.occurrence_id);
			localStorage.setItem("currReport_filename",response1.propertyMap.occurrence_filename);
			localStorage.setItem("currReport_creationTime",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_rating",response1.propertyMap.occurrence_rating);
			localStorage.setItem("currReport_city",response1.propertyMap.occurrence_city);

			console.log(response1.propertyMap.occurrence_creation_time.value);
			if(response1.propertyMap.occurrence_status == "Resolvido"){
				localStorage.setItem("currReport_changeTime",response1.propertyMap.occurrence_change_time.value);
			}

			window.location.href = "/occurrence.html";
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});


}

var notifications;
var myNotifications = [];
var numNotificationsTotal;

var isValidUser;
isValid = function(username){
	$.ajax({
		type: "GET",
		url: "/rest/register/getusername/" + username,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			isValidUser = true;
			$.ajax({
				type: "POST",
				url: "/rest/recover/" + username,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					//var lin = response;
					console.log(response);
					//enviar mail para o php
				},
				error: function(response) {
					alert("Error: "+ response.status);
				}
			});
		},
		error: function(response) {
			isValidUser = false;
		}
	});

	event.preventDefault();
};

recWord = function(event) {
	var data = $('form[name="recovery"]').jsonify();
	isValid(data.recovemail);
};

var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}


var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}

recovHide = function(event){
	document.getElementById("acc-recovery").style.display = "none";
}


likeme = function(event){
	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){
		var idd = Number(this.id.charAt(0));
		console.log(idd);
		var iddd = responseCurrPage[idd].propertyMap.occurrence_id;
		var currLikes = responseCurrPage[idd].propertyMap.occurrence_likes;
		if(document.getElementById(idd+"likeme").value == "not liked"){
			$.ajax({
				type: "PUT",
				url: "/rest/login/like/" + iddd + "/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					document.getElementById(idd+"likeme").style.color = "#66ccdb";
					document.getElementById(idd+"likeme").value = "liked";
					currLikes += 1;
					document.getElementById(idd+"likes").innerHTML = currLikes;

				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}else{
			$.ajax({
				type: "PUT",
				url: "/rest/login/unlike/" + iddd + "/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					document.getElementById(idd+"likeme").style.color = "black";
					document.getElementById(idd+"likeme").value = "not liked";

					currLikes -= 1;
					document.getElementById(idd+"likes").innerHTML = currLikes;
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}

}

var userLikes = [];
var responseMine = [];

var reportedTotal = 0;
var resolvedTotal = 0;

var currentCursor = "first";

window.onload = function() {

	if($('.navbar').length > 0){
		$(window).on("scroll load resize", function(){
			checkScroll();
		});
	}
	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){
		var now = +new Date();
		if(now > localStorage.getItem("tokenIDexpir")){
			logout();
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
		}else{
			document.getElementById("notifications").style.display = "block";
			document.getElementById("seguindoHide").style.display = "none";

			$.ajax({
				type: "GET",
				url: "/rest/login/getUser/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					console.log(response);
					console.log(response.propertyMap);
					console.log(response.propertyMap.user_email);
					localStorage.setItem('userLocation', response.propertyMap.user_address);
					localStorage.setItem('userType', response.propertyMap.user_type);
					if(localStorage.getItem("userType") == "Governo"){
						localStorage.setItem("userCity",response.propertyMap.user_city);
					}
					if(localStorage.getItem("userType") == "Admin"){
						document.getElementById("addGovOrNot").style.display = "block";
						//document.getElementById("statsOrNot").style.display = "block";

					}else if(localStorage.getItem("userType") == "Governo"){
						document.getElementById("colabOrNot").style.display = "block";
					}

					if(localStorage.getItem("userType") == "Cidadão"){
						document.getElementById("minhasHide").style.display = "block";
					}
					localStorage.setItem('userEmail', response.propertyMap.user_email);
					localStorage.setItem('userName', response.propertyMap.user_name);
					if(response.propertyMap.user_type == "Colaborador"){
						document.getElementById("solvingHide").style.display = "block";
					}

					$.ajax({ 
						type: "GET",
						//dataType: "json",
						url: "/rest/login/getAllNotificationsPage/" + response.propertyMap.user_email+"/"+"first",
						contentType: "application/json; charset=utf-8",
						crossDomain: true,
						success: function(response1){  

							if(response1.results.length != 0){
								currentCursorNoti = response1.cursor;
								responseCurrPageNoti = response1.results;
								console.log(response1.results.length);
								console.log("martaaaa");
								responseGlobalNoti = responseGlobalNoti.concat(response1.results);


								console.log(response1.results.length);
								var numNotifications = 0;
								document.getElementById("notifications").onclick = undisplayNumNotifications;

								for (i = 0; i < 4; i++) { 


									if(response1.results.length == i){
										break;
									}

									var readAlready = response1.results[i].propertyMap.not_opened;
									var message = response1.results[i].propertyMap.not_message;

									if(readAlready){
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +"</strong>";
									}else{
										numNotifications++;
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
									}
									myNotifications.push(response1.results[i]);
									var datei = response1.results[i].propertyMap.not_date.value;
									var idnot = response1.results[i].propertyMap.not_id;

									document.getElementById("notification"+i).style.display = "block";
									document.getElementById("notification"+i).onclick = clickedNotification;
									document.getElementById("date"+i).style.display = "block";
									document.getElementById("date"+i).innerHTML = datei;
									document.getElementById("title"+i).value = idnot;


									if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
										document.getElementById("text"+i).innerHTML = "";
									}else{
										document.getElementById("text"+i).innerHTML = "foi resolvido!";
									}
									
								}
								numNotificationsTotal = numNotifications;
								document.getElementById("numNotifications").innerHTML = numNotifications;
							}else{
								document.getElementById("prevPageNoti").style.display = "none";
								document.getElementById("nextPageNoti").style.display = "none";
								document.getElementById("numNotifications").innerHTML = "0";
							}
						},
						error: function(response) {
							alert("Error: "+ response.status);
							console.log(JSON.stringify(response));
						},
					});
					localStorage.setItem('userLikes', JSON.stringify(response.propertyMap.user_liked));

					userLikes = response.propertyMap.user_liked;
					$.ajax({ 
						type: "GET",
						//dataType: "json",
						url: "/rest/login/getAllOccurrences/"+"first",
						contentType: "application/json; charset=utf-8",
						crossDomain: true,
						success: function(response){      
							console.log(response.cursor);
							currentCursor = response.cursor;
							responseGlobal = response.results;
							responseCurrPage = response.results;
							var start = currPage *9;
							var end = start + 9;
							var j = 0;
							var r1 = [];
							for (i = start; i < end; i++) { 
								if (i < response.results.length){

									document.getElementById(j+'0').style.display = "block";
									document.getElementById(j).style.display = "block";

									var address = response.results[j].propertyMap.occurrence_location;
									var type = response.results[j].propertyMap.occurrence_type;
									var description = response.results[j].propertyMap.occurrence_description;
									var datei = response.results[j].propertyMap.occurrence_creation_time.value;
									var likes = response.results[j].propertyMap.occurrence_likes;
									var urgency = response.results[j].propertyMap.occurrence_urgency;
									var status = response.results[j].propertyMap.occurrence_status;
									var creator = response.results[j].propertyMap.occurrence_creator;
									var occid = response.results[j].propertyMap.occurrence_id;
									var filename = response.results[j].propertyMap.occurrence_filename;
									var anonim = response.results[j].propertyMap.occurrence_anonim;

									console.log(filename);
									if (filename != ""){
										document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
									}else{
										document.getElementById(i+"img").src = "img/post-img1.jpg";
									}
									document.getElementById(i+"p").innerHTML = description;
									document.getElementById(i+"hr").innerHTML = type + " em " + address;

									if(contains(userLikes,occid)){
										document.getElementById(i+"likeme").style.color = "#66ccdb";
										document.getElementById(i+"likeme").value = "liked";

									}else{
										document.getElementById(i+"likeme").value = "not liked";
									}


									document.getElementById(i+"likes").innerHTML = likes;
									document.getElementById(i+"date").innerHTML = datei;

									if(anonim){
										document.getElementById(i+"user").innerHTML = " | por Anónimo";
									}else{
										document.getElementById(i+"user").innerHTML = " | por "+creator;
									}
									//document.getElementById(i+"img").src = "img/post-img1.jpg";
									document.getElementById(i+"status").className = "label label-success";
									reportedTotal++;
									if(status == "Aberto"){
										document.getElementById(i+"status").className = "label label-danger";
									}else{
										resolvedTotal++;
										document.getElementById(i+"status").className = "label label-success";
									}
									document.getElementById(i+"status").innerHTML = status ;
									r1.push(response.results[j]);
								}
								j++;
							}
							responseCurrPage = response.results;
							localStorage.setItem("resolvedTotal",resolvedTotal);
							localStorage.setItem("reportedTotal",reportedTotal);
						},
						error: function(response) {
							alert("Error: "+ response.status);
							console.log(JSON.stringify(response));
						},
					});

					if(localStorage.getItem("userType") == "Cidadão"){
						document.getElementById("submitxi").style.display = "block";
					}
					document.getElementById("registar").style.display = "none";
					document.getElementById("entrar").style.display = "none";
					document.getElementById("sair").style.display = "block";
					document.getElementById("utilizador").style.display = "block";

					document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userName");
					if(localStorage.getItem("userType") == "Governo"){
						document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userCity");
					}
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}else{
		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllOccurrences/"+"first",
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 
				console.log(response.cursor);
				currentCursor = response.cursor;

				responseGlobal = response.results;
				responseCurrPage = response.results;

				var start = currPage *9;
				var end = start + 9;
				var j = 0;
				var r1 = [];
				for (i = start; i < end; i++) { 
					if (i < response.results.length){

						document.getElementById(j+'0').style.display = "block";
						document.getElementById(j).style.display = "block";

						var address = response.results[j].propertyMap.occurrence_location;
						var type = response.results[j].propertyMap.occurrence_type;
						var description = response.results[j].propertyMap.occurrence_description;
						var datei = response.results[j].propertyMap.occurrence_creation_time.value;
						var likes = response.results[j].propertyMap.occurrence_likes;
						var urgency = response.results[j].propertyMap.occurrence_urgency;
						var status = response.results[j].propertyMap.occurrence_status;
						var creator = response.results[j].propertyMap.occurrence_creator;
						var occid = response.results[j].propertyMap.occurrence_id;
						var anonim = response.results[j].propertyMap.occurrence_anonim;

						var filename = response.results[j].propertyMap.occurrence_filename;
						console.log(filename);
						if (filename != ""){
							document.getElementById(i+"img").src = "https://storage.googleapis.com/fit-sanctum-159416.appspot.com/"+filename;
						}else{
							document.getElementById(i+"img").src = "img/post-img1.jpg";
						}
						document.getElementById(i+"p").innerHTML = description;
						document.getElementById(i+"hr").innerHTML = type + " em " + address;


						document.getElementById(i+"likes").innerHTML = likes;
						document.getElementById(i+"date").innerHTML = datei;

						if(anonim){
							document.getElementById(i+"user").innerHTML = " | por Anónimo";
						}else{
							document.getElementById(i+"user").innerHTML = " | por "+creator;
						}
						//document.getElementById(i+"img").src = "img/post-img1.jpg";
						document.getElementById(i+"status").className = "label label-success";
						reportedTotal++;
						if(status == "Aberto"){
							document.getElementById(i+"status").className = "label label-danger";
						}else{
							resolvedTotal++;
							document.getElementById(i+"status").className = "label label-success";
						}
						document.getElementById(i+"status").innerHTML = status ;
						r1.push(response.results[j]);
					}
					j++;
				}
				//responseCurrPage = r1;
				localStorage.setItem("resolvedTotal",resolvedTotal);
				localStorage.setItem("reportedTotal",reportedTotal);
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});
	}

	document.getElementById('0likeme').onclick = likeme;
	document.getElementById('1likeme').onclick = likeme;
	document.getElementById('2likeme').onclick = likeme;
	document.getElementById('3likeme').onclick = likeme;
	document.getElementById('4likeme').onclick = likeme;
	document.getElementById('5likeme').onclick = likeme;
	document.getElementById('6likeme').onclick = likeme;
	document.getElementById('7likeme').onclick = likeme;
	document.getElementById('8likeme').onclick = likeme;

	if(localStorage.getItem('tokenID') == null || localStorage.getItem('tokenID') == ""){ //not logged in
		document.getElementById("registar").style.display = "block";
		document.getElementById("entrar").style.display = "block";
		document.getElementById("sair").style.display = "none";
		document.getElementById("utilizador").style.display = "none";
	}


	localStorage.setItem("currPage",0);
	var currPage = localStorage.getItem("currPage");
	whichType = 0;
	var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = captureData;

	var frms1 = $('form[name="register"]');     //var frms = document.getElementsByName("login");
	frms1[0].onsubmit = captureDataReg;

	var frms3 = $('form[name="recovery"]'); //recuperacaodepassword
	frms3[0].onsubmit = recWord;

	document.getElementById("sair").onclick = logout;
	document.getElementById("prevPage").onclick = prevPage;
	document.getElementById("nextPage").onclick = nextPage;
	document.getElementById("prevPageNoti").onclick = prevPageNoti;
	document.getElementById("nextPageNoti").onclick = nextPageNoti;

	document.getElementById("Todos").onclick = listAll;

	document.getElementById("Minhas").onclick = listMine;
	document.getElementById("Solving").onclick = listSolving;
	document.getElementById("Seguindo").onclick = listSeguindo;

	document.getElementById("Aberto").onclick = byStatus;
	document.getElementById("Resolvido").onclick = byStatus;

	document.getElementById("Sazonal").onclick = byCategory;
	document.getElementById("Lixo").onclick = byCategory;
	document.getElementById("Saúde Pública").onclick = byCategory;
	document.getElementById("Rua/Parque Danificado").onclick = byCategory;
	document.getElementById("Veículos/Estacionamento").onclick = byCategory;
	document.getElementById("Luzes").onclick = byCategory;
	document.getElementById("Graffiti Ilegal").onclick = byCategory;
	document.getElementById("Árvores").onclick = byCategory;

	$('.list-group-item').on('click', function() {
		var $this = $(this);
		var $alias = $this.data('alias');

		$('.active').removeClass('active');
		$this.toggleClass('active')

		// Pass clicked link element to another function
	})

	document.getElementById("0").onclick = goIntoReport;
	document.getElementById("1").onclick = goIntoReport;
	document.getElementById("2").onclick = goIntoReport;
	document.getElementById("3").onclick = goIntoReport;
	document.getElementById("4").onclick = goIntoReport;
	document.getElementById("5").onclick = goIntoReport;
	document.getElementById("6").onclick = goIntoReport;
	document.getElementById("7").onclick = goIntoReport;
	document.getElementById("8").onclick = goIntoReport;
	document.getElementById("0,0").onclick = goIntoReport;
	document.getElementById("1,0").onclick = goIntoReport;
	document.getElementById("2,0").onclick = goIntoReport;
	document.getElementById("3,0").onclick = goIntoReport;
	document.getElementById("4,0").onclick = goIntoReport;
	document.getElementById("5,0").onclick = goIntoReport;
	document.getElementById("6,0").onclick = goIntoReport;
	document.getElementById("7,0").onclick = goIntoReport;
	document.getElementById("8,0").onclick = goIntoReport;


}




prevPageNoti = function(event) {
	console.log("prev TA AQUIIII");
	event.stopPropagation();

	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	if (currPage > 0){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage--;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		document.getElementById("notifications").onclick = undisplayNumNotifications;

		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;


			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
						r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}

}

var responseGlobalNoti = [];
var responseCurrPageNoti = [];
var currentCursorNoti = "first";

nextPageNoti = function(event) {
	console.log("next TA AQUIIII");

	event.stopPropagation();

	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	var t = (r.length / 4) - 1;


	if (currPage < t){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage++;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		undisplayNumNotifications;
		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;


			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
			
			r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}else{

		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllNotificationsPage/"+ localStorage.getItem('userEmail') +"/"+currentCursorNoti,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					document.getElementById("notification0").style.display = "none";
					document.getElementById("notification1").style.display = "none";
					document.getElementById("notification2").style.display = "none";
					document.getElementById("notification3").style.display = "none";
					currPage++;
					localStorage.setItem("currPageNoti",currPage);
					currentCursorNoti = response.cursor;
					responseCurrPageNoti = response.results;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobalNoti = responseGlobalNoti.concat(response.results);
					var j = 0;
					var numNotifications = 0;
					undisplayNumNotifications;
					for (i = 0; i < 4; i++) { 


						if(response.results.length == i){
							break;
						}

						var readAlready = response.results[i].propertyMap.not_opened;
						var message = response.results[i].propertyMap.not_message;

						if(readAlready){
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
						}else{
							numNotifications++;
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
						}
						myNotifications.push(response.results[i]);
						var datei = response.results[i].propertyMap.not_date.value;
						var idnot = response.results[i].propertyMap.not_id;

						document.getElementById("notification"+j).style.display = "block";
						document.getElementById("notification"+j).onclick = clickedNotification;
						document.getElementById("date"+j).style.display = "block";
						document.getElementById("date"+j).innerHTML = datei;
						document.getElementById("title"+j).value = idnot;


						if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
							document.getElementById("text"+j).innerHTML = "";
						}else{
							document.getElementById("text"+j).innerHTML = "foi resolvido!";
						}
												//r1.push(response.results[i]);
						j++;

					}
					console.log(response.results);
					responseCurrPageNoti = response.results;
					numNotificationsTotal = numNotifications;
					document.getElementById("numNotifications").innerHTML = numNotifications;
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});

	}
}


