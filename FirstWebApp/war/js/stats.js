var labelz = [];
var sectionz = [];

function getSections(){
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/register/getSections/" + localStorage.getItem("userCity") ,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			responseGlobal = response;
			for (i = 0; i < responseGlobal.length; i++) { 
				labelz.push(responseGlobal[i]);
				getSatisfaction(responseGlobal[i]);
			}   
		},
		error: function(response) {
			//document.getElementById("myChart").style.display = "none";
		},
	});
}

function getSatisfaction(section){
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/ocurrences/satisfactionpercentage/" + localStorage.getItem("userCity") + "/" + section ,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){ 
			var val = response/100*5;
			sectionz.push("val");
		},
		error: function(response) {
			sectionz.push("0");
		},
	});
}

function initChart () {
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labelz,
			datasets: [{
				label: '% de satisfação após resolução de ocorrências',
				data: sectionz,
				backgroundColor: [
				                  'rgba(255, 99, 132, 0.2)',
				                  'rgba(54, 162, 235, 0.2)',
				                  'rgba(255, 206, 86, 0.2)',
				                  'rgba(75, 192, 192, 0.2)',
				                  'rgba(153, 102, 255, 0.2)',
				                  'rgba(255, 159, 64, 0.2)'
				                  ],
				                  borderColor: [
				                                'rgba(255,99,132,1)',
				                                'rgba(54, 162, 235, 1)',
				                                'rgba(255, 206, 86, 1)',
				                                'rgba(75, 192, 192, 1)',
				                                'rgba(153, 102, 255, 1)',
				                                'rgba(255, 159, 64, 1)'
				                                ],
				                                borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});

}

function getcitystats(){
	var city = localStorage.getItem("userCity");
	$.ajax({
		type: "GET",
		url: "/rest/ocurrences/cityStats/" + city + "/"+"todas",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			document.getElementById("submited-nr").innerHTML = response[0];
			document.getElementById("resolved-nr").innerHTML = response[1];
			document.getElementById("sat-nr").innerHTML = response[2];
		},
		error: function(response) {
		}
	});

	event.preventDefault();
}

window.onload = function() {
	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){
		var now = +new Date();
		if(now > localStorage.getItem("tokenIDexpir")){
			logout();
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
		}else{

			$.ajax({
				type: "GET",
				url: "/rest/login/getUser/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					console.log(response);
					console.log(response.propertyMap);
					console.log(response.propertyMap.user_email);
					localStorage.setItem('userLocation', response.propertyMap.user_address);
					localStorage.setItem('userType', response.propertyMap.user_type);
					localStorage.setItem('userEmail', response.propertyMap.user_email);
					localStorage.setItem('userName', response.propertyMap.user_name);
					console.log(localStorage.getItem("userType"));
					getcitystats();
					getSections()
					initChart();
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}
}