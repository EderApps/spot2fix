<?php
header('Content-Type: application/json');
$_POST = json_decode($_POST["x"], false);

if(isset($_POST)) {
 
     
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $email_from = "are.pereira@campus.fct.unl.pt";
 
    $email_subject = "Password Reset";
 
   
 
     
 
    function died($error) {
 
        // your error code can go here
 
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";
 
        die();
 
    }
  
    $email_to = $_POST['email'];
    $id = $_POST['id'];
    $email_message = "Form details below.\n\n";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }

    $email_message .= "Link: ".clean_string($id)."\n";
  
// create email headers
 
$headers = 'From: '.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
/*----
Un-comment to enable email sending
----*/
mail($email_to, $email_subject, $email_message, $headers); 
 
?>
{
    <!-- console degugger check to see if the form submitted correctly -->
    "success": true,
} 
 
<?php
 
} else {

?>   
{
    "success": false
} 

<?php
}


 
?>