var idU;

checkID = function(){

	$.ajax({
		type: "PUT",
		url: "/rest/register/" + idU +"/confirmemail",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            window.location.href = "/";
		},
		error: function(response) {
            document.getElementById("title").innerHTML="Falha na confirmação de email.";
			document.getElementById("text").innerHTML="Por favor confirme no seu email o link de confirmação de senha.";
            
		}
	});

	event.preventDefault();
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};
 

window.onload = function() {
    idU = getQueryVariable("id");
    checkID();
}