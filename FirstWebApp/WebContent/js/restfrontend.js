
logout = function(event) {
	var token = localStorage.getItem('tokenID');
	var tokenname = localStorage.getItem('tokenIDname');
	var tokenexp = localStorage.getItem('tokenIDexpir');
	var tokencre = localStorage.getItem('tokenIDcreate');
	var data = {};
	data.tokenID = token;
	data.username = tokenname;
	data.expirationData = tokenexp;
	data.creationData = tokencre;
	console.log(data);

	console.log(JSON.stringify(data));
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/logout/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
			localStorage.setItem('userLocation', "");
			localStorage.setItem('userType', "");
			localStorage.setItem('userEmail', "");
			localStorage.setItem('userName', "");
			localStorage.setItem('userLikes', "");

			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
	signOut();

}


captureData = function(event) {

	console.log("ekfmkrnkn");

	var data = $('form[name="login"]').jsonify();

	var checked = document.getElementById("check").checked;
	data.checked = checked;
	console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			// Store token id for later use in localStorage
			console.log(response.username);
			console.log(response.tokenID);
			console.log(response.creationData);
			console.log(response.expirationData);

			localStorage.setItem('tokenIDname', response.username);
			localStorage.setItem('tokenID', response.tokenID);
			localStorage.setItem('tokenIDcreate', response.creationData);
			localStorage.setItem('tokenIDexpir', response.expirationData);

			window.location.href = "/";

		},
		error: function(response) {
			if(response.status == 333){
				alert("A sua conta ainda não foi activada. Por favor vá à sua caixa de correio e confirme o seu email.");
			}else{
				alert("O email/password que inseriu está incorreto.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function onFailure(error) {
	alert(error);
}
function renderButton() {
	gapi.signin2.render('gSignIn', {
		'scope': 'profile email',
		'width': 290,
		'height': 50,
		'longtitle': true,
		'theme': 'dark',
		'onsuccess': onSuccess,
		'onfailure': onFailure
	});
}
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		$('.userContent').html('');
		$('#gSignIn').slideDown('slow');
	});
}

function onSuccess(googleUser) {

	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	var checked = document.getElementById("check").checked;
	var data = {};
	data.username = profile.getEmail();
	data.password = profile.getId();
	data.checked = checked;
	console.log(JSON.stringify(data));
	if(localStorage.getItem('tokenID') == ""){
		$.ajax({
			type: "POST",
			url: "/rest/login/logingoogle/" + profile.getEmail(),
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				// Store token id for later use in localStorage
				console.log(response.username);
				console.log(response.tokenID);
				console.log(response.creationData);
				console.log(response.expirationData);

				localStorage.setItem('tokenIDname', response.username);
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenIDcreate', response.creationData);
				localStorage.setItem('tokenIDexpir', response.expirationData);

				window.location.href = "/";

			},
			error: function(response) {
				//alert("Error: "+ response.status);
			},
			data: JSON.stringify(data)
		});
	}
}

captureDataReg = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Enviámos um mail para o endereço que registou. Por favor confirme para poder entrar.");
			window.location.href = "/";
		},
		error: function(response) {
			if(response.status == 111){
				alert("Registo inválido. Por favor verifique todos os campos.");
			}else if(response.status == 222){
				alert("Email inválido. Os emails têm de conter '@'.");
			}else if(response.status == 333){
				alert("Password inválida. As passwords têm de ter 6 caracteres ou mais, e conter números e letras.");
			}else if(response.status == 444){
				alert("A password que inseriu não coincide com a confirmação da password. Tente de novo.");
			}else if(response.status == 400){
				alert("Já existe um utilizador registado com o email que inseriu.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

var isValidUser;
isValid = function(username){
	console.log("marian "+username);
	$.ajax({
		type: "GET",
		url: "/rest/register/getusername/" + username,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			isValidUser = true;
			$.ajax({
				type: "POST",
				url: "/rest/recover/" + username,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					//var lin = response;
					console.log(response);
					//enviar mail para o php
					alert("Mail enviado!");
				},
				error: function(response) {
					alert("Error: "+ response.status);
				}
			});
		},
		error: function(response) {
			isValidUser = false;
		}
	});

	event.preventDefault();
};

recWord = function(event) {
	var data = $('form[name="recovery"]').jsonify();
	isValid(data.recovemail);
};

var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}


var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}

recovHide = function(event){
	document.getElementById("acc-recovery").style.display = "none";
}

undisplayNumNotifications = function(){
	document.getElementById("numNotifications").innerHTML = "0";
	var i;
	for(i = 0; i < responseCurrPageNoti.length; i++){
		var notIDcurr = responseCurrPageNoti[i].propertyMap.not_id;
		var openedNot = responseCurrPageNoti[i].propertyMap.not_opened;
		if(!openedNot){
			$.ajax({
				type: "PUT",
				url: "/rest/login/openNotification/" + notIDcurr ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}
}


clickedNotification = function(event){
	console.log(this.id);
	console.log(this.value);
	var vall = Number(this.value);

	var notIDcurr = responseCurrPageNoti[vall].propertyMap.not_id;
	var occIDcurr = responseCurrPageNoti[vall].propertyMap.not_occID;
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getOneOccurrence/" + occIDcurr,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response1){        
			localStorage.setItem("currReport_type",response1.propertyMap.occurrence_type);
			localStorage.setItem("currReport_location",response1.propertyMap.occurrence_location);
			localStorage.setItem("currReport_description",response1.propertyMap.occurrence_description);
			localStorage.setItem("currReport_creation_time",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_likes",response1.propertyMap.occurrence_likes);
			localStorage.setItem("currReport_urgency",response1.propertyMap.occurrence_urgency);
			localStorage.setItem("currReport_status",response1.propertyMap.occurrence_status);
			localStorage.setItem("currReport_creator",response1.propertyMap.occurrence_creator);
			localStorage.setItem("currReport_id",response1.propertyMap.occurrence_id);
			localStorage.setItem("currReport_filename",response1.propertyMap.occurrence_filename);
			localStorage.setItem("currReport_creationTime",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_rating",response1.propertyMap.occurrence_rating);
			localStorage.setItem("currReport_city",response1.propertyMap.occurrence_city);

			console.log(response1.propertyMap.occurrence_creation_time.value);
			if(response1.propertyMap.occurrence_status == "Resolvido"){
				localStorage.setItem("currReport_changeTime",response1.propertyMap.occurrence_change_time.value);
			}

			window.location.href = "/occurrence.html";
		},
		error: function(response) {
			alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});


}

function checkScroll(){
	var startY = $('.navbar').height() * 2; //The point where the navbar changes in px

	if($(window).scrollTop() > startY){
		$('.navbar').addClass("scrolled");
		$('.dropdown').addClass("scrolled");
	}else{
		$('.navbar').removeClass("scrolled");
	}
}


function getcitystats(city){
    $.ajax({
		type: "GET",
		url: "/rest/ocurrences/cityStats/" + city + "/"+"todas",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            document.getElementById("city-"+ city  +"-resolved").innerHTML = response[0];
            document.getElementById("city-"+ city  +"-sat").innerHTML = response[1];
        },
		error: function(response) {
		}
	});

	event.preventDefault();
};

var notifications;
var myNotifications = [];
var numNotificationsTotal;
window.onload = function() {
	console.log(localStorage.getItem("tokenIDname"));
	var counted = false;
	if($('.navbar').length > 0){
		$(window).on("scroll load resize", function(){
			checkScroll();
		});
	}
	
	var cities = ["Lisboa", "Almada"];
    for(i=0; i<cities.length; i++) {
        getcitystats(cities[i]);
    }

	$(window).scroll(function() {
		var hT = $('.row2').offset().top,
		hH = $('.row2').outerHeight(),
		wH = $(window).height(),
		wS = $(this).scrollTop();
		if (wS > (hT+hH-wH) && (hT > wS) && (wS+wH > hT+hH && !counted)){
			$('.count').each(function () {
				$(this).prop('Counter',0).animate({
					Counter: $(this).text()
				}, {
					duration: 4000,
					easing: 'swing',
					step: function (now) {
						$(this).text(Math.ceil(now));
					}
				});
			});
			counted=true;
		}
	});

	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){
		var now = +new Date();
		if(now > localStorage.getItem("tokenIDexpir")){
			logout();
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
		}else{
			document.getElementById("notifications").style.display = "block";

			$.ajax({
				type: "GET",
				url: "/rest/login/getUser/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					console.log(response);
					console.log(response.propertyMap);
					console.log(response.propertyMap.user_email);
					localStorage.setItem('userLocation', response.propertyMap.user_address);
					localStorage.setItem('userType', response.propertyMap.user_type);
					if(localStorage.getItem("userType") == "Governo"){
						localStorage.setItem("userCity",response.propertyMap.user_city);
					}
					localStorage.setItem('userEmail', response.propertyMap.user_email);
					localStorage.setItem('userName', response.propertyMap.user_name);
					console.log(localStorage.getItem("userType"));
					if(localStorage.getItem("userType") == "Admin"){
						document.getElementById("addGovOrNot").style.display = "block";
						//document.getElementById("statsOrNot").style.display = "block";
					}else if(localStorage.getItem("userType") == "Governo"){
						document.getElementById("colabOrNot").style.display = "block";
					}

					$.ajax({ 
						type: "GET",
						//dataType: "json",
						url: "/rest/login/getAllNotificationsPage/" + response.propertyMap.user_email+"/"+"first",
						contentType: "application/json; charset=utf-8",
						crossDomain: true,
						success: function(response1){  

							if(response1.results.length != 0){
								currentCursorNoti = response1.cursor;
								responseCurrPageNoti = response1.results;
								console.log(response1.results.length);
								console.log("martaaaa");
								responseGlobalNoti = responseGlobalNoti.concat(response1.results);


								console.log(response1.results.length);
								var numNotifications = 0;
								document.getElementById("notifications").onclick = undisplayNumNotifications;

								for (i = 0; i < 4; i++) { 


									if(response1.results.length == i){
										break;
									}

									var readAlready = response1.results[i].propertyMap.not_opened;
									var message = response1.results[i].propertyMap.not_message;

									if(readAlready){
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +"</strong>";
									}else{
										numNotifications++;
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
									}
									myNotifications.push(response1.results[i]);
									var datei = response1.results[i].propertyMap.not_date.value;
									var idnot = response1.results[i].propertyMap.not_id;

									document.getElementById("notification"+i).style.display = "block";
									document.getElementById("notification"+i).onclick = clickedNotification;
									document.getElementById("date"+i).style.display = "block";
									document.getElementById("date"+i).innerHTML = datei;
									document.getElementById("title"+i).value = idnot;


									if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
										document.getElementById("text"+i).innerHTML = "";
									}else{
										document.getElementById("text"+i).innerHTML = "foi resolvido!";
									}
									
								}
								numNotificationsTotal = numNotifications;
								document.getElementById("numNotifications").innerHTML = numNotifications;
							}else{
								document.getElementById("prevPageNoti").style.display = "none";
								document.getElementById("nextPageNoti").style.display = "none";
								document.getElementById("numNotifications").innerHTML = "0";
							}
						},
						error: function(response) {
							alert("Error: "+ response.status);
							console.log(JSON.stringify(response));
						},
					});

					if(localStorage.getItem("userType") == "Cidadão"){
						document.getElementById("submitxi").style.display = "block";
					}
					document.getElementById("registar").style.display = "none";
					document.getElementById("entrar").style.display = "none";
					document.getElementById("sair").style.display = "block";
					document.getElementById("utilizador").style.display = "block";
					document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userName");
					if(localStorage.getItem("userType") == "Governo"){
						document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userCity");
					}
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}

	var reportedTotal;
	var resolvedTotal;
	var totalUsers;

	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getStatistics/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			resolvedTotal = response.propertyMap.stats_occurrencesResolved;
			reportedTotal = response.propertyMap.stats_occurrencesReported;
			totalUsers = response.propertyMap.stats_totalUsers;
			document.getElementById("ocres").innerHTML = resolvedTotal;
			document.getElementById("ocrep").innerHTML = reportedTotal;
			document.getElementById("totalUsers").innerHTML = totalUsers;

		},
		error: function(response) {
		},
	});




	if (localStorage.getItem('tokenID') == null){
		localStorage.setItem('tokenID', "");
	}
	if (localStorage.getItem('tokenID').localeCompare("") != 0){
		//window.location.href = "/userarea";

	}

	if(localStorage.getItem('tokenID') == null || localStorage.getItem('tokenID') == ""){ //not logged in
		document.getElementById("registar").style.display = "block";
		document.getElementById("entrar").style.display = "block";
		document.getElementById("sair").style.display = "none";
		document.getElementById("utilizador").style.display = "none";
		document.getElementById("notifications").style.display = "none";
	}


	var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
	frms[0].onsubmit = captureData;

	var frms1 = $('form[name="register"]');     //var frms = document.getElementsByName("login");
	frms1[0].onsubmit = captureDataReg;
	document.getElementById("sair").onclick = logout;
	document.getElementById("prevPageNoti").onclick = prevPageNoti;
	document.getElementById("nextPageNoti").onclick = nextPageNoti;

	var frms2 = $('form[name="recovery"]'); //recuperacaodepassword
	frms2[0].onsubmit = recWord;

	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/checkTimes/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){


			//alert("Checked times!");

		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});

}



prevPageNoti = function(event) {
	console.log("prev TA AQUIIII");
	event.stopPropagation();

	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	if (currPage > 0){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage--;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		document.getElementById("notifications").onclick = undisplayNumNotifications;

		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;


			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
						r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}

}

var responseGlobalNoti = [];
var responseCurrPageNoti = [];
var currentCursorNoti = "first";

nextPageNoti = function(event) {
	console.log("next TA AQUIIII");

	event.stopPropagation();
	
	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	var t = (r.length / 4) - 1;


	if (currPage < t){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage++;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		undisplayNumNotifications;
		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;
			if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
				document.getElementById("text"+j).innerHTML = "";
			}else{
				document.getElementById("text"+j).innerHTML = "foi resolvido!";
			}
			r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}else{

		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllNotificationsPage/"+ localStorage.getItem('userEmail') +"/"+currentCursorNoti,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					document.getElementById("notification0").style.display = "none";
					document.getElementById("notification1").style.display = "none";
					document.getElementById("notification2").style.display = "none";
					document.getElementById("notification3").style.display = "none";
					currPage++;
					localStorage.setItem("currPageNoti",currPage);
					currentCursorNoti = response.cursor;
					responseCurrPageNoti = response.results;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobalNoti = responseGlobalNoti.concat(response.results);
					var j = 0;
					var numNotifications = 0;
					undisplayNumNotifications;
					for (i = 0; i < 4; i++) { 


						if(response.results.length == i){
							break;
						}

						var readAlready = response.results[i].propertyMap.not_opened;
						var message = response.results[i].propertyMap.not_message;

						if(readAlready){
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
						}else{
							numNotifications++;
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
						}
						myNotifications.push(response.results[i]);
						var datei = response.results[i].propertyMap.not_date.value;
						var idnot = response.results[i].propertyMap.not_id;

						document.getElementById("notification"+j).style.display = "block";
						document.getElementById("notification"+j).onclick = clickedNotification;
						document.getElementById("date"+j).style.display = "block";
						document.getElementById("date"+j).innerHTML = datei;
						document.getElementById("title"+j).value = idnot;

						if(message == "Foi lhe atribuída esta ocorrência para resolver!"){
							document.getElementById("text"+j).innerHTML = "";
						}else{
							document.getElementById("text"+j).innerHTML = "foi resolvido!";
						}
						//r1.push(response.results[i]);
						j++;

					}
					console.log(response.results);
					responseCurrPageNoti = response.results;
					numNotificationsTotal = numNotifications;
					document.getElementById("numNotifications").innerHTML = numNotifications;
				}
			},
			error: function(response) {
				alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});

	}
}

