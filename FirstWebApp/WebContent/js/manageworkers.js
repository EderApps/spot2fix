var selectedWorker = "15";
function checkScroll(){
    var startY = $('.navbar').height() * 1; //The point where the navbar changes in px

    if($(window).scrollTop() > startY){
        $('.navbar').addClass("scrolled");
        $('.dropdown').addClass("scrolled");
    }else{
        $('.navbar').removeClass("scrolled");
    }
};

logout = function(event) {
	var token = localStorage.getItem('tokenID');
	var tokenname = localStorage.getItem('tokenIDname');
	var tokenexp = localStorage.getItem('tokenIDexpir');
	var tokencre = localStorage.getItem('tokenIDcreate');
	var data = {};
	data.tokenID = token;
	data.username = tokenname;
	data.expirationData = tokenexp;
	data.creationData = tokencre;
	console.log(data);

	console.log(JSON.stringify(data));
	$.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/login/logout/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
			localStorage.setItem('userLocation', "");
			localStorage.setItem('userType', "");
			localStorage.setItem('userEmail', "");
			localStorage.setItem('userCity', "");
			localStorage.setItem('userName', "");
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
		data:JSON.stringify(data)
	});
	signOut();

};

function onFailure(error) {
    //alert(error);
};

function onSuccess(googleUser) {

	  var profile = googleUser.getBasicProfile();
	  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	  console.log('Name: ' + profile.getName());
	  console.log('Image URL: ' + profile.getImageUrl());
	  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	    var checked = document.getElementById("check").checked;
	  var data = {};
	  data.username = profile.getEmail();
	  data.password = profile.getId();
	  data.checked = checked;
	  console.log(JSON.stringify(data));
	  if(localStorage.getItem('tokenID') == ""){

	  $.ajax({
			type: "POST",
			url: "/rest/login/logingoogle",
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			//dataType: "json",
			success: function(response) {
				// Store token id for later use in localStorage
				console.log(response.username);
				console.log(response.tokenID);
				console.log(response.creationData);
				console.log(response.expirationData);

				localStorage.setItem('tokenIDname', response.username);
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenIDcreate', response.creationData);
				localStorage.setItem('tokenIDexpir', response.expirationData);
				window.location.href = "/submitreport";

			},
			error: function(response) {
				//alert("Error: "+ response.status);
			},
			data: JSON.stringify(data)
		});
	  }
};

function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('User signed out.');
	});
};

function onLoad() {
	gapi.load('auth2', function() {
		gapi.auth2.init();
	});
};

var editjumbo = false;    
editWorkerInfo = function(event) {
		
    var res = this.id.split("-");
    var idd = res[2];
    console.log("monstroooo idd "+idd +" selected is "+selectedWorker);
    if((!editjumbo && selectedWorker != idd) || (!editjumbo && selectedWorker == idd) || (editjumbo && selectedWorker != idd)){
    	
        document.getElementById("edit-jumbo").style.display = "block";
        selectedWorker = idd;
    	var currName = document.getElementById("worker-"+idd+"-name").innerHTML;
    	console.log(currName+ " nameeee");
    	document.getElementById("nameEditing").innerHTML = currName;
        //document.getElementById("companyname").placeholder= document.getElementById("worker-"+ idd +"-company").innerHTML;
        //document.getElementById("name").placeholder= document.getElementById("worker-"+ idd +"-name").innerHTML;
        editjumbo = true;
    }else if(editjumbo && selectedWorker == idd){
        document.getElementById("edit-jumbo").style.display = "none";
        undoRemoval;
        editjumbo = false;
    }
};

removeWorkerModal = function(event) {
    var res = this.id.split("-");
    var idd = res[2];
    selectedWorker = idd;
};

function undoRemoval() {
    selectedWorker = "15";
};

captureData = function(event) {

	console.log("ekfmkrnkn");

	var data = $('form[name="login"]').jsonify();

	var checked = document.getElementById("check").checked;
	data.checked = checked;
	console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			// Store token id for later use in localStorage
			console.log(response.username);
			console.log(response.tokenID);
			console.log(response.creationData);
			console.log(response.expirationData);

			localStorage.setItem('tokenIDname', response.username);
			localStorage.setItem('tokenID', response.tokenID);
			localStorage.setItem('tokenIDcreate', response.creationData);
			localStorage.setItem('tokenIDexpir', response.expirationData);

			window.location.href = "/";

		},
		error: function(response) {
			if(response.status == 333){
				alert("A sua conta ainda não foi activada. Por favor vá à sua caixa de correio e confirme o seu email.");
			}else{
				alert("O email/password que inseriu está incorreto.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};


captureDataReg = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Enviámos um mail para o endereço que registou. Por favor confirme para poder entrar.");

			window.location.href = "/";
		},
		error: function(response) {
			if(response.status == 111){
				alert("Registo inválido. Por favor verifique todos os campos.");
			}else if(response.status == 222){
				alert("Email inválido. Os emails têm de conter '@'.");
			}else if(response.status == 333){
				alert("Password inválida. As passwords têm de ter 6 caracteres ou mais, e conter números e letras.");
			}else if(response.status == 444){
				alert("A password que inseriu não coincide com a confirmação da password. Tente de novo.");
			}else if(response.status == 400){
				alert("Já existe um utilizador registado com o email que inseriu.");
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function sectionSelected() {
    var x = document.getElementById("typeOc").value;
    
    document.getElementById("section-title").style.display = "block";
    $("#section-category h3").html(x);
   
    document.getElementById("managing-info").style.display = "none";
    document.getElementById("not-found").style.display = "none";
    for(z = 0; z < 9; z++){
        document.getElementById("worker-"+z).style.display = "none";
    }
    
    $.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/register/getWorkers/" + x + "/" + localStorage.getItem("userCity") ,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			responseGlobal = response;
            for (i = 0; i < 9; i++) { 
                if (i < responseGlobal.length){
                    document.getElementById("worker-"+i+"-name").innerHTML = responseGlobal[i].propertyMap.user_incharge;
                    document.getElementById("worker-"+i+"-company").innerHTML = responseGlobal[i].propertyMap.user_name;
                    document.getElementById("worker-"+i+"-email").innerHTML = responseGlobal[i].propertyMap.user_email;
                    document.getElementById("worker-"+i+"-date").innerHTML = responseGlobal[i].propertyMap.user_creation_time.value;
                    document.getElementById("worker-" + i).style.display = "block";
                }
            }   
		},
		error: function(response) {
            document.getElementById("not-found").style.display = "block";
		},
	});
};

function searchWorker() {
    var x = document.getElementById("search-worker").value;
    
    document.getElementById("not-found").style.display = "none";
    for(z = 0; z < 9; z++){
        document.getElementById("worker-"+z).style.display = "none";
    }
    document.getElementById("managing-info").style.display = "none";
    document.getElementById("section-title").style.display = "none";
    $.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/register/searchWorker/" + x + "/" +localStorage.getItem("userCity") ,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){        
			responseGlobal = response;
            for (i = 0; i < 9; i++) { 
                if (i < responseGlobal.length){
                    document.getElementById("worker-" + i).style.display = "block";
                    document.getElementById("worker-"+i+"-name").innerHTML = responseGlobal[i].propertyMap.user_incharge;
                    document.getElementById("worker-"+i+"-company").innerHTML = responseGlobal[i].propertyMap.user_name;
                    document.getElementById("worker-"+i+"-email").innerHTML = responseGlobal[i].propertyMap.user_email;
                    document.getElementById("worker-"+i+"-date").innerHTML = responseGlobal[i].propertyMap.user_creation_time.value;
                }
            }
		},
		error: function(response) {
            document.getElementById("not-found").style.display = "block";
		},
	});
    event.preventDefault();
};


addSection = function(event) {
    var x = document.getElementById("section-selected").value;
    console.log(x);
	$.ajax({
		type: "POST",
		url: "/rest/register/add/" + localStorage.getItem("userCity") +"/collaborator/" + document.getElementById("worker-"+ selectedWorker +"-email").innerHTML+"/"+x,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            //alert("Adicionado com sucesso!");
			window.location.href = "/manageworkers.html";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
		},
	});
    
    event.preventDefault();
};



addWorker = function(event) {
	var data = $('form[name="add-worker"]').jsonify();
    data.city = localStorage.getItem("userCity");
    console.log(data);
	console.log(JSON.stringify(data));
	$.ajax({
		type: "POST",
		url: "/rest/register/collaborator",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            //alert("Colaborador adicionado!");
			window.location.href = "/manageworkers.html";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function removeWorker(){
    var x = document.getElementById("typeOc").value;
    console.log(x);

	$.ajax({
		type: "PUT",
		url: "/rest/register/removeSection/" + localStorage.getItem("userCity") +"/collaborator/" + document.getElementById("worker-"+ selectedWorker +"-email").innerHTML +"/" + x,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            //alert("Removido com sucesso!");
			window.location.href = "/manageworkers.html";

		},
		error: function(response) {
			//alert("Error: "+ response.status);
		}
	});
    
    event.preventDefault();
};

window.onload = function() {
	if(localStorage.getItem("tokenIDname") != null && localStorage.getItem("tokenIDname") != ""){

		var now = +new Date();
		if(now > localStorage.getItem("tokenIDexpir")){
			logout();
			localStorage.setItem('tokenIDname', "");
			localStorage.setItem('tokenID', "");
			localStorage.setItem('tokenIDcreate', "");
			localStorage.setItem('tokenIDexpir', "");
		}else{
			document.getElementById("notifications").style.display = "block";

			$.ajax({
				type: "GET",
				url: "/rest/login/getUser/" + localStorage.getItem("tokenIDname") ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					console.log(response);
					console.log(response.propertyMap);
					console.log(response.propertyMap.user_email);
					localStorage.setItem('userLocation', response.propertyMap.user_address);
					localStorage.setItem('userType', response.propertyMap.user_type);
					if(localStorage.getItem("userType") == "Governo"){
						localStorage.setItem("userCity",response.propertyMap.user_city);
					}
					if(response.propertyMap.user_type != "Governo"){
						window.location.href = "/";
					}else{
						localStorage.setItem("userCity",response.propertyMap.user_city);
					}
					if(localStorage.getItem("userType") == "Admin"){
						document.getElementById("addGovOrNot").style.display = "block";
						//document.getElementById("statsOrNot").style.display = "block";

					}else if(localStorage.getItem("userType") == "Governo"){
						document.getElementById("colabOrNot").style.display = "block";
					}
					localStorage.setItem('userEmail', response.propertyMap.user_email);
					localStorage.setItem('userName', response.propertyMap.user_name);
					$.ajax({ 
						type: "GET",
						//dataType: "json",
						url: "/rest/login/getAllNotificationsPage/" + response.propertyMap.user_email+"/"+"first",
						contentType: "application/json; charset=utf-8",
						crossDomain: true,
						success: function(response1){  

							if(response1.results.length != 0){
								currentCursorNoti = response1.cursor;
								responseCurrPageNoti = response1.results;
								console.log(response1.results.length);
								console.log("martaaaa");
								responseGlobalNoti = responseGlobalNoti.concat(response1.results);


								console.log(response1.results.length);
								var numNotifications = 0;
								document.getElementById("notifications").onclick = undisplayNumNotifications;

								for (i = 0; i < 4; i++) { 


									if(response1.results.length == i){
										break;
									}

									var readAlready = response1.results[i].propertyMap.not_opened;
									var message = response1.results[i].propertyMap.not_message;

									if(readAlready){
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +"</strong>";
									}else{
										numNotifications++;
										document.getElementById("title"+i).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
									}
									myNotifications.push(response1.results[i]);
									var datei = response1.results[i].propertyMap.not_date.value;
									var idnot = response1.results[i].propertyMap.not_id;

									document.getElementById("notification"+i).style.display = "block";
									document.getElementById("notification"+i).onclick = clickedNotification;
									document.getElementById("date"+i).style.display = "block";
									document.getElementById("date"+i).innerHTML = datei;
									document.getElementById("title"+i).value = idnot;

									document.getElementById("text"+i).innerHTML = "foi resolvido!";

								}
								numNotificationsTotal = numNotifications;
								document.getElementById("numNotifications").innerHTML = numNotifications;
							}else{
								document.getElementById("prevPageNoti").style.display = "none";
								document.getElementById("nextPageNoti").style.display = "none";
								document.getElementById("numNotifications").innerHTML = "0";
							}
						},
						error: function(response) {
							//alert("Error: "+ response.status);
							console.log(JSON.stringify(response));
						},
					});
					localStorage.setItem('userLikes', JSON.stringify(response.propertyMap.user_liked));
					if(localStorage.getItem("userType") == "Cidadão"){
						document.getElementById("submitxi").style.display = "block";
					}
					document.getElementById("registar").style.display = "none";
					document.getElementById("entrar").style.display = "none";
					document.getElementById("sair").style.display = "block";
					document.getElementById("utilizador").style.display = "block";

					document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userName");
					if(localStorage.getItem("userType") == "Governo"){
						
						document.getElementById("utilizadorPa").innerHTML = localStorage.getItem("userCity");
					}
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}
	
	if(localStorage.getItem('tokenID') == null || localStorage.getItem('tokenID') == ""){ //not logged in
		window.location.href = "/";

		document.getElementById("registar").style.display = "block";
		document.getElementById("entrar").style.display = "block";
		document.getElementById("sair").style.display = "none";
		document.getElementById("utilizador").style.display = "none";
	}
	//document.getElementById("logout").onclick = logout;
	//document.getElementById("getAllMaps").onclick = getAllMaps;

    if($('.navbar').length > 0){
        $(window).on("scroll load resize", function(){
            checkScroll();
        });
    }
    document.getElementById("edit-worker-0-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-1-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-2-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-3-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-4-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-5-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-6-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-7-info").onclick = editWorkerInfo;
    document.getElementById("edit-worker-8-info").onclick = editWorkerInfo;
    
    document.getElementById("remove-worker-0").onclick = removeWorkerModal;
    document.getElementById("remove-worker-1").onclick = removeWorkerModal;
    document.getElementById("remove-worker-2").onclick = removeWorkerModal;
    document.getElementById("remove-worker-3").onclick = removeWorkerModal;
    document.getElementById("remove-worker-4").onclick = removeWorkerModal;
    document.getElementById("remove-worker-5").onclick = removeWorkerModal;
    document.getElementById("remove-worker-6").onclick = removeWorkerModal;
    document.getElementById("remove-worker-7").onclick = removeWorkerModal;
    document.getElementById("remove-worker-8").onclick = removeWorkerModal;
    
    document.getElementsByName('city')[0].placeholder= localStorage.getItem('userCity'); //substituir pelo nome da entidade na localstorage localStorage.getItem('tokenIDname');
    document.getElementsByName('city')[0].value = localStorage.getItem('userCity');
    //document.getElementById("registar").style.display = "block";
    
    var frms = $('form[name="add-worker"]');
	frms[0].onsubmit = addWorker;
    
    var frms1 = $('form[name="add-section"]');
	frms1[0].onsubmit = addSection;
    
    document.getElementById("sair").onclick = logout;
    document.getElementById("prevPageNoti").onclick = prevPageNoti;
	document.getElementById("nextPageNoti").onclick = nextPageNoti;
}


prevPageNoti = function(event) {
	console.log("prev TA AQUIIII");
	event.stopPropagation();

	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	if (currPage > 0){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage--;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		document.getElementById("notifications").onclick = undisplayNumNotifications;

		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;

			document.getElementById("text"+j).innerHTML = "foi resolvido!";
			r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}

}



undisplayNumNotifications = function(){
	document.getElementById("numNotifications").innerHTML = "0";
	var i;
	for(i = 0; i < myNotifications.length; i++){
		var notIDcurr = myNotifications[i].propertyMap.not_id;
		var openedNot = myNotifications[i].propertyMap.not_opened;
		if(!openedNot){
			$.ajax({
				type: "PUT",
				url: "/rest/login/openNotification/" + notIDcurr ,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		}
	}
}


clickedNotification = function(event){
	console.log(this.id);
	console.log(this.value);
	var vall = Number(this.value);

	var notIDcurr = myNotifications[vall].propertyMap.not_id;
	var occIDcurr = myNotifications[vall].propertyMap.not_occID;
	$.ajax({ 
		type: "GET",
		//dataType: "json",
		url: "/rest/login/getOneOccurrence/" + occIDcurr,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response1){        
			localStorage.setItem("currReport_type",response1.propertyMap.occurrence_type);
			localStorage.setItem("currReport_location",response1.propertyMap.occurrence_location);
			localStorage.setItem("currReport_description",response1.propertyMap.occurrence_description);
			localStorage.setItem("currReport_creation_time",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_likes",response1.propertyMap.occurrence_likes);
			localStorage.setItem("currReport_urgency",response1.propertyMap.occurrence_urgency);
			localStorage.setItem("currReport_status",response1.propertyMap.occurrence_status);
			localStorage.setItem("currReport_creator",response1.propertyMap.occurrence_creator);
			localStorage.setItem("currReport_id",response1.propertyMap.occurrence_id);
			localStorage.setItem("currReport_filename",response1.propertyMap.occurrence_filename);
			localStorage.setItem("currReport_creationTime",response1.propertyMap.occurrence_creation_time.value);
			localStorage.setItem("currReport_rating",response1.propertyMap.occurrence_rating);
			localStorage.setItem("currReport_city",response1.propertyMap.occurrence_city);

			console.log(response1.propertyMap.occurrence_creation_time.value);
			if(response1.propertyMap.occurrence_status == "Resolvido"){
				localStorage.setItem("currReport_changeTime",response1.propertyMap.occurrence_change_time.value);
			}

			window.location.href = "/occurrence.html";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		},
	});


}

var notifications;
var myNotifications = [];
var numNotificationsTotal;

var isValidUser;
isValid = function(username){
	$.ajax({
		type: "GET",
		url: "/rest/register/getusername/" + username,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			isValidUser = true;
			$.ajax({
				type: "POST",
				url: "/rest/recover/" + username,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				//dataType: "json",
				success: function(response) {
					//var lin = response;
					console.log(response);
					//enviar mail para o php
					alert("Mail enviado!");
				},
				error: function(response) {
					//alert("Error: "+ response.status);
				}
			});
		},
		error: function(response) {
			isValidUser = false;
		}
	});

	event.preventDefault();
};

recWord = function(event) {
	var data = $('form[name="recovery"]').jsonify();
	isValid(data.recovemail);
};

var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}


var hidden = true;
recovShow = function(event){
	if(hidden){
		document.getElementById("acc-recovery").style.display = "block";
		hidden=false;

	}
	else{
		document.getElementById("acc-recovery").style.display = "none";
		hidden=true;
	}
}

recovHide = function(event){
	document.getElementById("acc-recovery").style.display = "none";
}


var responseGlobalNoti = [];
var responseCurrPageNoti = [];
var currentCursorNoti = "first";

nextPageNoti = function(event) {
	console.log("next TA AQUIIII");

	event.stopPropagation();
	
	var r = responseGlobalNoti;

	var currPage = localStorage.getItem("currPageNoti");
	var t = (r.length / 4) - 1;


	if (currPage < t){
		document.getElementById("notification0").style.display = "none";
		document.getElementById("notification1").style.display = "none";
		document.getElementById("notification2").style.display = "none";
		document.getElementById("notification3").style.display = "none";
		currPage++;
		localStorage.setItem("currPageNoti",currPage);
		var start = currPage * 4;
		var end = start + 4;
		var j = 0;
		console.log("stuff "+r.length);
		console.log("starttt "+start);
		console.log("endddd "+end);
		var r1 = [];
		var numNotifications = 0;
		undisplayNumNotifications;
		for (i = start; i < end; i++) { 


			if(responseGlobalNoti.length == i){
				break;
			}

			var readAlready = responseGlobalNoti[i].propertyMap.not_opened;
			var message = responseGlobalNoti[i].propertyMap.not_message;

			if(readAlready){
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
			}else{
				numNotifications++;
				document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
			}
			myNotifications.push(responseGlobalNoti[i]);
			var datei = responseGlobalNoti[i].propertyMap.not_date.value;
			var idnot = responseGlobalNoti[i].propertyMap.not_id;

			document.getElementById("notification"+j).style.display = "block";
			document.getElementById("notification"+j).onclick = clickedNotification;
			document.getElementById("date"+j).style.display = "block";
			document.getElementById("date"+j).innerHTML = datei;
			document.getElementById("title"+j).value = idnot;

			document.getElementById("text"+j).innerHTML = "foi resolvido!";
			r1.push(responseGlobalNoti[i]);
			j++;

		}
		responseCurrPageNoti = r1;
		numNotificationsTotal = numNotifications;
		document.getElementById("numNotifications").innerHTML = numNotifications;
	}else{

		$.ajax({ 
			type: "GET",
			//dataType: "json",
			url: "/rest/login/getAllNotificationsPage/"+ localStorage.getItem('userEmail') +"/"+currentCursorNoti,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response){ 

				if(response.results.length != 0){
					document.getElementById("notification0").style.display = "none";
					document.getElementById("notification1").style.display = "none";
					document.getElementById("notification2").style.display = "none";
					document.getElementById("notification3").style.display = "none";
					currPage++;
					localStorage.setItem("currPageNoti",currPage);
					currentCursorNoti = response.cursor;
					responseCurrPageNoti = response.results;
					console.log(response.results.length);
					console.log("martaaaa");
					responseGlobalNoti = responseGlobalNoti.concat(response.results);
					var j = 0;
					var numNotifications = 0;
					undisplayNumNotifications;
					for (i = 0; i < 4; i++) { 


						if(response.results.length == i){
							break;
						}

						var readAlready = response.results[i].propertyMap.not_opened;
						var message = response.results[i].propertyMap.not_message;

						if(readAlready){
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +"</strong>";
						}else{
							numNotifications++;
							document.getElementById("title"+j).innerHTML = "<strong>"+ message +" (NOVO)</strong>";
						}
						myNotifications.push(response.results[i]);
						var datei = response.results[i].propertyMap.not_date.value;
						var idnot = response.results[i].propertyMap.not_id;

						document.getElementById("notification"+j).style.display = "block";
						document.getElementById("notification"+j).onclick = clickedNotification;
						document.getElementById("date"+j).style.display = "block";
						document.getElementById("date"+j).innerHTML = datei;
						document.getElementById("title"+j).value = idnot;

						document.getElementById("text"+j).innerHTML = "foi resolvido!";
						//r1.push(response.results[i]);
						j++;

					}
					console.log(response.results);
					responseCurrPageNoti = response.results;
					numNotificationsTotal = numNotifications;
					document.getElementById("numNotifications").innerHTML = numNotifications;
				}
			},
			error: function(response) {
				//alert("Error: "+ response.status);
				console.log(JSON.stringify(response));
			},
		});

	}
}


