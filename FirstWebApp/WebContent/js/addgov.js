function checkScroll(){
	var startY = $('.navbar').height() * 0.1; //The point where the navbar changes in px

	if($(window).scrollTop() > startY){
		$('.navbar').addClass("scrolled");
		$('.dropdown').addClass("scrolled");
	}else{
		$('.navbar').removeClass("scrolled");
	}
}

captureData = function(event) {

	var data = $('form[name="govern-regist"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/govReg/" + data.gov +"/"+ data.email,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			alert("Mail enviado!");
		},
		error: function(response) {
			//alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	if($('.navbar').length > 0){
		$(window).on("scroll load resize", function(){
			checkScroll();
		});
	}
	
    var frms0 = $('form[name="govern-regist"]');
    frms0[0].onsubmit = captureData;
}