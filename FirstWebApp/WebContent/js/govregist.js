var idU;
var username;
var check ;
checkID = function(){

	$.ajax({
		type: "GET",
		url: "/rest/govReg/getCity/" + idU,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
            username = response.propertyMap.gov_username;
            check = true;
            document.getElementById("city").value = response.propertyMap.gov_city;
		},
		error: function(response) {
			 alert("Pedimos desculpa mas este link de recuperação não existe ou poderá ter expirado.");
		        window.location.href = "/";
		}
	});

	event.preventDefault();
};

removeIDEntry = function(){
    $.ajax({ 
		type: "DELETE",
		//dataType: "json",
		url: "/rest/govReg/remove/" + idU,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response){
			window.location.href = "/";
		},
		error: function(response) {
			//alert("Error: "+ response.status);
			console.log(JSON.stringify(response));
		}
	});
}

captureData = function(event) {

	var data = $('form[name="govreg"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "/rest/register/gov",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			removeIDEntry();
		},
		error: function(response) {
			alert("Error: "+ response);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};
 

window.onload = function() {
    idU = getQueryVariable("id");
    checkID();
    
    console.log(idU);
    var frms0 = $('form[name="govreg"]');
    frms0[0].onsubmit = captureData;
}