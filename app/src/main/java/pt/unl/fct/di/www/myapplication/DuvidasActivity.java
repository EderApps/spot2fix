package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class DuvidasActivity extends AppCompatActivity {
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duvidas);

        ListView duvidas = (ListView) findViewById(R.id.duv);
        String[] items = {" "};
        arrayList = new ArrayList<>(Arrays.asList(items));
        adapter = new ArrayAdapter<String>(this, R.layout.list,R.id.txtitem,arrayList);
        duvidas.setAdapter(adapter);
        editText = (EditText) findViewById(R.id.editText);
        Button bSubmeterDuv = (Button) findViewById(R.id.bSubmeterDuv);



        final Button bListarOc = (Button) findViewById(R.id.bListarOc);
        final Button bSubmeterOc = (Button) findViewById(R.id.bSubmeterOc);
        final Button bDuvidas = (Button) findViewById(R.id.bDuvidas);
        final Button bMenu = (Button) findViewById(R.id.bMenu);


        bListarOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listarOcIntent = new Intent(DuvidasActivity.this,ListOcActivity.class);
                DuvidasActivity.this.startActivity(listarOcIntent);
            }
        });

        bMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent(DuvidasActivity.this,UserAreaActivity.class);
                DuvidasActivity.this.startActivity(menuIntent);
            }
        });


        bSubmeterOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fazerOcIntent = new Intent(DuvidasActivity.this,SubmeterOcActivity.class);
                DuvidasActivity.this.startActivity(fazerOcIntent);
            }
        });

        bDuvidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent duvidasIntent = new Intent(DuvidasActivity.this,DuvidasActivity.class);
                DuvidasActivity.this.startActivity(duvidasIntent);
            }
        });

        bSubmeterDuv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String newItem = editText.getText().toString();
                arrayList.add(newItem);
                adapter.notifyDataSetChanged();

                //Intent subDuvIntent = new Intent(DuvidasActivity.this,DuvidasActivity.class);
                //DuvidasActivity.this.startActivity(subDuvIntent);
            }
        });


    }


}
