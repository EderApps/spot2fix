package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FrontPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);


        final Button logInButton = (Button) findViewById(R.id.bLogIn);
        final TextView registerLink = (TextView) findViewById(R.id.registerLink);


        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(FrontPage.this, LoginActivity.class);
                FrontPage.this.startActivity(loginIntent);
            }
        });

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(FrontPage.this, LoginActivity.class);
                FrontPage.this.startActivity(loginIntent);
            }
        });

    }
}
