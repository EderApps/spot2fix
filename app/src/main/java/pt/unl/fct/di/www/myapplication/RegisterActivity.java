package pt.unl.fct.di.www.myapplication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class RegisterActivity extends AppCompatActivity {

    private Button registerButton;
    private View RegisterFormView, ProgressView;
    private EditText etNome, etEmail, etCidade, etMorada, password, confPassword ;

    private UserRegisterTask mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etNome = (EditText) findViewById(R.id.etNome);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etCidade = (EditText) findViewById(R.id.etCidade);
        etMorada = (EditText) findViewById(R.id.etMorada);
        password = (EditText) findViewById(R.id.password);
        confPassword = (EditText) findViewById(R.id.confirmPassword);

        registerButton = (Button) findViewById(R.id.register_button);


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        RegisterFormView = findViewById(R.id.login_form);
        ProgressView = findViewById(R.id.login_progress);

    }

    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }


        etNome.setError(null);
        etEmail.setError(null);
        etCidade.setError(null);
        etMorada.setError(null);
        password.setError(null);
        confPassword.setError(null);

        String nome = etNome.getText().toString();
        String email = etEmail.getText().toString();
        String cidade = etCidade.getText().toString();
        String morada = etMorada.getText().toString();
        String pass = password.getText().toString();
        String confPass = confPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(nome)) {
            etNome.setError("This field is required");
            focusView = etNome;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            etEmail.setError("This field is required");
            focusView = etEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            etEmail.setError("This field is invalid");
            focusView = etEmail;
            cancel = true;
        }

        if(TextUtils.isEmpty(cidade)) {
            etCidade.setError("This field is required");
            focusView = etCidade;
            cancel = true;
        }

        if(TextUtils.isEmpty(morada)) {
            etMorada.setError("This field is required");
            focusView = etMorada;
            cancel = true;
        }

        if (TextUtils.isEmpty(pass) && !isPasswordValid(pass)) {
            password.setError("This field is required");
            focusView = password;
            cancel = true;
        }

        if (TextUtils.isEmpty(confPass) && !isPasswordValid(confPass)) {
            confPassword.setError("This field is required");
            focusView = confPassword;
            cancel = true;
        }
        else if(!TextUtils.equals(pass,confPass)) {
            confPassword.setError("This field is incorrect");
            focusView = confPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else{
            showProgress(true);
        }


    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            RegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            ProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {

                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            RegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */

    public class UserRegisterTask extends AsyncTask<Void, Void, String> {
        private final String uNome;
        private final String uEmail;
        private final String uCidade;
        private final String uMorada;
        private final String uPassword;
        private final String passConf;

        UserRegisterTask(String email, String password, String nome,String cidade, String morada, String passconfirmacao) {
            uEmail = email;
            uPassword = password;
            uNome = nome;
            uCidade = cidade;
            uMorada = morada;
            passConf = passconfirmacao;
        }

        @Override
        protected void onPreExecute() {
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if((networkInfo == null) || !networkInfo.isConnected() ||
                    ((networkInfo.getType() != ConnectivityManager.TYPE_WIFI) &&
                            (networkInfo.getType() != ConnectivityManager.TYPE_MOBILE))) {
                cancel(true);
            }
        }



        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service..
            return null;
        }
    }
}
