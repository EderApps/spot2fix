package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SubmeterOcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submeter_oc);


        final Button bListarOc = (Button) findViewById(R.id.bListarOc);
        final Button bFazerOc = (Button) findViewById(R.id.bFazerOc);
        final Button bDuvidas = (Button) findViewById(R.id.bDuvidas);
        final Button bMenu = (Button) findViewById(R.id.bMenu);
        final Button bSubmeterOc = (Button) findViewById(R.id.bSubmeterOc);

        bListarOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listarOcIntent = new Intent(SubmeterOcActivity.this, ListOcActivity.class);
                SubmeterOcActivity.this.startActivity(listarOcIntent);
            }
        });

        bMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent(SubmeterOcActivity.this, UserAreaActivity.class);
                SubmeterOcActivity.this.startActivity(menuIntent);
            }
        });

        bFazerOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fazerOcIntent = new Intent(SubmeterOcActivity.this, SubmeterOcActivity.class);
                SubmeterOcActivity.this.startActivity(fazerOcIntent);
            }
        });

        bDuvidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent duvidasIntent = new Intent(SubmeterOcActivity.this, DuvidasActivity.class);
                SubmeterOcActivity.this.startActivity(duvidasIntent);
            }
        });

        bSubmeterOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent submeterIntent = new Intent(SubmeterOcActivity.this, ListOcActivity.class);
                SubmeterOcActivity.this.startActivity(submeterIntent);
            }
        });

    }
}

