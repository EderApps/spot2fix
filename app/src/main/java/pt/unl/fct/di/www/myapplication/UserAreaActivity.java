package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserAreaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_area);

        final Button bListarOc = (Button) findViewById(R.id.bListarOc);
        final Button bSubmeterOc = (Button) findViewById(R.id.bSubmeterDuv);
        final Button bDuvidas = (Button) findViewById(R.id.bDuvidas);
        final TextView logout = (TextView) findViewById(R.id.logout);

        bListarOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listarOcIntent = new Intent(UserAreaActivity.this,ListOcActivity.class);
                UserAreaActivity.this.startActivity(listarOcIntent);
            }
        });

        bSubmeterOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent submeterOcIntent = new Intent(UserAreaActivity.this,SubmeterOcActivity.class);
                UserAreaActivity.this.startActivity(submeterOcIntent);
            }
        });

        bDuvidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent duvidasIntent = new Intent(UserAreaActivity.this,DuvidasActivity.class);
                UserAreaActivity.this.startActivity(duvidasIntent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logout = new Intent(UserAreaActivity.this,FrontPage.class);
                UserAreaActivity.this.startActivity(logout);
            }
        });


    }
}
