package pt.unl.fct.di.www.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ListOcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_oc);


        final Button bListarOc = (Button) findViewById(R.id.bListarOc);
        final Button bDuvidas = (Button) findViewById(R.id.bDuvidas);
        final Button bMenu = (Button) findViewById(R.id.bMenu);
        final Button bSubmeterOc = (Button) findViewById(R.id.bSubmeterOc);

        bListarOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listarOcIntent = new Intent(ListOcActivity.this,ListOcActivity.class);
                ListOcActivity.this.startActivity(listarOcIntent);
            }
        });

        bMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent(ListOcActivity.this,UserAreaActivity.class);
                ListOcActivity.this.startActivity(menuIntent);
            }
        });



        bSubmeterOc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fazerOcIntent = new Intent(ListOcActivity.this,SubmeterOcActivity.class);
                ListOcActivity.this.startActivity(fazerOcIntent);
            }
        });

        bDuvidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent duvidasIntent = new Intent(ListOcActivity.this,DuvidasActivity.class);
                ListOcActivity.this.startActivity(duvidasIntent);
            }
        });

    }
}
